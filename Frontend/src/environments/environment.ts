// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAdR4Ba1aJ2vnSLVuQ_IYFwaSPEFEwsvUQ',
    authDomain: 'proyectoicfes-5e7d6.firebaseapp.com',
    databaseURL: 'https://proyectoicfes-5e7d6.firebaseio.com',
    projectId: 'proyectoicfes-5e7d6',
    storageBucket: 'proyectoicfes-5e7d6.appspot.com',
    messagingSenderId: '895792055053'
  },
  //url: 'http://localhost:1641/'
  url: 'http://preicfesparasordos.com:1641/'
  // url: '52.67.9.30:1641/'
};
