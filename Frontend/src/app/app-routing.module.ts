import { ShowQuestionsComponent } from './components/show-questions/show-questions.component';
import { AveragesComponent } from './components/averages/averages.component';
import { ShowTablesResultsComponent } from './components/show-tables-results/show-tables-results.component';
import { VocabularyComponent } from './components/vocabulary/vocabulary.component';
import { ResourcesComponent } from './components/resources/resources.component';
import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {HomeScreenComponent} from './components/home-screen/home-screen.component';
import { RecoverPasswordComponent } from './components/recover-password/recover-password.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { IndexUserComponent } from './components/index-user/index-user.component';
import { SettingUserComponent } from './components/setting-user/setting-user.component';
import { AdminComponent } from './components/admin/admin.component';
import { AddQuestionComponent } from './components/add-question/add-question.component';
import { AddTestComponent } from './components/add-test/add-test.component';
import { ErrorComponent } from './components/error/error.component';
import { TestSectionComponent } from './components/test-section/test-section.component';
import { EndTestComponent } from './components/end-test/end-test.component';
import { ResultsComponent } from './components/results/results.component';
import { TestStartComponent } from './components/test-start/test-start.component';
import { SuggestionsComponent } from './components/suggestions/suggestions.component';
import { ResultsTriesComponent } from './components/results-tries/results-tries.component';
import { TipsComponent } from './components/tips/tips.component';
import { ShowSuggestionsComponent } from './components/show-suggestions/show-suggestions.component';

const routes: Routes = [
  {path: '', component: HomeScreenComponent},
  {path: 'recover/:type', component: RecoverPasswordComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'index', component: IndexUserComponent},
  {path: 'setting/:type', component: SettingUserComponent},
  {path: 'admin/:tab', component: AdminComponent},
  {path: 'addQuestion', component: AddQuestionComponent},
  {path: 'addQuestion/:id', component: AddQuestionComponent},
  {path: 'addTest', component: AddTestComponent},
  {path: 'addTest/:id', component: AddTestComponent},
  {path: 'test/:subType/:session', component: TestSectionComponent},
  {path: 'endTest/:type', component: EndTestComponent},
  {path: 'showResults/:question/:session', component: ResultsComponent},
  {path: 'showResults/:question/:session/:try', component: ResultsComponent},
  {path: 'resources', component: ResourcesComponent},
  {path: 'testStart/:subType', component: TestStartComponent},
  {path: 'testStart/:subType/:second', component: TestStartComponent},
  {path: 'vocabulary', component: VocabularyComponent},
  {path: 'suggestions', component: SuggestionsComponent},
  {path: 'suggestions/:back', component: SuggestionsComponent},
  {path: 'showTables', component: ShowTablesResultsComponent},
  {path: 'showTables/:try', component: ShowTablesResultsComponent},
  {path: 'resultsTries/:id/:subType', component: ResultsTriesComponent},
  {path: 'tips', component: TipsComponent},
  {path: 'showSuggestions', component: ShowSuggestionsComponent},
  {path: 'averages', component: AveragesComponent},
  {path: 'showQuestions', component: ShowQuestionsComponent},
  {path: '**', component: ErrorComponent}
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
