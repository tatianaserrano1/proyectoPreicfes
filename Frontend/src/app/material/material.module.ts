import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  MatMenuModule,
  MatCheckboxModule,
  MatButtonModule,
  MatIconModule,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatInputModule,
  MatGridListModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatToolbarModule,
  MatCardModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatSelectModule,
  MatDividerModule,
  MatListModule,
  MatSnackBarModule,
  MatSidenavModule,
  MatDialogModule,
  MatRadioModule,
  MatError,
  MatBadgeModule,
  MatStepperModule,
  MatSlideToggleModule,
  MatProgressSpinnerModule


} from '@angular/material';
import { DialogComponent } from '../components/table-users/table-users.component';
import { DialogQuestionComponent } from '../components/table-questions/table-questions.component';
import { DialogTestComponent } from '../components/table-tests/table-tests.component';
import { DialogSectionComponent, DialogConfirmationComponent } from '../components/test-section/test-section.component';
import { DialogInformationComponent } from '../components/add-test/add-test.component';
import { HelpDialogComponent, HelpDialogAdminComponent } from './../components/navbar-user/navbar-user.component';
import { DialogSpinnerComponent } from '../components/suggestions/suggestions.component';

const components = [
  MatMenuModule,
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatDividerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatSelectModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatSidenavModule,
  ReactiveFormsModule,
  MatDialogModule,
  MatRadioModule,
  MatBadgeModule,
  MatStepperModule,
  MatSlideToggleModule,
  MatProgressSpinnerModule

];

@NgModule({
  imports: components,
  exports: components,
  entryComponents: [DialogComponent, DialogQuestionComponent, DialogTestComponent, DialogSectionComponent, DialogConfirmationComponent, DialogInformationComponent, HelpDialogComponent, HelpDialogAdminComponent, DialogSpinnerComponent]
})
export class MaterialModule {}
