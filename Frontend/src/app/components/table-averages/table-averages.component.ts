import { Component, OnInit, Input } from '@angular/core';
import { UserService } from './../../services/user.service';
import {TestService} from '../../services/test.service';
import {MatTableDataSource} from '@angular/material';

export interface averagesData {
  area: string;
  score: any;
  people: number;

}
@Component({
  selector: 'app-table-averages',
  templateUrl: './table-averages.component.html',
  styleUrls: ['./table-averages.component.css'],
  providers: [ UserService, TestService]
})
export class TableAveragesComponent implements OnInit {

  // @Input() idTest: string;
  // @Input() type: string;
  public idTest:string;
  public existTest: boolean;
  public testSelected;
  public type:string;
  public tests: any[] = [];
  public testsSimulacro: any[] = [];
  public testsPruebas: any[] = [];
  public identity;
  public token: string;
  public flag = 0;
  public averagesArray: any[] = [];
  public totalAverage;
  public totalAmount;
  public blnShow: boolean = true;
  displayedColumns: string[] = ['area', 'score', 'people'];
  dataSource: MatTableDataSource<averagesData>;
  constructor(private _testService: TestService, private _userService: UserService) {
    this.existTest = false;
   }

  ngOnInit() {
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getTokenUser();
    this.getTests();
  }

  getTests(){
    this._testService.getTests(this.token, this.identity._id).subscribe(
      response =>{
        if (response.state === true) {
          this.tests = response.tests;
          for(let test of this.tests){
            if(test.type === 'simulacro'){
              this.testsSimulacro.push(test);
            }else if(test.type === 'prueba'){
              this.testsPruebas.push(test);
            }
          }
        }
      },
     error => {
       const errorMessage = <any>error;
       console.log(errorMessage);
     }
    );
  }
  getAverages(idTest: string, typeTest: string){
    console.log(idTest);
    console.log(this.identity._id);
    console.log(this.token);
    console.log(typeTest);
    this._userService.getAverage(idTest, this.identity._id, this.token, typeTest).subscribe(
      response =>{
        console.log(response);
        if(response.state === 1){
          this.blnShow = false;
          this.existTest = true;
          var pruebas = response.stats;
          localStorage.setItem('showQues',JSON.stringify(pruebas.preguntas));
          this.totalAverage = pruebas.total.score;
          this.totalAmount = pruebas.total.amount;
          this.averagesArray = [
            { area: 'Matemáticas', score: pruebas.matematicas ? pruebas.matematicas.score: 0, people: pruebas.matematicas ? pruebas.matematicas.amount: 0},
            { area: 'Lectura Crítica', score: pruebas.lectura ? pruebas.lectura.score: 0, people: pruebas.lectura ? pruebas.lectura.amount: 0},
            { area: 'Ciencias Naturales', score: pruebas.naturales ? pruebas.naturales.score: 0, people: pruebas.naturales? pruebas.naturales.amount: 0},
            { area: 'Sociales y ciudadanas', score: pruebas.sociales ? pruebas.sociales.score: 0, people: pruebas.sociales ? pruebas.sociales.amount: 0}
        ];
        this.dataSource = new MatTableDataSource(this.averagesArray);
        }else if(response.state === 0){
          this.flag = 1;
          this.existTest = false;
          this.averagesArray = [];
        }
      },
     error => {
       const errorMessage = <any>error;
       console.log(errorMessage);
     }
    );
  }
  getTableSimulacro(){

  }
  getTablePrueba(){

  }
  getSimulacro(type: string){
    if(type === 'examen'){
      this.flag = 5;
    }else if(type === 'prueba'){
      this.flag = 6
    }
  }


}
