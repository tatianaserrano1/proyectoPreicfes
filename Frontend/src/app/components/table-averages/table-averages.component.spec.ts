import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableAveragesComponent } from './table-averages.component';

describe('TableAveragesComponent', () => {
  let component: TableAveragesComponent;
  let fixture: ComponentFixture<TableAveragesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableAveragesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableAveragesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
