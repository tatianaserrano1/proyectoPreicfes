import { IndexUserComponent } from './../index-user/index-user.component';
import { Test } from './../../classes/test';
import { UserService } from './../../services/user.service';
import { TestService } from './../../services/test.service';
import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { OpenSnackBarService } from './../../services/openSnackBar.service';

export interface SessionData {
  test: string;
  numberQues: number;
  time: string;
}
@Component({
  selector: 'app-test-start',
  templateUrl: './test-start.component.html',
  styleUrls: ['./test-start.component.css'],
  providers: [TestService, UserService, OpenSnackBarService]
})
export class TestStartComponent implements OnInit {

  //Atributos
  displayedColumns: string[] = [ 'test', 'numberQues', 'time'];
  dataSource: MatTableDataSource<SessionData>;
  dataSource2: MatTableDataSource<SessionData>;
  public subType: string;
  public second: any;
  public start: boolean = true;
  public start2:boolean = false;
  public type:string;
  public token: string;
  public identity;
  public route;
  public test: any;
  public index: IndexUserComponent;
  public idTest: string; // Atributo contiene el idTest para iniciar test del intento 2 en adelante
  public sessionArray: SessionData[] = [];
  public questionsTest: any[]; //Array de preguntas de la prueba
  public session1area1: any[] =[];
  public session1area2: any[] =[];
  public session2area1: any[] =[];
  public session2area2: any[] =[];
  public timePrueba = '1 hora';
  public timeSimulacro;

  //Métodos
  constructor(private _testService: TestService,
              private _userService: UserService,
              private _route: ActivatedRoute,
              private _router: Router,
              private openSnack: OpenSnackBarService) {
    this.dataSource  = new MatTableDataSource<SessionData>([]);
    this.dataSource2  = new MatTableDataSource<SessionData>([]);
  }

  ngOnInit() {
    this.subType = this._route.snapshot.paramMap.get('subType');
    this.second = this._route.snapshot.paramMap.get('second');
    this.token = this._userService.getTokenUser();
    this.identity = this._userService.getIdentity();
    this.getTest();
  }

  //Inicia un simulacro
  startTest(session:string){
    if(this.subType === 'try'){
      this._testService.startTry(this.identity._id, this.test._id, this.token).subscribe(
        response => {
          if(response.state === true){
            this.identity.pruebasR = response.testsUpdated.pruebasR;
            this.identity.pruebasNR = response.testsUpdated.pruebasNR;
            this._userService.setIdentity(this.identity);
            this._router.navigate(['/test', this.test.subType, session]);
          }else if(response.state === 3){
            var message = "Error de autenticación."
            this.openSnack.openSnackBar(message);
            localStorage.clear();
            this._router.navigate(['/login']);
          }
        },
        error => {
          const errorMessage = <any>error;
          console.log(errorMessage);
        }
      );
    }else{
      this._testService.startTest(this.identity._id, this.test._id, this.token).subscribe(
        response => {
          if(response.state === true){
            this.identity.pruebasR = response.testsUpdated.pruebasR;
            this.identity.pruebasNR = response.testsUpdated.pruebasNR;
            this._userService.setIdentity(this.identity);
            this._router.navigate(['/test', this.test.subType, session]);
          }else if(response.state === 3){
            var message = "Error de autenticación."
            this.openSnack.openSnackBar(message);
            localStorage.clear();
            this._router.navigate(['/login']);
          }
        },
        error => {
          const errorMessage = <any>error;
          console.log(errorMessage);
        }
      );
    }
  }

  getTimesSimulacros(){
    if(this.test.type === 'simulacro'){
      switch(this.test.subType){
        case 'corto':
          this.timeSimulacro = '30 minutos';
          break;
        case 'medio':
          this.timeSimulacro = '1 hora'
          break;
        case 'largo':
          this.timeSimulacro = '2 horas'

      }
    }
  }
  //Llena la tabla con los datos que se obtienen del test
  fillTable(){
    this.getTimesSimulacros();
    if(this.test.type === 'prueba'){
      switch(this.test.subType){
        case 'lectura':
          this.type = 'Lectura Crítica';
          break;
        case 'naturales':
          this.type = 'Ciencias Naturales';
          break;
        case 'sociales':
          this.type = 'Sociales y ciudadanas';
          break;
        case 'sociales':
          this.type = 'Sociales y ciudadanas';
          break;
        case 'matematicas':
          this.type = 'Matemáticas';
      }
      const session: SessionData[] =[
        {test: this.type, numberQues: this.test.session1Area1.length, time: this.timePrueba},
      ]
      this.dataSource = new MatTableDataSource(session);
    }else if(this.test.type === 'simulacro'){
      const session: SessionData[] =[
        {test: this.test.sess1SubType[0], numberQues: this.test.session1Area1.length, time: this.timeSimulacro},
        {test: this.test.sess1SubType[1], numberQues: this.test.session1Area2.length, time: this.timeSimulacro}
      ]
      this.dataSource = new MatTableDataSource(session);

      const session2: SessionData[] = [
        {test: this.test.sess2SubType[0], numberQues: this.test.session2Area1.length, time: this.timeSimulacro},
        {test: this.test.sess2SubType[1], numberQues: this.test.session2Area2.length, time: this.timeSimulacro}
      ]
      this.dataSource2 = new MatTableDataSource(session2);
    }
  }
  //Obtiene el test
  getTest(){
    if(this.second === '2'){
      this.start2 = true;
      this.start = false;
      this.test = JSON.parse(localStorage.getItem('test'));
      this.fillTable();
    }else if(this.subType === 'try'){
      this._testService.getTestId(this.second, this.token, this.identity._id).subscribe(
        response =>{
          if(response.state){
            this.test = response.test;
            localStorage.setItem('test', JSON.stringify(this.test));
            this.fillTable();
          }
        },
        error => {
          const errorMessage = <any>error;
          console.log(errorMessage);
        }
      );
    }else{
      this._testService.getTestSub(this.subType,this.token, this.identity._id).subscribe(
        response => {
          if(response.state === true){
            this.test = response.test;
            localStorage.setItem('test', JSON.stringify(this.test));
            this.fillTable();
          }else if(response.state === 2){
            console.log('No hay pruebas de ese tipo');
            this._router.navigate(['index']);
          }else if(response.state === 3){
            var message = "Error de autenticación."
            this.openSnack.openSnackBar(message);
            localStorage.clear();
            this._router.navigate(['/login']);
          }
        },
        error => {
          const errorMessage = <any>error;
          console.log(errorMessage);
        }
      );
    }
  }
}
