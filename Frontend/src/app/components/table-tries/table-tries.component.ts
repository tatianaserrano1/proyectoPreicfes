import { Component, OnInit, Input} from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import { TestService } from '../../services/test.service';
import {UserService} from '../../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';

export interface triesData {
  id: string;
  try: any;
  score: number;
  good: number;
  bad: number;
  notAnswered: number;
}
@Component({
  selector: 'app-table-tries',
  templateUrl: './table-tries.component.html',
  styleUrls: ['./table-tries.component.css'],
  providers: [UserService, TestService]
})
export class TableTriesComponent implements OnInit {

  @Input() id: string;
  @Input() subType: string;
  public tries:number;
  public triesArray: any[] = [];
  public docTry;
  displayedColumns: string[] = ['try', 'score', 'good', 'bad', 'noAnswered'];
  dataSource: MatTableDataSource<triesData>;

  constructor(private _testService: TestService,
              private _route: ActivatedRoute,
              private _router: Router) { }

  ngOnInit() {
    this.getTries(this.id, this.subType);
  }

  getDocTry(intento: number){
    this.docTry = this._testService.getTestTry(this.id, this.subType, intento);
    console.log(this.docTry);
    localStorage.setItem('testResults', JSON.stringify(this.docTry));
    this._router.navigate(['showTables', intento])
  }

  getTries(id:string, subType: string){
    let identity = JSON.parse(localStorage.getItem('identity'));
    for(let i = 0; i< identity.pruebasR.length; i++){
      if(identity.pruebasR[i]._id === subType){
        var tests = identity.pruebasR[i].pruebas;
        if(tests.length === 0){
           console.log('No hay pruebas de ese subtipo');
        }else{
          for(let j = 0; j< tests.length; j++){
            if(tests[j]._id === id){
              this.tries = tests[j].intentos.length - 1;
              var arrayTries = tests[j].intentos;
              var y = 0;
              for(let k of arrayTries){
                console.log(k);
                this.triesArray.push({
                  try: y = y +1,
                  score: Math.round(k.testScore),
                  good: k.correctas? k.correctas: 0,
                  bad: k.incorrectas? k.incorrectas: 0,
                  notAnswered: k.noRespondidas? k.noRespondidas: 0
                });
              }
            }
          }
        }
      }
    }
    this.dataSource = new MatTableDataSource(this.triesArray);
    // return this.try;
  }

}
