import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableTriesComponent } from './table-tries.component';

describe('TableTriesComponent', () => {
  let component: TableTriesComponent;
  let fixture: ComponentFixture<TableTriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableTriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableTriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
