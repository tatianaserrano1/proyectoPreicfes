import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-vocabulary',
  templateUrl: './vocabulary.component.html',
  styleUrls: ['./vocabulary.component.css']
})
export class VocabularyComponent implements OnInit {

  //Atributos
  public images: any[] = []; //Array con la dirección de las imagenes a cargar
  public imagesNaturales: any[] = [];
  public imagesSociales: any[] = [];
  public imagesLectura: any[] = [];
  public imagesMatematicas: any[] = [];
  constructor() { }

  ngOnInit() {
    // this.loadImage();
  }

  removeChild(){
    this.images = [];
    const container = document.getElementById("voc");
    while(container.hasChildNodes()){
      container.removeChild(container.firstChild);
    }
  }

  loadImageType(cantidad: Number, subject: String){

    this.removeChild();
    for(let i=0; i<cantidad; i++){
      this.images[i] = '../../../assets/vocabulary/'+subject+'/'+(i+1)+'.png';
    }
    for(let i=0; i<cantidad; i++){
      document.getElementById("voc").innerHTML += '<img src="'+this.images[i]+'">';
    }

  }

  loadImages(type: String) {
    switch(type){
      case 'matematica':
          document.getElementById("matematica").style.opacity = "1";
          document.getElementById("lectura").style.opacity = "0.5";
          document.getElementById("naturales").style.opacity = "0.5";
          document.getElementById("sociales").style.opacity = "0.5";
          this.loadImageType(4, 'matematica');
      break;
      case 'sociales':
          document.getElementById("sociales").style.opacity = "1";
          document.getElementById("lectura").style.opacity = "0.5";
          document.getElementById("naturales").style.opacity = "0.5";
          document.getElementById("matematica").style.opacity = "0.5";
          this.loadImageType(1, 'sociales');
      break;
      case 'lectura':
          document.getElementById("lectura").style.opacity = "1";
          document.getElementById("sociales").style.opacity = "0.5";
          document.getElementById("naturales").style.opacity = "0.5";
          document.getElementById("matematica").style.opacity = "0.5";
          this.loadImageType(2, 'lectura');
      break;
      case 'naturales':
          document.getElementById("naturales").style.opacity = "1";
          document.getElementById("sociales").style.opacity = "0.5";
          document.getElementById("lectura").style.opacity = "0.5";
          document.getElementById("matematica").style.opacity = "0.5";
          this.loadImageType(11, 'naturales');
    }

  }
}
