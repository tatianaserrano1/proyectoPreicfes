import { Component, HostListener} from '@angular/core';

@Component({
  selector: 'app-home-screen',
  templateUrl: './home-screen.component.html',
  styleUrls: ['./home-screen.component.css']
})
export class HomeScreenComponent  {

  constructor() {
   }

   @HostListener('window:scroll', ['$event'])
   public hideInfo($event: Event) {
     let scrollOffset = $event.target['scrollingElement'].scrollTop;
     const elementInfo = document.getElementById('moreInfo');
     if (scrollOffset > 100) {
      elementInfo.style.display = 'none';
     } else {
      elementInfo.style.display = 'block';
     }
   }

}
