import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowTablesResultsComponent } from './show-tables-results.component';

describe('ShowTablesResultsComponent', () => {
  let component: ShowTablesResultsComponent;
  let fixture: ComponentFixture<ShowTablesResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowTablesResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowTablesResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
