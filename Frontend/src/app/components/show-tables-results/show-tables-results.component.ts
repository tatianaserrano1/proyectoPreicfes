import { Component, OnInit } from '@angular/core';
import { TestService } from '../../services/test.service';
import {UserService} from '../../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { getTestBed } from '@angular/core/testing';

@Component({
  selector: 'app-show-tables-results',
  templateUrl: './show-tables-results.component.html',
  styleUrls: ['./show-tables-results.component.css'],
  providers: [UserService, TestService]
})
export class ShowTablesResultsComponent implements OnInit {

  public intento;
  public test: any;
  public indetity;
  public s1a1: string;
  public s1a2: string;
  public s2a1: string;
  public s2a2: string;

  constructor(private _userService: UserService,
              private _testService: TestService,
              private _route: ActivatedRoute,
              private _router: Router) { }

  ngOnInit() {
    this.intento = this._route.snapshot.paramMap.get('try');
    this.getTest();
    this.getAreas();
  }
  goBack(){
    if(this.intento){
      this._router.navigate(['resultsTries', this.test._id, this.test.subType]);
    }else{
      this._router.navigate(['endTest', 'end']);
    }
  }

  getTest(){
    if(this.intento){
      this.test = JSON.parse(localStorage.getItem('testResults'));
    }else{
      this.test = this._testService.getTestLocal();
    }
    this.test.session1Area1Score = Math.round(this.test.session1Area1Score);
    this.test.session1Area2Score = Math.round(this.test.session1Area2Score);
    this.test.session2Area1Score = Math.round(this.test.session2Area1Score);
    this.test.session2Area2Score = Math.round(this.test.session2Area2Score);
    this.test.testScore = Math.round(this.test.testScore);
  }
  getAreas(){
    if(this.test.type === 'simulacro'){
      this.s1a1 = this.test.sess1SubType[0];
      this.s1a2 = this.test.sess1SubType[1];
      this.s2a1 = this.test.sess2SubType[0];
      this.s2a2 = this.test.sess2SubType[1];
    }
  }
}
