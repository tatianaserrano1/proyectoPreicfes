import { Component, OnInit, ViewChild} from '@angular/core';
import {UserService} from '../../services/user.service';
import {MatTableDataSource, MatPaginator, MatSort} from '@angular/material';


export interface testData {
  id: any;
  position: number;
  subType: string;
  try: number;
}
@Component({
  selector: 'app-index-user',
  templateUrl: './index-user.component.html',
  styleUrls: ['./index-user.component.css'],
  providers: [UserService]
})
export class IndexUserComponent implements OnInit {

  // Atributos
  public identity: any;
  public type;
  public testsArray: any[] =[];
  public pruebasR: any[] = [];
  public pruebasNR: any[] = [];
  public largos: any[]= [];
  public medios: any[] = [];
  public cortos: any[]= [];
  public naturales: any[]= [];
  public sociales: any[]= [];
  public lectura: any[]= [];
  public matematicas: any[]= [];
  public numberNaturales: number;
  public numberLectura: number;
  public numberSociales: number;
  public numberMatematicas: number;
  public numberLargo: number;
  public numberMedio: number;
  public numberCorto: number;


  // Métodos
  constructor(private _userService: UserService) {
  }

   ngOnInit() {
      this.identity = this._userService.getIdentity();
      this.type = this.identity.tipo;
      this.getTests();
   }

   getTests(){
     this.pruebasR = this.identity.pruebasR;
     this.pruebasNR = this.identity.pruebasNR;
     //Calcula el numero de cada notificación de simulacros
     for(let nr of this.pruebasNR){
       if(nr._id === 'naturales'){
         this.numberNaturales = nr.pruebas.length;
       }else if(nr._id === 'sociales'){
         this.numberSociales = nr.pruebas.length;
       }else if(nr._id === 'lectura'){
         this.numberLectura = nr.pruebas.length;
       }else if(nr._id === 'matematicas'){
         this.numberMatematicas = nr.pruebas.length;
       }else if(nr._id === 'largo'){
         this.numberLargo = nr.pruebas.length;
       }else if(nr._id === 'medio'){
         this.numberMedio = nr.pruebas.length;
       }else if(nr._id === 'corto'){
         this.numberCorto = nr.pruebas.length;
       }
     }
   }

}
