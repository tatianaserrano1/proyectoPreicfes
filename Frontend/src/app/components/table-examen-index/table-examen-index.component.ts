import { Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource, MatPaginator, MatSort} from '@angular/material';
import {UserService} from '../../services/user.service';
import { TestService } from '../../services/test.service';

export interface testData {
  id: any;
  position: number;
  subType: string;
  subType2: string;
  try: number;
}

@Component({
  selector: 'app-table-examen-index',
  templateUrl: './table-examen-index.component.html',
  styleUrls: ['./table-examen-index.component.css'],
  providers: [UserService, TestService]
})
export class TableExamenIndexComponent implements OnInit {

  public identity: any;
  public type;
  public pruebasR: any[] = [];
  public pruebasNR: any[] = [];
  public testsArray: any[] =[];
  public token: string;
  displayedColumns: string[] = ['position', 'subType', 'try', 'button', 'buttonResults'];
  dataSource: MatTableDataSource<testData>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private _userService: UserService, private _testService: TestService) {
    this.dataSource  = new MatTableDataSource<testData>([]);

   }

  ngOnInit() {
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getTokenUser();
    this.type = this.identity.tipo;
    this.getTests();
  }

  applyFilter(filterValue: string){
    filterValue = filterValue.trim(); //Elimina espacios en blanco
    filterValue = filterValue.toLowerCase(); // coloca en minuscula
    this.dataSource.filter = filterValue;
  }

  getTests(){
    this.pruebasR = this.identity.pruebasR;
    this.pruebasNR = this.identity.pruebasNR;
    //Calcula el numero de cada notificación de simulacros
   //Llena el array que se presenta en la tabla de simulacros realizados
   var y = 0;
   for(let test of this.pruebasR){
     if(test._id==='largo'){
       for(let i of test.pruebas){
         this.testsArray.push({
           id: i._id,
           position: y = y+1,
           subType: 'Largo',
           subType2: test._id,
           try: i.intentos.length
         });
       }
     }
     if(test._id==='medio'){
       for(let j of test.pruebas){
         this.testsArray.push({
           id: j._id,
           position: y = y+1,
           subType: 'Medio',
           subType2: test._id,
           try: j.intentos.length
         });
       }
     }
     if(test._id==='corto'){
       for(let k of test.pruebas){
         this.testsArray.push({
           id: k._id,
           position: y = y+1,
           subType: 'Corto',
           subType2: test._id,
           try: k.intentos.length
         });
       }
     }
   }
   this.dataSource = new MatTableDataSource(this.testsArray);
   this.dataSource.paginator = this.paginator;
   this.dataSource.sort = this.sort;
  }
}
