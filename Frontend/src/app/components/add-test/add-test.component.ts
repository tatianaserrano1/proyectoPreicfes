import { environment } from './../../../environments/environment';
import { UserService } from './../../services/user.service';
import { Component, OnInit,  Inject } from '@angular/core';
import { Injectable } from '@angular/core';
import {SelectionModel} from '@angular/cdk/collections';
import {Test} from '../../classes/test';
import {QuestionService} from '../../services/question.service';
import {TestService} from '../../services/test.service';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {MatDialog, MatDialogRef, MatDialogConfig, MAT_DIALOG_DATA} from '@angular/material';
import { OpenSnackBarService } from './../../services/openSnackBar.service';

@Component({
  selector: 'app-add-test',
  templateUrl: './add-test.component.html',
  styleUrls: ['./add-test.component.css'],
  providers: [QuestionService, TestService, UserService, OpenSnackBarService]
})
export class AddTestComponent implements OnInit{

  // Atributos
  isLinear = true;
  myModel = true;
  public area11: string; //Área 1 de la sesión 1
  public area21: string; //Área 2 de la sesión 1
  public area12: string;
  public area22: string;
  public test: Test;
  public idEdit;
  public token: string;
  public idUser: string;
  public check = true;
  public questionsAdd: any[] = []; // Arreglo que guarda las preguntas de enviar
  public questions: any[] = []; //Arreglo que guarda las preguntas por tipo
  public questionsArray: any[] = []; //Arreglo que guarda todas las preguntas
  public questionsCheck: any[] = []; // Arreglo que guarda las preguntas seleccionadas cuando es tipo: prueba
  public questionsCheck1: any[] = []; // Arreglo que guarda las preguntas seleccionadas 1 cuando es tipo: simulacro
  public questionsCheck2: any[] = []; // Arreglo que guarda las preguntas seleccionadas 2 cuando es tipo: simulacro
  public questionsCheck3: any[] = []; // Arreglo que guarda las preguntas seleccionadas 3 cuando es tipo: simulacro
  public questionsCheck4: any[] = []; // Arreglo que guarda las preguntas seleccionadas 4 cuando es tipo: simulacro
  public sesion1SubType: string[] = [];
  public sesion2SubType: string[] = [];
  public session1area1: any[] =[];
  public session1area2: any[] =[];
  public session2area1: any[] =[];
  public session2area2: any[] =[];
  public quesNaturales: any[] =[];
  public quesSociales: any[] =[];
  public quesMatematicas: any[] =[];
  public quesLectura: any[] =[];
  public dsPrueba: boolean = true;
  public dsExamen: boolean = true;

  tipos = [
    {value: 'prueba', viewValue: 'Simulacro de prueba'},
    {value: 'simulacro', viewValue: 'Simulacro de examen'}
  ];
  asignaturas = [
    {value: 'naturales', viewValue: 'Ciencias Naturales'},
    {value: 'sociales', viewValue: 'Sociales y ciudadanas'},
    {value: 'lectura', viewValue: 'Lectura crítica'},
    {value: 'matematicas', viewValue: 'Matemáticas'}
  ];
  duraciones = [
    {value: 'largo', viewValue: 'Largo'},
    {value: 'medio', viewValue: 'Medio'},
    {value: 'corto', viewValue: 'Corto'}
  ];

  selection = new SelectionModel<any>(true, []); // Selección de preguntas de simulacro de prueba
  selectionSesion1area1 = new SelectionModel<any>(true, []); //Selección de preguntas de la primera área
  selectionSesion1area2 = new SelectionModel<any>(true, []); //Selección de preguntas de la segunda área
  selectionSesion2area1 = new SelectionModel<any>(true, []); //Selección de preguntas de la tercera área
  selectionSesion2area2 = new SelectionModel<any>(true, []); //Selección de preguntas de la cuarta área

  // Métodos
  constructor(private _questionService: QuestionService,
              private _testService: TestService,
              private _userService: UserService,
              private _route: ActivatedRoute,
              private _router: Router,
              public dialog: MatDialog,
              private openSnack: OpenSnackBarService) {
  }

  ngOnInit() {
    this.test = new Test(null, null, null, null, [],[], [], [], [], []);
    this.idEdit = this._route.snapshot.paramMap.get('id');
    this.token = this._userService.getTokenUser();
    this.idUser = this._userService.getIdentity()._id;
    this.getTestEdit();
  }

  cleanArray(area: string){
    // console.log('entra');
    var toRemove = document.getElementById("messageWarn");
    var toRemove11 = document.getElementById("messageWarn11");
    var toRemove21 = document.getElementById("messageWarn21");
    var toRemove12 = document.getElementById("messageWarn12");
    var toRemove22 = document.getElementById("messageWarn22");
    toRemove.childNodes[0]? toRemove.removeChild(toRemove.childNodes[0]): console.log('no tiene hijos');
    if(toRemove11 !== null ){
      toRemove11.childNodes[0]? toRemove11.removeChild(toRemove11.childNodes[0]): console.log('no tiene hijos');
    }
    if(toRemove21 !== null){
      toRemove21.childNodes[0]? toRemove21.removeChild(toRemove21.childNodes[0]): console.log('no tiene hijos');
    }
    if( toRemove12 !== null ){
      toRemove12.childNodes[0]? toRemove12.removeChild(toRemove12.childNodes[0]): console.log('no tiene hijos');
    }
    if( toRemove22 !== null ){
      toRemove22.childNodes[0]? toRemove22.removeChild(toRemove22.childNodes[0]): console.log('no tiene hijos');
    }
    switch(area){
      case '11':
        this.selectionSesion1area1.clear();
        break;
      case '12':
        this.selectionSesion1area2.clear();
        break;
      case '21':
        this.selectionSesion2area1.clear();
        break;
      case '22':
        this.selectionSesion2area2.clear();
        break;
      case 'todo':
        this.selectionSesion1area1.clear();
        this.selectionSesion1area2.clear();
        this.selectionSesion2area1.clear();
        this.selectionSesion2area2.clear();
        this.selection.clear();
        break;
      case 'solo':
      this.selection.clear();
    }

  }
   getTestEdit(){
     if (this.idEdit) {
       this._testService.getTestId(this.idEdit, this.token, this.idUser).subscribe(
         response => {
           if(response.state === true){
             this.test = response.test;
             if(this.test.type === 'prueba'){
               this.questionsCheck = this.test.session1Area1;
               this.getQuestionsType(this.test.subType);
             }else{
               this.area11 = this.test.sess1SubType[0];
               this.area21 = this.test.sess1SubType[1];
               this.area12 = this.test.sess2SubType[0];
               this.area22 = this.test.sess2SubType[1];
               this.questionsCheck1 = this.test.session1Area1;
               this.questionsCheck2 = this.test.session1Area2;
               this.questionsCheck3 = this.test.session2Area1;
               this.questionsCheck4 = this.test.session2Area2;
               this.getQuestionsType(this.area11);
               this.getQuestionsType(this.area21);
               this.getQuestionsType(this.area12);
               this.getQuestionsType(this.area22);
             }
           }else if(response.state === 3){
            var message = "Error de autenticación."
            this.openSnack.openSnackBar(message);
            localStorage.clear();
            this._router.navigate(['/login']);
          }
         },
         error => {
           const errorMessage = <any>error;
           console.log(errorMessage);
           console.log('Error en el servidor');

         }
       );
     }
   }

   getNumberQuestionsExamen(){
    switch (this.test.subType) {
      case 'corto':
      switch(this.area11){
        case 'lectura':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>6</b> preguntas</p></div>"
          document.getElementById("messageWarn11").innerHTML =  newHTML;
        break;
        case 'naturales':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>9</b> preguntas</p></div>"
          document.getElementById("messageWarn11").innerHTML =  newHTML;
          break;
        case 'matematicas':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>8</b> preguntas</p></div>"
            document.getElementById("messageWarn11").innerHTML =  newHTML;
        break;
        case 'sociales':
        var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>8</b> preguntas</p></div>"
          document.getElementById("messageWarn11").innerHTML =  newHTML;
      }
      switch(this.area21){
        case 'lectura':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>6</b> preguntas</p></div>"
          document.getElementById("messageWarn21").innerHTML =  newHTML;
        break;
        case 'naturales':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>9</b> preguntas</p></div>"
            document.getElementById("messageWarn21").innerHTML =  newHTML;
        break;
        case 'matematicas':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>8</b> preguntas</p></div>"
            document.getElementById("messageWarn21").innerHTML =  newHTML;
        break;
        case 'sociales':
        var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>8</b> preguntas</p></div>"
          document.getElementById("messageWarn21").innerHTML =  newHTML;
      }
      switch(this.area12){
        case 'lectura':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>6</b> preguntas</p></div>"
          document.getElementById("messageWarn12").innerHTML =  newHTML;
        break;
        case 'naturales':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>9</b> preguntas</p></div>"
            document.getElementById("messageWarn12").innerHTML =  newHTML;
        break;
        case 'matematicas':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>8</b> preguntas</p></div>"
            document.getElementById("messageWarn12").innerHTML =  newHTML;
        break;
        case 'sociales':
        var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>8</b> preguntas</p></div>"
          document.getElementById("messageWarn12").innerHTML =  newHTML;
      }
      switch(this.area22){
        case 'lectura':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>6</b> preguntas</p></div>"
          document.getElementById("messageWarn22").innerHTML =  newHTML;
        break;
        case 'naturales':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>9</b> preguntas</p></div>"
            document.getElementById("messageWarn22").innerHTML =  newHTML;
        break;
        case 'matematicas':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>8</b> preguntas</p></div>"
            document.getElementById("messageWarn22").innerHTML =  newHTML;
        break;
        case 'sociales':
        var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>8</b> preguntas</p></div>"
          document.getElementById("messageWarn22").innerHTML =  newHTML;
      }
      break;
      case 'medio':
      switch(this.area11){
        case 'lectura':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>12</b> preguntas</p></div>"
          document.getElementById("messageWarn11").innerHTML =  newHTML;
        break;
        case 'naturales':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>17</b> preguntas</p></div>"
            document.getElementById("messageWarn11").innerHTML =  newHTML;
        break;
        case 'matematicas':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>17</b> preguntas</p></div>"
            document.getElementById("messageWarn11").innerHTML =  newHTML;
        break;
        case 'sociales':
        var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>15</b> preguntas</p></div>"
          document.getElementById("messageWarn11").innerHTML =  newHTML;
      }
      switch(this.area21){
        case 'lectura':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>12</b> preguntas</p></div>"
          document.getElementById("messageWarn21").innerHTML =  newHTML;
        break;
        case 'naturales':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>17</b> preguntas</p></div>"
            document.getElementById("messageWarn21").innerHTML =  newHTML;
        break;
        case 'matematicas':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>17</b> preguntas</p></div>"
            document.getElementById("messageWarn21").innerHTML =  newHTML;
        break;
        case 'sociales':
        var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>15</b> preguntas</p></div>"
          document.getElementById("messageWarn21").innerHTML =  newHTML;
      }
      switch(this.area12){
        case 'lectura':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>12</b> preguntas</p></div>"
          document.getElementById("messageWarn12").innerHTML =  newHTML;
        break;
        case 'naturales':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>17</b> preguntas</p></div>"
            document.getElementById("messageWarn12").innerHTML =  newHTML;
        break;
        case 'matematicas':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>17</b> preguntas</p></div>"
            document.getElementById("messageWarn12").innerHTML =  newHTML;
        break;
        case 'sociales':
        var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>15</b> preguntas</p></div>"
          document.getElementById("messageWarn12").innerHTML =  newHTML;
      }
      switch(this.area22){
        case 'lectura':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>12</b> preguntas</p></div>"
          document.getElementById("messageWarn22").innerHTML =  newHTML;
        break;
        case 'naturales':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>17</b> preguntas</p></div>"
            document.getElementById("messageWarn22").innerHTML =  newHTML;
        break;
        case 'matematicas':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>17</b> preguntas</p></div>"
            document.getElementById("messageWarn22").innerHTML =  newHTML;
        break;
        case 'sociales':
        var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>15</b> preguntas</p></div>"
          document.getElementById("messageWarn22").innerHTML =  newHTML;
      }
      break;
      case 'largo':
      switch(this.area11){
        case 'lectura':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>25</b> preguntas</p></div>"
          document.getElementById("messageWarn11").innerHTML =  newHTML;
        break;
        case 'naturales':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>37</b> preguntas</p></div>"
            document.getElementById("messageWarn11").innerHTML =  newHTML;
        break;
        case 'matematicas':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>34</b> preguntas</p></div>"
            document.getElementById("messageWarn11").innerHTML =  newHTML;
        break;
        case 'sociales':
        var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>30</b> preguntas</p></div>"
          document.getElementById("messageWarn11").innerHTML =  newHTML;
      }
      switch(this.area21){
        case 'lectura':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>25</b> preguntas</p></div>"
          document.getElementById("messageWarn21").innerHTML =  newHTML;
        break;
        case 'naturales':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>37</b> preguntas</p></div>"
            document.getElementById("messageWarn21").innerHTML =  newHTML;
        break;
        case 'matematicas':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>34</b> preguntas</p></div>"
            document.getElementById("messageWarn21").innerHTML =  newHTML;
        break;
        case 'sociales':
        var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>30</b> preguntas</p></div>"
          document.getElementById("messageWarn21").innerHTML =  newHTML;
      }
      switch(this.area12){
        case 'lectura':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>25</b> preguntas</p></div>"
          document.getElementById("messageWarn12").innerHTML =  newHTML;
        break;
        case 'naturales':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>37</b> preguntas</p></div>"
            document.getElementById("messageWarn12").innerHTML =  newHTML;
        break;
        case 'matematicas':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>34</b> preguntas</p></div>"
            document.getElementById("messageWarn12").innerHTML =  newHTML;
        break;
        case 'sociales':
        var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>30</b> preguntas</p></div>"
          document.getElementById("messageWarn12").innerHTML =  newHTML;
      }
      switch(this.area22){
        case 'lectura':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>25</b> preguntas</p></div>"
          document.getElementById("messageWarn22").innerHTML =  newHTML;
        break;
        case 'naturales':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>37</b> preguntas</p></div>"
            document.getElementById("messageWarn22").innerHTML =  newHTML;
        break;
        case 'matematicas':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>34</b> preguntas</p></div>"
            document.getElementById("messageWarn22").innerHTML =  newHTML;
        break;
        case 'sociales':
        var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>30</b> preguntas</p></div>"
          document.getElementById("messageWarn22").innerHTML =  newHTML;
      }
    }
   }

   getNumberQuestions(){
       switch (this.test.subType) {
         case 'lectura':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>12</b> preguntas</p></div>"
          document.getElementById("messageWarn").innerHTML =  newHTML;
          break;
         case 'naturales':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>17</b> preguntas</p></div>"
            document.getElementById("messageWarn").innerHTML =  newHTML;
          break;
         case 'matematicas':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>17</b> preguntas</p></div>"
            document.getElementById("messageWarn").innerHTML =  newHTML;
          break;
         case 'sociales':
          var newHTML = "<div id='warn' style='background-color: #fd9726; height: 50px;'><p style='text-align:center; justify-content:center'>Debe escoger <b>12</b> preguntas</p></div>"
          document.getElementById("messageWarn").innerHTML =  newHTML;
       }
   }
  checkNumberQuestions(){
    if(this.test.type === 'prueba'){
      this.test.session1Area1.length === 2? this.dsPrueba = false: this.dsPrueba = true;
    }else{
      // switch(this.test.subType){
      //   case 'corto':
      //     switch(this.area11){
      //       case 'lectura'
      //     }
      //   break;
      //   case 'medio':
      //   break;
      //   case 'largo':
      //   break;
      // }
    }

  }
  onSubmit() {
    this.selection.selected.forEach(question => this.questionsAdd.push(question._id));
    this.selectionSesion1area1.selected.forEach(question => this.session1area1.push(question._id));
    this.selectionSesion1area2.selected.forEach(question => this.session1area2.push(question._id));
    this.selectionSesion2area1.selected.forEach(question => this.session2area1.push(question._id));
    this.selectionSesion2area2.selected.forEach(question => this.session2area2.push(question._id));
    if(this.test.type === 'prueba'){
      this.test.sess1SubType = null;
      this.test.sess2SubType = null;
      this.test.session1Area1 = this.questionsAdd;
      this.test.session1Area2 = null;
      this.test.session2Area1 = null;
      this.test.session2Area2 = null;
    }else{
      this.test.sess1SubType[0] = this.area11;
      this.test.sess1SubType[1] = this.area21;
      this.test.sess2SubType[0] = this.area12;
      this.test.sess2SubType[1] = this.area22;
      this.test.session1Area1 = this.session1area1;
      this.test.session1Area2 = this.session1area2;
      this.test.session2Area1 = this.session2area1;
      this.test.session2Area2 = this.session2area2;
    }
    console.log(this.test);
    if(this.idEdit){
      this._testService.updateTest(this.test, this.token, this.idUser).subscribe(
        response => {
          if(response.state === true){
            this._router.navigate(['/admin','0']);
          }else if(response.state === 3){
            var message = "Error de autenticación."
            this.openSnack.openSnackBar(message);
            localStorage.clear();
            this._router.navigate(['/login']);
          }
        },
        error => {
          const errorMessage = <any>error;
          console.log(errorMessage);
        }
      );
    }else{

      this._testService.createTest(this.test, this.token, this.idUser).subscribe(
        response => {
          if (response.state === true) {
            this._router.navigate(['/admin', '0']);
          } else if(response.state === 3){
            var message = "Error de autenticación."
            this.openSnack.openSnackBar(message);
            localStorage.clear();
            this._router.navigate(['/login']);
          }
        },
        error => {
          const errorMessage = <any>error;
          console.log(errorMessage);
        }
      );
    }
  }

  getQuestionsType(type: string) {
     this._questionService.getQuestionsType(type, this.token, this.idUser).subscribe(
        response => {
          if (response.state === true) {
            this.questions = response.questions;
            if(this.test.type === 'prueba'){
              if(this.idEdit){
                for(let question of this.questions){
                  for(let check of this.questionsCheck){
                    if(question._id === check._id){
                      this.selection.select(question);
                    }
                   }
                }
              }
            }else{
              switch(type){
                case 'naturales':
                  this.quesNaturales = this.questions;
                  if(this.area11 === 'naturales'){
                    for(let ques of this.quesNaturales){
                      for(let check of this.questionsCheck1){
                        if(ques._id === check._id){
                          this.selectionSesion1area1.select(ques);
                        }
                      }
                    }
                  }else if(this.area21 === 'naturales'){
                    for(let ques of this.quesNaturales){
                      for(let check of this.questionsCheck2){
                        if(ques._id === check._id){
                          this.selectionSesion1area2.select(ques);
                        }
                      }
                    }
                  }else if(this.area12 === 'naturales'){
                    for(let ques of this.quesNaturales){
                      for(let check of this.questionsCheck3){
                        if(ques._id === check._id){
                          this.selectionSesion2area1.select(ques);
                        }
                      }
                    }
                  }
                  else if(this.area22 === 'naturales'){
                    for(let ques of this.quesNaturales){
                      for(let check of this.questionsCheck4){
                        if(ques._id === check._id){
                          this.selectionSesion2area2.select(ques);
                        }
                      }
                    }
                  }
                  break;
                case 'sociales':
                  this.quesSociales = this.questions;
                  if(this.area11 === 'sociales'){
                    for(let ques of this.quesSociales){
                      for(let check of this.questionsCheck1){
                        if(ques._id === check._id){
                          this.selectionSesion1area1.select(ques);
                        }
                      }
                    }
                  }else if(this.area21 === 'sociales'){
                    for(let ques of this.quesSociales){
                      for(let check of this.questionsCheck2){
                        if(ques._id === check._id){
                          this.selectionSesion1area2.select(ques);
                        }
                      }
                    }
                  }else if(this.area12 === 'sociales'){
                    for(let ques of this.quesSociales){
                      for(let check of this.questionsCheck3){
                        if(ques._id === check._id){
                          this.selectionSesion2area1.select(ques);
                        }
                      }
                    }
                  }
                  else if(this.area22 === 'sociales'){
                    for(let ques of this.quesSociales){
                      for(let check of this.questionsCheck4){
                        if(ques._id === check._id){
                          this.selectionSesion2area2.select(ques);
                        }
                      }
                    }
                  }
                  break;
                case 'lectura':
                  this.quesLectura = this.questions;
                  if(this.area11 === 'lectura'){
                    for(let ques of this.quesLectura){
                      for(let check of this.questionsCheck1){
                        if(ques._id === check._id){
                          this.selectionSesion1area1.select(ques);
                        }
                      }
                    }
                  }else if(this.area21 === 'lectura'){
                    for(let ques of this.quesLectura){
                      for(let check of this.questionsCheck2){
                        if(ques._id === check._id){
                          this.selectionSesion1area2.select(ques);
                        }
                      }
                    }
                  }else if(this.area12 === 'lectura'){
                    for(let ques of this.quesLectura){
                      for(let check of this.questionsCheck3){
                        if(ques._id === check._id){
                          this.selectionSesion2area1.select(ques);
                        }
                      }
                    }
                  }
                  else if(this.area22 === 'lectura'){
                    for(let ques of this.quesLectura){
                      for(let check of this.questionsCheck4){
                        if(ques._id === check._id){
                          this.selectionSesion2area2.select(ques);
                        }
                      }
                    }
                  }
                  break;
                case 'matematicas':
                  this.quesMatematicas = this.questions;
                  if(this.area11 === 'matematicas'){
                    for(let ques of this.quesMatematicas){
                      for(let check of this.questionsCheck1){
                        if(ques._id === check._id){
                          this.selectionSesion1area1.select(ques);
                        }
                      }
                    }
                  }else if(this.area21 === 'matematicas'){
                    for(let ques of this.quesMatematicas){
                      for(let check of this.questionsCheck2){
                        if(ques._id === check._id){
                          this.selectionSesion1area2.select(ques);
                        }
                      }
                    }
                  }else if(this.area12 === 'matematicas'){
                    for(let ques of this.quesMatematicas){
                      for(let check of this.questionsCheck3){
                        if(ques._id === check._id){
                          this.selectionSesion2area1.select(ques);
                        }
                      }
                    }
                  }
                  else if(this.area22 === 'matematicas'){
                    for(let ques of this.quesMatematicas){
                      for(let check of this.questionsCheck4){
                        if(ques._id === check._id){
                          this.selectionSesion2area2.select(ques);
                        }
                      }
                    }
                  }
              }
            }
          }else if(response.state === 3){
            var message = "Error de autenticación."
            this.openSnack.openSnackBar(message);
            localStorage.clear();
            this._router.navigate(['/login']);
          }
        },
        error => {
          const errorMessage = <any>error;
          console.log(errorMessage);
        }
      );
  }

  getQuestions(){
    this._questionService.getQuestions(this.token, this.idUser).subscribe(
      response => {
        if(response.state === true && this.questionsArray.length > 0){
          for(let question of this.questionsArray){
            if(question._id){
              console.log('existe');
            }
          }
        }else if(response.state === true){
          for(var i = 0; i < response.questions.length; i++){
            for(let j = 0; j < response.questions[i].preguntas.length; j++){
              this.questionsArray.push(response.questions[i].preguntas[j]);
            }
          }
        }else if(response.state === 3){
          var message = "Error de autenticación."
          this.openSnack.openSnackBar(message);
          localStorage.clear();
          this._router.navigate(['/login']);
        }

      },
      error => {
        const errorMessage = <any>error;
        console.log(errorMessage);
      }
      );
    }

  openDialogInformation(question: any){
    document.getElementById("btnID").addEventListener("click", function(event){
      event.preventDefault()
    });
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '950px';
    dialogConfig.data = {question: question}
    const dialogRef = this.dialog.open(DialogInformationComponent, dialogConfig);

  }
}

@Component({
  selector: 'app-dialogSection-component',
  templateUrl: './dialogInformation.html'
})
@Injectable()
export class DialogInformationComponent implements OnInit{

  public question: any;
  public url = environment.url;

  constructor(public dialogRef: MatDialogRef<DialogInformationComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {

    this.question = data.question;
  }

    ngOnInit(){
      this.getImages();
    }
    getImages(){
      if(this.question.image !== undefined && this.question.image !== null){
        var imageQ = this.url + this.question.image;
        document.getElementById("imageQ").innerHTML = "<b>Imagen:</b><br/><img id='imgQ' width='115px' height='130px'/>";
        document.getElementById("imgQ").setAttribute("src", imageQ);
      }
      if(this.question.imageAnswerA !== undefined && this.question.imageAnswerA !== null){
        var imageA = this.url + this.question.imageAnswerA;
        document.getElementById("imageA").innerHTML = "<b>Imagen A:</b><br/><img id='imgA' width='115px' height='130px'/>";
        document.getElementById("imgA").setAttribute("src", imageA);
      }
      if(this.question.imageAnswerB !== undefined && this.question.imageAnswerB !== null){
        var imageB = this.url +this.question.imageAnswerB;
        document.getElementById("imageB").innerHTML = "<b>Imagen B:</b><br/><img id='imgB' width='115px' height='130px'/>";
        document.getElementById("imgB").setAttribute("src", imageB);
      }
      if(this.question.imageAnswerA !== undefined && this.question.imageAnswerB !== null){
        var imageC = this.url +this.question.imageAnswerC;
        document.getElementById("imageC").innerHTML = "<b>Imagen C:</b><br/><img id='imgC' width='115px' height='130px'/>";
        document.getElementById("imgC").setAttribute("src", imageC);
      }
      if(this.question.imageAnswerA !== undefined && this.question.imageAnswerB !== null){
        var imageD = this.url +this.question.imageAnswerD;
        document.getElementById("imageD").innerHTML = "<b>Imagen D:</b><br/><img id='imgD' width='115px' height='130px'/>";
        document.getElementById("imgD").setAttribute("src", imageD);
      }
    }
    ngAfterViewInit(){
    }

    onNoClick(): void {
    this.dialogRef.close();
  }
}
