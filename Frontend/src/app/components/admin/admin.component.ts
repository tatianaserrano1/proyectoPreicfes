import { environment } from './../../../environments/environment';
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],
  providers: [UserService]
})
export class AdminComponent implements OnInit {

  // Atributos
  public activeTab: number;
  public identity: any;
  public type;
  public url = environment.url;

  constructor(private _route: ActivatedRoute, private _userService: UserService) {}

   // Servicios
  ngOnInit() {
    this.activeTab = parseInt(this._route.snapshot.paramMap.get('tab'));
    this.identity = this._userService.getIdentity();
    this.type = this.identity.tipo;
  }
}
