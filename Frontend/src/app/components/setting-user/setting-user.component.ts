import { Component, OnInit } from '@angular/core';
import {User} from '../../classes/user';
import {UserService} from '../../services/user.service';
import * as sha512 from 'js-sha512';
import { FormsModule, ReactiveFormsModule, FormGroup } from '@angular/forms';
import { FormBuilder, Validators, FormControl } from '../../../../node_modules/@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { OpenSnackBarService } from './../../services/openSnackBar.service';
import { environment } from './../../../environments/environment';

@Component({
  selector: 'app-setting-user',
  templateUrl: './setting-user.component.html',
  styleUrls: ['./setting-user.component.css'],
  providers: [UserService, OpenSnackBarService]
})
export class SettingUserComponent implements OnInit {

  // Atributos
  public token: string;
  public user: User;
  public identity: any;
  public update: any[];
  public safe: string;
  public type;
  public imageProfile;
  public maxDate = new Date();
  public url: string;

  generos = [
    {value: 'hombre', viewValue: 'Hombre'},
    {value: 'mujer', viewValue: 'Mujer'},
  ];

  setForm = this.fb.group({
    name: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
    email: '',
    surname: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
    password: ['', Validators.minLength(6)],
    birthdate: ['', Validators.required],
    gender: ['', Validators.required],
    profilePicture: [''],
    confirmation: ['', Validators.minLength(6)]}, {validator: this.checkPassword}
    );

  // Métodos
  constructor(private _userService: UserService,
              private fb: FormBuilder,
              private _route: ActivatedRoute,
              private _router: Router,
              private openSnack: OpenSnackBarService) {
    this.user = new User(null, null, null, null, null, null, null, null);
   }

   ngOnInit() {
    this.url =  environment.url;
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getTokenUser();
    this.identity.token = this._userService.getTokenUser();
    this.type = this._route.snapshot.paramMap.get('type');
    this.getImageProfile();
  }

  checkPassword(group: FormGroup){
    let pass = group.controls.password.value;
    let confirm = group.controls.confirmation.value;
    // this.confirmation = this.regForm.get('confirmation');
    return pass === confirm ? null : {notsame: true}
  }

  public fileToUpload: File;
   changeListener(fileInput: any){
    this.fileToUpload = fileInput.target.files[0];
    console.log(this.fileToUpload);
    var reader = new FileReader();
    reader.readAsDataURL(this.fileToUpload);
    reader.onload = ()=> {
    var dataUrl = reader.result;
    this.imageProfile = dataUrl;
    console.log(this.imageProfile);
    document.getElementById("profile").setAttribute("src", this.imageProfile);
   };
    reader.onerror = function (error) {
    console.log('Error: ', error);
    };
  }

  getImageProfile(){
    if(this.identity.picture !== null && this.identity.picture !== undefined && !this.identity.picture.startsWith('data')){
      document.getElementById("profile").setAttribute("src", this.url + this.identity.picture);
    }else{
      document.getElementById("profile").setAttribute("src", "../../../assets/imagesIcons/avatar.svg");
    }
  }
  onSubmit() {
    // Para enviar la nueva contraseña encriptada o nula si no la cambia
    if (this.identity.password !== '') {
      this.safe = sha512(this.identity.password);
      this.identity.password = this.safe;
    }else if(this.identity.password === ''){
      this.identity.password = null;
    }
    this.identity.picture = this.imageProfile;
    this._userService.updateUser(this.identity).subscribe(
      response => {
        console.log(response);
        if(response.state === true){
          this._userService.getDataUser(this.identity._id, this.token).subscribe(
            response =>{
              if(response.state === 1){
                this.identity = response.user;
                this.identity.password = '';
                localStorage.setItem('identity', JSON.stringify(this.identity));
              }
            },
            error => {
              const errorMessage = <any>error;
              console.log(errorMessage);
            }
          );
          // document.getElementById("profile").setAttribute("src", "../../../assets/imagesIcons/avatar.svg");
          //this.identity.password = '';
          //localStorage.setItem('identity', JSON.stringify(this.identity));
          if(this.type === 'student'){
            this._router.navigate(['/index']);
          }else if(this.type === 'admin'){
            this._router.navigate(['/admin', '0']);
          }
          //location.reload();
        }else if(response.state === 3){
          var message = "Error de autenticación."
          this.openSnack.openSnackBar(message);
          localStorage.clear();
          this._router.navigate(['/login']);
        }
      },
      error => {
        const errorMessage = <any>error;
        console.log(errorMessage);
      }
    );
  }
}
