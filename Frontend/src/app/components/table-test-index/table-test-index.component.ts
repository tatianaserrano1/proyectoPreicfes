import { Component, OnInit, ViewChild, Input} from '@angular/core';
import {MatTableDataSource, MatPaginator, MatSort} from '@angular/material';
import {UserService} from '../../services/user.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

export interface testData {
  id: any;
  position: number;
  subType: string;
  subType2: string;
  try: number;
}

@Component({
  selector: 'app-table-test-index',
  templateUrl: './table-test-index.component.html',
  styleUrls: ['./table-test-index.component.css'],
  providers: [UserService]

})
export class TableTestIndexComponent implements OnInit {

  @Input() idUser;
  public identity: any;
  public token: string;
  public type;
  public pruebasR: any[] = [];
  public pruebasNR: any[] = [];
  public testsArray: any[] =[];
  displayedColumns: string[] = ['position', 'subType', 'try', 'button', 'buttonResults'];
  dataSource: MatTableDataSource<testData>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private _userService: UserService) {
    this.dataSource  = new MatTableDataSource<testData>([]);

   }

  ngOnInit() {
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getTokenUser();
    this.type = this.identity.tipo;
    this.getTests();
  }

  applyFilter(filterValue: string){
    filterValue = filterValue.trim(); //Elimina espacios en blanco
    filterValue = filterValue.toLowerCase(); // coloca en minuscula
    this.dataSource.filter = filterValue;

  }

  getTests(){
    if(!this.idUser){
      this.pruebasR = this.identity.pruebasR;
      this.pruebasNR = this.identity.pruebasNR;
    }else{
      this._userService.getUserById(this.identity._id, this.idUser, this.token).subscribe(
        response =>{

        },
        error => {
          const errorMessage = <any>error;
          console.log(errorMessage);
        }
      );
    }
   //Llena el array que se presenta en la tabla de simulacros realizados
   var y = 0;
   for(let test of this.pruebasR){
     if(test._id==='naturales'){
       for(let nat of test.pruebas){
         this.testsArray.push({
           id: nat._id,
           position: y = y+1,
           subType: 'Ciencias Naturales',
           subType2: test._id,
           try: nat.intentos.length
         });
       }
     }
     if(test._id==='sociales'){
       for(let soc of test.pruebas){
         this.testsArray.push({
           id: soc._id,
           position: y = y+1,
           subType: 'Sociales y ciudadanas',
           subType2: test._id,
           try: soc.intentos.length
         });
       }
     }
     if(test._id==='lectura'){
       for(let lec of test.pruebas){
         this.testsArray.push({
           id: lec._id,
           position: y = y+1,
           subType: 'Lectura Crítica',
           subType2: test._id,
           try: lec.intentos.length
         });
       }
     }
     if(test._id==='matematicas'){
       for(let mat of test.pruebas){
         this.testsArray.push({
           id: mat._id,
           position: y = y+1,
           subType: 'Matemáticas',
           subType2: test._id,
           try: mat.intentos.length
         });
       }
     }
   }
   this.dataSource = new MatTableDataSource(this.testsArray);
   this.dataSource.paginator = this.paginator;
   this.dataSource.sort = this.sort;
  }


}
