import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsTriesComponent } from './results-tries.component';

describe('ResultsTriesComponent', () => {
  let component: ResultsTriesComponent;
  let fixture: ComponentFixture<ResultsTriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultsTriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsTriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
