import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-results-tries',
  templateUrl: './results-tries.component.html',
  styleUrls: ['./results-tries.component.css']
})
export class ResultsTriesComponent implements OnInit {

  public idTest: string;
  public subTypeTest: string;

  constructor(private _route: ActivatedRoute, private _router: Router) {
    this.idTest =  this._route.snapshot.paramMap.get('id');
    this.subTypeTest =  this._route.snapshot.paramMap.get('subType');
   }

  ngOnInit() {
  }

}
