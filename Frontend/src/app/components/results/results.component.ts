import { Component, OnInit } from '@angular/core';
import { UserService } from './../../services/user.service';
import { TestService } from './../../services/test.service';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { environment } from './../../../environments/environment';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css'],
  providers: [TestService, UserService]
})
export class ResultsComponent implements OnInit {

  //Atributos
  public url= environment.url;
  public identity: any;
  public test: any;
  public link; //Enlace de la pregunta
  public linkA; //Enlace de la respuesta a
  public linkB; //Enlace de la respuesta b
  public linkC; //Enlace de la respuesta c
  public linkD; //Enlace de la respuesta d
  public idQuestion: string;
  public session: number;
  public questions: any[];
  public questionsS1: any[] = [];
  public questionsS2: any[] = [];
  public question: any;
  public actual: number;
  public indice: number;
  public type;
  public token: string;
  public x: number; // contador del numero de pregunta
  public equal: boolean = false;
  public try;
  public answers: Map<string, string>;
  public answers2: Map<string, string>;
  public answerUser: string;
  public intento;

  //Métodos
  constructor(private _testService: TestService,
    private _userService: UserService,
    private _route: ActivatedRoute,
    private _router: Router) {
    this.idQuestion = this._route.snapshot.paramMap.get('question');
    this.session = parseInt(this._route.snapshot.paramMap.get('session'));
    this.intento = this._route.snapshot.paramMap.get('try');
   }

  ngOnInit() {
    this.identity = this._userService.getIdentity();
    // this.try = this._testService.getTry();
    this.answers = new Map(JSON.parse(localStorage.getItem('answers1')));
    this.answers2 = new Map(JSON.parse(localStorage.getItem('answers2')));
    this.getTest();
    this.getQuestion(this.idQuestion);
    this.getImages();
    this.token = this._userService.getTokenUser();
  }

  //Inserta el video correspondiente al enlace de youtube obtenido
  insertVideo(option: string){
    if(this.question.subject !== 'lectura'){
    document.getElementById("section-video").setAttribute("style", "visibility: visible;");
    switch(option){
      case "q" :
        document.getElementById("vid").setAttribute("src", this.link);
        break;
      case "a":
        document.getElementById("vid").setAttribute("src", this.linkA);
        break;
      case "b":
        document.getElementById("vid").setAttribute("src", this.linkB);
        break;
      case "c":
        document.getElementById("vid").setAttribute("src", this.linkC);
        break;
      case "d":
        document.getElementById("vid").setAttribute("src", this.linkD);
    }
  }else{
    document.getElementById("section-video").setAttribute("style", "display:none;");
  }
 }
 goBack(){
  if(this.intento){
    this._router.navigate(['/showTables', this.intento]);
  }else{
    this._router.navigate(['showTables']);
  }
 }
  getTest(){
    if(this.intento){
      this.test = JSON.parse(localStorage.getItem('testResults'));
    }else{
      this.test = this._testService.getTestLocal();
    }
    if(this.test.type === 'prueba'){
      this.questions = this.test.session1Area1;
    }else{
      for(let question of this.test.session1Area1){
        this.questionsS1.push(question);
      }
      for(let question of this.test.session1Area2){
        this.questionsS1.push(question);
      }
      for(let question of this.test.session2Area1){
        this.questionsS2.push(question);
      }
      for(let question of this.test.session2Area2){
        this.questionsS2.push(question);
      }
    }
  }

  getQuestion(id: string){
    document.getElementById("a").style.background = "#FFFFFF";
    document.getElementById("b").style.background = "#FFFFFF";
    document.getElementById("c").style.background = "#FFFFFF";
    document.getElementById("d").style.background = "#FFFFFF";
    if(this.test.type === 'prueba'){
      for(let ques of this.questions){
        if(ques._id === id){
          this.question = ques;
        }
      }
    }else{
      if(this.session === 1){
        for(let ques of this.questionsS1){
          if(ques._id === id){
            this.question = ques;
          }
        }
      }else if(this.session === 2){
        for(let ques of this.questionsS2){
          if(ques._id === id){
            this.question = ques;
          }
        }
      }
    }
    this.linkA = this.question.linkAnswerA;
    this.linkB = this.question.linkAnswerB;
    this.linkC = this.question.linkAnswerC;
    this.linkD = this.question.linkAnswerD;
    this.link = this.question.link;
    this.answerUser = this.question.answerUser;
    var correctAnswer = this.question.correctAnswer;
     switch(correctAnswer){
      case 'A':
        document.getElementById("a").style.background = "#77DC32";
        break;
      case 'B':
        document.getElementById("b").style.background = "#77DC32";
        break;
      case 'C':
        document.getElementById("c").style.background = "#77DC32";
        break;
      case 'D':
        document.getElementById("d").style.background = "#77DC32";
    }
     if(correctAnswer !== this.answerUser){
        switch(this.answerUser){
          case 'A':
            document.getElementById("a").style.background = "#DC4632";
            break;
          case 'B':
            document.getElementById("b").style.background = "#DC4632";
            break;
          case 'C':
            document.getElementById("c").style.background = "#DC4632";
            break;
          case 'D':
            document.getElementById("d").style.background = "#DC4632";
        }
    }
    this.insertVideo('q');

    // document.getElementById("vid").setAttribute("src", this.question.link);
  }
  getImages(){
    if(this.question.image !== undefined && this.question.image !== null){
      var imageQ = this.url + this.question.image;
      document.getElementById("imageQ").innerHTML = "<b>Imagen:</b><br/><img id='imgQ' width='115px' height='130px'/>";
      document.getElementById("imgQ").setAttribute("src", imageQ);
    }
    if(this.question.imageAnswerA !== undefined && this.question.imageAnswerA !== null){
      var imageA = this.url + this.question.imageAnswerA;
      document.getElementById("imageA").innerHTML = "<img id='imgA' width='115px' height='130px'/>";
      document.getElementById("imgA").setAttribute("src", imageA);
    }
    if(this.question.imageAnswerB !== undefined && this.question.imageAnswerB !== null){
      var imageB = this.url +this.question.imageAnswerB;
      document.getElementById("imageB").innerHTML = "<img id='imgB' width='115px' height='130px'/>";
      document.getElementById("imgB").setAttribute("src", imageB);
    }
    if(this.question.imageAnswerC !== undefined && this.question.imageAnswerC !== null){
      var imageC = this.url +this.question.imageAnswerC;
      document.getElementById("imageC").innerHTML = "<img id='imgC' width='115px' height='130px'/>";
      document.getElementById("imgC").setAttribute("src", imageC);
    }
    if(this.question.imageAnswerD !== undefined && this.question.imageAnswerD !== null){
      var imageD = this.url +this.question.imageAnswerD;
      document.getElementById("imageD").innerHTML = "<img id='imgD' width='115px' height='130px'/>";
      document.getElementById("imgD").setAttribute("src", imageD);
    }
  }

}
