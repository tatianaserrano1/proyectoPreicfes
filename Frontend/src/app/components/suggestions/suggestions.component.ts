import { Component, OnInit, Inject } from '@angular/core';
import {UserService} from '../../services/user.service';
import {Router, ActivatedRoute} from '@angular/router';
import { OpenSnackBarService } from './../../services/openSnackBar.service';
import {MatDialog, MatDialogRef, MatDialogConfig, MAT_DIALOG_DATA} from '@angular/material';
import { Injectable } from '@angular/core';

@Component({
  selector: 'app-suggestions',
  templateUrl: './suggestions.component.html',
  styleUrls: ['./suggestions.component.css'],
  providers: [UserService, OpenSnackBarService]
})
export class SuggestionsComponent implements OnInit {

  public video;
  public identity;
  public token: string;
  public back:boolean;
  public dialogRef;
  constructor(private _userService: UserService,
              private _route: ActivatedRoute,
              private _router: Router,
              private openSnack: OpenSnackBarService,
              public dialog: MatDialog) {

  }

  ngOnInit() {
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getTokenUser();
    this._route.snapshot.paramMap.get('back') ? this.back = true : this.back = false;
  }

  public fileToUpload: File;
   changeListener(fileInput: any){
    this.fileToUpload = fileInput.target.files[0];
    console.log(this.fileToUpload);
    var reader = new FileReader();
    reader.readAsDataURL(this.fileToUpload);
    reader.onload = ()=> {
      this.video = reader.result;
      //console.log(this.video);
      // var dataUrl = reader.result;
    // this.video = dataUrl;
   };
    reader.onerror = function (error) {
    console.log('Error: ', error);
    };
  }

  openSpinner() {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    this.dialogRef = this.dialog.open(DialogSpinnerComponent, dialogConfig);

   }
  sendVideo(){
    console.log('token' + this.token);
    console.log('idUser' + this.identity._id);
    console.log( 'video' + this.video);
    this.openSpinner();
    this._userService.sendVideo(this.video, this.token, this.identity._id).subscribe(
      response =>{
        if(response.state === 1){
          this.dialog.closeAll();
          var message = "Su video se envió correctamente."
          this.openSnack.openSnackBar(message);
          this._router.navigate(['/index']);
        }else if(response.state === 3){
          var message = "Error de autenticación."
          this.openSnack.openSnackBar(message);
          localStorage.clear();
          this._router.navigate(['/login']);
        }
      },
      error => {
        const errorMessage = <any>error;
        console.log(errorMessage);
      }
    );
  }

}

@Component({
  selector: 'app-dialogSpinner-component',
  templateUrl: './spinnerModal.html',
})
@Injectable()
export class DialogSpinnerComponent {


  constructor(public dialogRef: MatDialogRef<DialogSpinnerComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              ) {

    }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

}
