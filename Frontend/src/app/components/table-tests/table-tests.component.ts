import { UserService } from './../../services/user.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource, MatDialog, MatDialogRef, MatDialogConfig, MatPaginator, MatSort} from '@angular/material';
import {TestService} from '../../services/test.service';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { OpenSnackBarService } from './../../services/openSnackBar.service';

export interface TestData {
  id: any;
  position: number;
  title: string;
  type: string;
  subType: string;
  state: boolean;
}
@Component({
  selector: 'app-table-tests',
  templateUrl: './table-tests.component.html',
  styleUrls: ['./table-tests.component.css'],
  providers: [TestService, UserService, OpenSnackBarService]
})
export class TableTestsComponent implements OnInit{

  // Atributos
  public tests: any[];
  public status: boolean;
  public testsArray: TestData[] = [];
  public testsDeactivate: string[] = [];
  public token: string;
  public idUser: string;
  displayedColumns: string[] = ['position', 'title', 'type', 'subType', 'deactivate'];
  dataSource: MatTableDataSource<TestData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private _testService: TestService,
              private _userService: UserService,
              public dialog: MatDialog,
              private _route: ActivatedRoute,
              private _router: Router,
              private openSnack: OpenSnackBarService) {
    this.dataSource  = new MatTableDataSource<TestData>([]);
   }

   ngOnInit(){
     this.token = this._userService.getTokenUser();
     this.idUser = this._userService.getIdentity()._id;
     this.getTests();
   }

  applyFilter(filterValue: string){
    filterValue = filterValue.trim(); //Elimina espacios en blanco
    filterValue = filterValue.toLowerCase(); // coloca en minuscula
    this.dataSource.filter = filterValue;

  }

 getTests() {
   this._testService.getTests(this.token, this.idUser).subscribe(
     response => {
       if (response.state === true) {
         this.tests = response.tests;
         var y = 0;
         for(let test of this.tests) {
          this.testsArray.push({
            id: test._id,
            position: y = y + 1,
            title: test.title,
            type: test.type === 'simulacro' ? 'examen' : test.type,
            subType: test.subType,
            state: test.state
          });
         }
         this.dataSource = new MatTableDataSource(this.testsArray);
         this.dataSource.paginator = this.paginator;
         this.dataSource.sort = this.sort;
       }else if(response.state === 3){
        var message = "Error de autenticación."
        this.openSnack.openSnackBar(message);
        localStorage.clear();
        this._router.navigate(['/login']);
      }
     },
     error => {
       const errorMessage = <any>error;
       console.log(errorMessage);
     }
   );
 }

 deactivateTest(id:string) {
  // this.selection.selected.forEach(row => this.testsDeactivate.push(row.id));
  this.testsDeactivate.push(id);
  for(let test of this.testsArray){
    if(test.id === id && test.state === true){
      this._testService.deactivateTest(this.testsDeactivate, this.token, this.idUser).subscribe(
        response => {
          if (response.state === true) {
            this.testsDeactivate = [];
            for(let test of this.testsArray){
              if(id === test.id){
                test.state = false;
              }
            }
            this._router.navigate(['/admin', '0']);
          }else if(response.state === 3){
            var message = "Error de autenticación."
            this.openSnack.openSnackBar(message);
            localStorage.clear();
            this._router.navigate(['/login']);
          }
        },
        error => {
          const errorMessage = <any>error;
          console.log(errorMessage);
          console.log('Error en el servidor');
        }
      );
    }else if(test.id === id && test.state === false){
      this._testService.activateTest(this.testsDeactivate, this.token, this.idUser).subscribe(
        response => {
          if (response.state === true) {
            this.testsDeactivate = [];
            for(let test of this.testsArray){
              if(id === test.id){
                test.state = true;
              }
            }
            this._router.navigate(['/admin', '0']);
          }else if(response.state === 3){
            var message = "Error de autenticación."
            this.openSnack.openSnackBar(message);
            localStorage.clear();
            this._router.navigate(['/login']);
          }
        },
        error => {
          const errorMessage = <any>error;
          console.log(errorMessage);
          console.log('Error en el servidor');
        }
      );
    }
  }

 }


updateTest(id) {
  this._router.navigateByUrl('/addTest/' + id);
}
 openDialog() {
  const dialogConfig = new MatDialogConfig();
  dialogConfig.disableClose = true;
  dialogConfig.autoFocus = true;
  dialogConfig.width = '250px';
  const dialogRef = this.dialog.open(DialogTestComponent, dialogConfig);

  dialogRef.afterClosed().subscribe(
    data => {
      if(data === true) {
      // this.deactivateTest();
      }
    }
  );
 }

}

@Component({
  selector: 'app-dialogTest-component',
  templateUrl: './dialogTest.html',
})
export class DialogTestComponent {
  public state: boolean;

  constructor(
    public dialogRef: MatDialogRef<DialogTestComponent>) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  deactivate() {
    this.state = true;
    this.dialogRef.close(this.state);
  }

}
