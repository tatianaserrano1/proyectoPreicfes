import { LoginObject } from './../../classes/loginObject';
import { Component, OnInit, Input, Inject} from '@angular/core';
import {UserService} from '../../services/user.service';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {MatDialog, MatDialogRef, MatDialogConfig} from '@angular/material';
import { Injectable } from '@angular/core';
import { environment } from './../../../environments/environment';
import { OpenSnackBarService } from './../../services/openSnackBar.service';

@Component({
  selector: 'app-navbar-user',
  templateUrl: './navbar-user.component.html',
  styleUrls: ['./navbar-user.component.css'],
  providers: [UserService, OpenSnackBarService]
})
export class NavbarUserComponent implements OnInit {

  //Atributos
  @Input() rol: string;
  public identity: any;
  public token: string;
  public type;
  public loginUser: LoginObject;
  public url: string;

  //Métodos
  constructor(private _userService: UserService,
              private _route: ActivatedRoute,
              private _router: Router,
              public dialog: MatDialog,
              private openSnack: OpenSnackBarService) {
    this.loginUser = new LoginObject('', '', 2);
   }

  ngOnInit() {
    this.url =  environment.url;
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getTokenUser();
    if(this.rol === 'student'){
      this.type = true;
    }else if(this.rol === 'admin'){
      this.type = false;
    }
    this.getImageProfile();
  }
  getImageProfile(){
    if(this.identity.picture !== null && this.identity.picture !== undefined && !this.identity.picture.startsWith('data')){
      document.getElementById("profile2").setAttribute("src", this.url + this.identity.picture);
    }else{
      document.getElementById("profile2").setAttribute("src", "../../../assets/imagesIcons/avatar.svg");
    }
  }
  setting(){
    if(this.type){
      this._router.navigate(['/setting', 'student']);
    }else if(!this.type){
      this._router.navigate(['/setting', 'admin']);
    }
  }
  go(){
    localStorage.removeItem('time');
    this._userService.getDataUser(this.identity._id, this.token).subscribe(
       response =>{
        if(response.state === 1){
          this.identity = response.user;
          this.identity.password = '';
          localStorage.setItem('identity', JSON.stringify(this.identity));
          //location.reload();
        }else if(response.state === 3){
          var message = "Error de autenticación."
          this.openSnack.openSnackBar(message);
          localStorage.clear();
          this._router.navigate(['/login']);
        }
      },
        error => {
          const errorMessage = <any>error;
          console.log(errorMessage);
        }
    );
    if(this.rol === 'student'){
      this._router.navigate(['/index']);
    }else if(this.rol === 'admin'){
      this._router.navigate(['/admin', '0']);
    }
  }
  openHelpDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '1200px';
    dialogConfig.height = '500px';
    const dialogRef = this.dialog.open(HelpDialogComponent, dialogConfig);
   }
   openDialogAdmin(){
    const dialogConfig2 = new MatDialogConfig();
    dialogConfig2.disableClose = true;
    dialogConfig2.autoFocus = true;
    dialogConfig2.width = '1200px';
    dialogConfig2.height = '500px';
    const dialogRef = this.dialog.open(HelpDialogAdminComponent, dialogConfig2);
   }

  logOut(){
    localStorage.clear();
    this.identity = null;
    this.token = null;
    this._router.navigate(['/']);
  }

}

@Component({
  selector: 'app-helpDialog-component',
  templateUrl: './helpDialog.html',
})
@Injectable()
export class HelpDialogComponent implements OnInit{

  constructor(public dialogRef: MatDialogRef<HelpDialogComponent>) {
    }

    ngOnInit(){
    }
    ngAfterViewInit(){
    }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

@Component({
  selector: 'app-helpDialogAdmin-component',
  templateUrl: './helpDialogAdmin.html',
})
@Injectable()
export class HelpDialogAdminComponent implements OnInit{

  constructor(public dialogRef: MatDialogRef<HelpDialogComponent>) {
    }

    ngOnInit(){
    }
    ngAfterViewInit(){
    }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
