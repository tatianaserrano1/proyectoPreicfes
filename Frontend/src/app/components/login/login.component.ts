import { Component, OnInit} from '@angular/core';
import {UserService} from '../../services/user.service';
import { LoginObject } from '../../classes/loginObject';
import * as sha512 from 'js-sha512';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { MatSnackBar, MatSnackBarConfig } from '../../../../node_modules/@angular/material';
import {JwtHelperService} from '@auth0/angular-jwt';
import { FormsModule, ReactiveFormsModule, FormGroup } from '@angular/forms';
import { FormBuilder, Validators, FormControl } from '../../../../node_modules/@angular/forms';

import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider,
  SocialUser
} from 'angular-6-social-login';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UserService]
})

export class LoginComponent implements OnInit {

  // Atributos

  public loginUser: LoginObject;
  private userSocial: SocialUser;
  public authorized: boolean = false;
  public safe: string;
  public identity: any;
  public token: string;

  logForm = this.fb.group({
    email: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])],
    password: ['', Validators.required]
  });

  // private jwHelper: JwtHelperService = new JwtHelperService();

  // Métodos
  constructor(private _userService: UserService,
              private socialAuthService: AuthService,
              private _route: ActivatedRoute,
              private _router: Router,
              public snackBar: MatSnackBar,
              private fb: FormBuilder) {
    this.loginUser = new LoginObject('', '', 2);
    // const token = localStorage.getItem('token');
    // const token2 = this.jwHelper.decodeToken(token);
   }

  // setToken(token: any) {
  //   localStorage.setItem('token', token);
  // }
  ngOnInit() {
  }

  public onSubmit() {
    this.safe = sha512(this.loginUser.password);
    this.loginUser.password = this.safe;
    this.loginUser.flag = 2;
    this._userService.login(this.loginUser).subscribe(
      response => {
        console.log(response);
        if (response.state === 1 && response.doc_user.state === true) {
        this.identity = response.doc_user;
        this.token = response.token;
        localStorage.setItem('token', JSON.stringify(this.token));
        this.identity.password = '';
        localStorage.setItem('identity', JSON.stringify(this.identity));
        if(this.identity.tipo === 0/* && this.type==='user' */){
          this._router.navigate(['/index']);
        }else if(this.identity.tipo === 1 /*&& this.type=== 'admin'*/){
          this._router.navigate(['/admin', '0']);
        }
        } else if (response.state === 0 || response.doc_user.state === false) {
        this.loginUser.email = null;
        this.loginUser.password = null;
        this.identity = null;
        let message = "Los datos ingresados no coinciden con nuestros registros. Por favor, revise e inténtelo de nuevo."
        this.openSnackBar(message);
        }else if(response.state === 3){
          let message = "No puede iniciar sesión por este medio."
          this.openSnackBar(message);
        }else{
        alert('Error en el servidor');
        this.identity = null;
        }
      },
      error => {
        const errorMessage = <any>error;
        console.log(errorMessage);
        this.identity = null;
      }
    );
  }
  openSnackBar(message: string){
    const snackConfig = new MatSnackBarConfig();
    snackConfig.verticalPosition = 'top';
    snackConfig.duration = 8000;
    this.snackBar.open(message, "Cerrar", snackConfig);
  }

  public socialSignIn(socialPlatform: string) {
    let socialPlatformProvider;
    if (socialPlatform === 'facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialPlatform === 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log(socialPlatform + ' sign in data : ' , userData);
        // Now sign-in with userData
        // ...
        if (userData != null) {
          this.authorized = true;
          this.userSocial = userData;
          var user = {
            name: userData.name,
            email: userData.email,
            flag: 1
          }
          this._userService.login(user).subscribe(
            response =>{
              if (response.state === 1 && response.doc_user.state === true) {
                this.identity = response.doc_user;
                this.token = response.token;
                localStorage.setItem('token', JSON.stringify(this.token));
                this.identity.password = '';
                localStorage.setItem('identity', JSON.stringify(this.identity));
                if(this.identity.tipo === 0/* && this.type==='user' */){
                  this._router.navigate(['/index']);
                }else if(this.identity.tipo === 1 /*&& this.type=== 'admin'*/){
                  this._router.navigate(['/admin', '0']);
                }
                } else if (response.state === 0 || response.doc_user.state === false) {
                this.loginUser.email = null;
                this.loginUser.password = null;
                this.identity = null;
                var message = "Los datos ingresados no coinciden con nuestros registros. Por favor, revise e inténtelo de nuevo."
                this.openSnackBar(message);
                }else{
                alert('Error en el servidor');
                this.identity = null;
                }
            },
            error =>{
              const errorMessage = <any>error;
              console.log(errorMessage);
              this.identity = null;
            }
          );
       }
      }
    );
  }
  public signOut() {
    this.socialAuthService.signOut();
    this.authorized = false;
}

}
