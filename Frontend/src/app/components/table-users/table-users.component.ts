import { Component, OnInit, ViewChild} from '@angular/core';
import {SelectionModel} from '@angular/cdk/collections';
import {MatTableDataSource, MatDialog, MatDialogRef, MatDialogConfig, MatPaginator, MatSort} from '@angular/material';
import { UserService} from '../../services/user.service';
import {Router, ActivatedRoute} from '@angular/router';
import { OpenSnackBarService } from './../../services/openSnackBar.service';

export interface UserData {
  id: any;
  position: number;
  name: string;
  email: string;
  gender: string;
  state: boolean;
}
@Component({
  selector: 'app-table-users',
  templateUrl: './table-users.component.html',
  styleUrls: ['./table-users.component.css'],
  providers: [UserService, OpenSnackBarService]
})
export class TableUsersComponent {

  // Atributos
  public status: boolean;
  public users: Array<any>;
  public userState: boolean;
  public token: string;
  public idUser: string;
  displayedColumns: string[] = ['position', 'name', 'email', 'gender', 'state'];
  dataSource: MatTableDataSource<UserData>;
  usersArray: UserData[] = [];
  usersDeactivate: string[] =[];
  selection = new SelectionModel<UserData>(true, []);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  // Métodos
  constructor(private _userService: UserService,
              public dialog: MatDialog,
              private _route: ActivatedRoute,
              private _router: Router,
              private openSnack: OpenSnackBarService) {
    this.dataSource  = new MatTableDataSource<UserData>([]);
   }

   ngOnInit(){
     this.token = this._userService.getTokenUser();
     this.idUser = this._userService.getIdentity()._id;
     this.getUsers();
   }

   applyFilter(filterValue: string){
    filterValue = filterValue.trim(); //Elimina espacios en blanco
    filterValue = filterValue.toLowerCase(); // coloca en minuscula
    this.dataSource.filter = filterValue;

  }

//  Checkbox
/** Whether the number of selected elements matches the total number of rows. */
   isAllSelected() {
    const numSelected = this.selection.selected.length;

    const numRows = this.dataSource.data.length;
    if (numSelected >= 1) {
      this.status = true;
    }
    return numSelected === numRows;
  }
/** Selects all rows if they are not all selected; otherwise clear selection. */
   masterToggle() {
    this.isAllSelected() ?
       this.selection.clear() :
       this.dataSource.data.forEach(row => this.selection.select(row));
       this.status = false;
  }

 statusChange(){
   this.status = false;
 }

 getUsers() {
   this._userService.getUsers(this.token, this.idUser).subscribe(
     response => {
       if (response.state === true) {
         this.users = response.users;
         console.log(this.users);
         var y = 0;
         for(let user of this.users) {
          this.usersArray.push({
            id: user._id,
            position: y = y + 1,
            name: user.name + '  ' + user.surname,
            email: user.email,
            gender: user.gender,
            state: user.state
          });
        }
        this.dataSource = new MatTableDataSource(this.usersArray);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
       } else if(response.state === 3){
        var message = "Error de autenticación."
        this.openSnack.openSnackBar(message);
        localStorage.clear();
        this._router.navigate(['/login']);
      }
     },
     error => {
       const errorMessage = <any>error;
       console.log(errorMessage);
     }
   );
 }

 openDialog() {
  const dialogConfig = new MatDialogConfig();
  dialogConfig.disableClose = true;
  dialogConfig.autoFocus = true;
  dialogConfig.width = '250px';
  const dialogRef = this.dialog.open(DialogComponent, dialogConfig);

  dialogRef.afterClosed().subscribe(
    data => {
      if (data === true) {
      // this.deactivateUser();
      }
    }
  );
 }

 //Método que activa o desactiva un usuario
 deactivateUser(id: string) {
   //1 significa desactivar, 2 significa activar
    this.usersDeactivate.push(id);
    for(let user of this.usersArray){
      if(user.id === id && user.state === true){
        this._userService.deactivateUser(this.usersDeactivate, this.token, this.idUser).subscribe(
          response => {
            if(response.state === true){
              this.usersDeactivate = [];
                for(let user of this.usersArray){
                  if(id === user.id){
                    user.state = false;
                  }
                }
            }else if(response.state === 3){
              var message = "Error de autenticación."
              this.openSnack.openSnackBar(message);
              localStorage.clear();
              this._router.navigate(['/login']);
            }
            this._router.navigate(['/admin', '2']);
          },
          error => {
            const errorMessage = <any>error;
            console.log(errorMessage);
            console.log('Error en el servidor');
          }
        );
      }else if(user.id === id && user.state === false){
        this._userService.activateUser(this.usersDeactivate, this.token, this.idUser).subscribe(
          response =>{
            if(response.state === true){
              this.usersDeactivate = [];
              for(let user of this.usersArray){
                if(id === user.id){
                  user.state = true;
                }
              }
            }else if(response.state === 3){
              var message = "Error de autenticación."
              this.openSnack.openSnackBar(message);
              localStorage.clear();
              this._router.navigate(['/login']);
            }
            this._router.navigate(['/admin', '2']);

          },
          error => {
            const errorMessage = <any>error;
            console.log(errorMessage);
            console.log('Error en el servidor');
          }
        );

      }
    }

  }
}

@Component({
  selector: 'app-dialog-component',
  templateUrl: './dialog.html',
})
export class DialogComponent{
  public state: boolean;

  constructor(
    public dialogRef: MatDialogRef<DialogComponent>) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  deactivate(){
    this.state = true;
    this.dialogRef.close(this.state);
  }
}
