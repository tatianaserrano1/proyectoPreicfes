import { OpenSnackBarService } from './../../services/openSnackBar.service';
import { environment } from './../../../environments/environment';
import { UserService } from './../../services/user.service';
import { Component, OnInit} from '@angular/core';
import {Question} from '../../classes/question';
import {QuestionService} from '../../services/question.service';
import { FormBuilder, Validators, FormControl } from '../../../../node_modules/@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-add-question',
  templateUrl: './add-question.component.html',
  styleUrls: ['./add-question.component.css'],
  providers: [QuestionService, UserService, OpenSnackBarService]
})
export class AddQuestionComponent implements OnInit {
  public idEdit;
  public token: string;
  public idUser: string;
  public question: Question;
  public state: boolean;
  public image;
  public imageA;
  public imageB;
  public imageC;
  public imageD;
  public typeAnswer;
  public url: string;
  public valOptions: boolean = false;
  public generalTopic: any[] = [];

  types = [
    {value: 'image', viewValue: 'Imagen'},
    {value: 'text', viewValue:'Texto'}
  ]
  options = [
    {value: 'A'},
    {value: 'B'},
    {value: 'C'},
    {value: 'D'}
  ]
  asignaturas = [
    {value: 'naturales', viewValue: 'Ciencias Naturales'},
    {value: 'sociales', viewValue: 'Sociales y ciudadanas'},
    {value: 'lectura', viewValue: 'Lectura crítica'},
    {value: 'matematicas', viewValue: 'Matemáticas'}
  ];
  topicMatematica = [
    {value: 'geometria', viewValue: 'Geometría'},
    {value: 'estadistica', viewValue: 'Estadística'},
    {value: 'algebra', viewValue: 'Algebra y calculo'}
  ];
  topicSociales = [
    {value: 'violencia', viewValue: 'Violencia y sociedad'},
    {value: 'geografia', viewValue: 'Geografía'},
    {value: 'historia', viewValue: 'Historia'},
    {value: 'politica', viewValue: 'Política'}
  ];
  topicLectura = [
    {value: 'lenguaje', viewValue: 'Lenguaje'},
    {value: 'filosofia', viewValue: 'Filosofía'}
  ];
  topicNaturales = [
    {value: 'biologia', viewValue: 'Biología'},
    {value: 'fisica', viewValue: 'Física'},
    {value: 'quimica', viewValue: 'Química'},
    {value: 'cts', viewValue: 'Ciencia, tecnología y sociedad (CTS)'}
  ];

  addQuesForm = this.fb.group({

  });

  constructor(private _questionService: QuestionService,
              private _userService: UserService,
              private fb: FormBuilder,
              private _route: ActivatedRoute,
              private _router: Router,
              private openSnack: OpenSnackBarService){

    this.state = false;
  }

  ngOnInit() {
    this.question = new Question(null, null, null, null, null, null, null, null,
      null, null, null, null, null, null, null, null, null, null);
    this.url =  environment.url;
    this.idEdit = this._route.snapshot.paramMap.get('id');
    this.token = this._userService.getTokenUser();
    this.idUser = this._userService.getIdentity()._id;
    this.getQuestion();
  }
  public fileToUpload: File;
  changeListener(fileInput: any, type: string){
    this.fileToUpload = fileInput.target.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(this.fileToUpload);
    reader.onload = () => {
      var dataUrl = reader.result;
      switch(type){
        case "question":
          this.image = dataUrl;
          break;
        case "imageA":
          this.imageA = dataUrl;
          break;
        case "imageB":
          this.imageB = dataUrl;
          break;
        case "imageC":
          this.imageC = dataUrl;
          break;
        case "imageD":
          this.imageD = dataUrl;
      }
    };
    reader.onerror = function (error) {
      console.log('Error: ', error);
    };
  }

  changeSubject(event){
    console.log(event);
    switch(event){
      case 'naturales':
        this.generalTopic = this.topicNaturales;
        break;
      case 'sociales':
        this.generalTopic = this.topicSociales;
        break;
      case 'matematicas':
        this.generalTopic = this.topicMatematica;
        break;
      case 'lectura':
        this.generalTopic = this.topicLectura;
    }

  }

  getQuestion(){
    if (this.idEdit) {
      this._questionService.getQuestion(this.idEdit, this.token, this.idUser).subscribe(
        response => {
          if (response.state === true) {
            this.deleteAllImages();
            this.question = response.question;
            console.log(this.question);
            if(this.question.image !== null && this.question.image !== undefined){
              var imageQ = this.url + this.question.image;
              // document.getElementById("elementReplace").innerHTML = "<div id='imageQ'></div>";
              document.getElementById("imageQ").innerHTML = "<img id='imgQ' width='115px' height='130px'/>";
              document.getElementById("imgQ").setAttribute("src", imageQ);
            }
            if(this.question.imageAnswerA !== null && this.question.imageAnswerA !== undefined){
              var imageA = this.url + this.question.imageAnswerA;
              document.getElementById("imageA").innerHTML = "<img id='imgA' width='115px' height='130px'/>";
              document.getElementById("imgA").setAttribute("src", imageA);
            }
            if(this.question.imageAnswerB !== null && this.question.imageAnswerB !== undefined){
              var imageB = this.url +this.question.imageAnswerB;
              document.getElementById("imageB").innerHTML = "<img id='imgB' width='115px' height='130px'/>";
              document.getElementById("imgB").setAttribute("src", imageB);
            }
            if(this.question.imageAnswerC !== null && this.question.imageAnswerC !== undefined){
              var imageC = this.url +this.question.imageAnswerC;
              document.getElementById("imageC").innerHTML = "<img id='imgC' width='115px' height='130px'/>";
              document.getElementById("imgC").setAttribute("src", imageC);
            }
            if(this.question.imageAnswerD !== null && this.question.imageAnswerC !== undefined){
              var imageD = this.url +this.question.imageAnswerD;
              document.getElementById("imageD").innerHTML = "<img id='imgD' width='115px' height='130px'/>";
              document.getElementById("imgD").setAttribute("src", imageD);
            }
            // document.getElementById("cuadrado").setAttribute("src", this.question.image);
          }else if(response.state === 3){
            var message = "Error de autenticación."
            this.openSnack.openSnackBar(message);
            localStorage.clear();
            this._router.navigate(['/login']);
          }
        },
        error => {
          const errorMessage = <any>error;
          console.log(errorMessage);

        }
      );
    }
  }
  deleteAllImages(){
    this.question.image = null;
    this.question.imageAnswerA = null;
    this.question.imageAnswerB = null;
    this.question.imageAnswerC = null;
    this.question.imageAnswerD = null;

  }
  deleteImage(imageLetter: string){
    switch(imageLetter){
      case 'Q':
      document.getElementById("imageQ").remove();
      this.question.image = null;
      break;
      case 'A':
      document.getElementById("imageA").remove();
      this.question.imageAnswerA = null;
      break;
      case 'B':
      document.getElementById("imageB").remove();
      this.question.imageAnswerB = null;
      break;
      case 'C':
      document.getElementById("imageC").remove();
      this.question.imageAnswerC = null;
      break;
      case 'D':
      document.getElementById("imageD").remove();
      this.question.imageAnswerD = null;
      break;
    }
  }

  validateOptionsAnswer(){
    if((this.question.answerA === null || this.question.answerA === undefined || this.question.answerA === "" ) && (this.question.imageAnswerA === null || this.question.imageAnswerA === undefined || this.question.imageAnswerA === "")){
      return false;
    }else{
      return true;
    }
    // console.log(this.valOptions);

  }
  sendImageCorrect(){
    if(this.image === undefined && !this.idEdit){
      this.question.image = null;
    }else if(!this.idEdit){
      this.question.image = this.image;
    }else if(this.idEdit && this.image === undefined && this.question.image !== null){
      this.question.image = 'misma';
    }else if(this.idEdit){
      this.question.image = this.image;
    }

    if(this.imageA === undefined && !this.idEdit){
      this.question.imageAnswerA = null;
    }else if(!this.idEdit){
      this.question.imageAnswerA = this.imageA;
    }else if(this.idEdit && this.imageA === undefined && this.question.imageAnswerA !== null){
      this.question.imageAnswerA = 'misma';
    }else if(this.idEdit){
      this.question.imageAnswerA = this.imageA;
    }

    if(this.imageB === undefined && !this.idEdit){
      this.question.imageAnswerB = null;
    }else if(!this.idEdit){
      this.question.imageAnswerB = this.imageB;
    }else if(this.idEdit && this.imageB === undefined && this.question.imageAnswerB !== null){
      this.question.imageAnswerB = 'misma';
    }else if(this.idEdit){
      this.question.imageAnswerB = this.imageB;
    }

    if(this.imageC === undefined && !this.idEdit){
      this.question.imageAnswerC = null;
    }else if(!this.idEdit){
      this.question.imageAnswerC = this.imageC;
    }else if(this.idEdit && this.imageC === undefined && this.question.imageAnswerC !== null){
      this.question.imageAnswerC = 'misma';
    }else if(this.idEdit){
      this.question.imageAnswerC = this.imageC;
    }

    if(this.imageD === undefined && !this.idEdit){
      this.question.imageAnswerD = null;
    }else if(!this.idEdit){
      this.question.imageAnswerD = this.imageD;
    }else if(this.idEdit && this.imageD === undefined && this.question.imageAnswerD !== null){
      this.question.imageAnswerD = 'misma';
    }else if(this.idEdit){
      this.question.imageAnswerD = this.imageD;
    }
  }
  sendLinkCorrect(){
    if(this.question.subject !== 'lectura'){
      this.question.link = this.question.link.replace("watch?v=", "embed/");
      this.question.linkAnswerA = this.question.linkAnswerA.replace("watch?v=", "embed/");
      this.question.linkAnswerB = this.question.linkAnswerB.replace("watch?v=", "embed/");
      this.question.linkAnswerC = this.question.linkAnswerC.replace("watch?v=", "embed/");
      this.question.linkAnswerD = this.question.linkAnswerD.replace("watch?v=", "embed/");
    }
  }
  onSubmit() {
    this.sendImageCorrect();
    this.sendLinkCorrect();
    this.validateOptionsAnswer();
    console.log(this.question);
    if (this.idEdit) {
      this._questionService.updateQuestion(this.question, this.token, this.idUser, this.idEdit).subscribe(
        response => {
          if (response.state === true) {
            this._router.navigate(['/admin', '1']);
          }else if(response.state === 3){
            var message = "Error de autenticación."
            this.openSnack.openSnackBar(message);
            localStorage.clear();
            this._router.navigate(['/login']);
          }
        },
        error => {
          const errorMessage = <any>error;
          console.log(errorMessage);
        }
      );
    } else {
      // .split(',')[1]

      this._questionService.createQuestion(this.question, this.token, this.idUser).subscribe(
        response => {
          if (response.state === true) {
            this._router.navigate(['/admin/', '1']);
          } else if(response.state === 3){
            var message = "Error de autenticación."
            this.openSnack.openSnackBar(message);
            localStorage.clear();
            this._router.navigate(['/login']);
          }
        },
        error => {
          const errorMessage = <any>error;
          console.log(errorMessage);
        }
      );
    }

  }
}
