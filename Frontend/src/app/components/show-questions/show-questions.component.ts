import { Component, OnInit } from '@angular/core';
import {QuestionService} from '../../services/question.service';
import { UserService } from './../../services/user.service';
import { OpenSnackBarService } from './../../services/openSnackBar.service';
import {Router, ActivatedRoute, Params} from '@angular/router';

export interface questionData {
  id: string;
  text: string;
  subject: string;
  correctas: number;
  incorrectas: number;
  noRespondidas: number;
  porCorrectas: number;

}
@Component({
  selector: 'app-show-questions',
  templateUrl: './show-questions.component.html',
  styleUrls: ['./show-questions.component.css'],
  providers: [QuestionService, UserService, OpenSnackBarService]
})
export class ShowQuestionsComponent implements OnInit {

  public identity;
  public token: string;
  public questions: any[] = [];
  public dataQuestions: questionData[] = [];

  constructor(private _questionService: QuestionService,
              private _userService: UserService,
              private openSnack: OpenSnackBarService,
              private _route: ActivatedRoute,
              private _router: Router) {

              }

  ngOnInit() {
    this.questions = JSON.parse(localStorage.getItem('showQues'));
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getTokenUser();
    this.getDataQuestion();
    console.log(this.questions);
  }

  getDataQuestion(){
    for(let q of this.questions){
      console.log(q.idpregunta);
      this._questionService.getQuestion(q.idpregunta, this.token, this.identity._id).subscribe(
        response =>{
          if(response.state === true){
            this.dataQuestions.push({
              id: q.idPregunda,
              text: response.question.text,
              subject: response.question.subject,
              correctas: q.correctas,
              incorrectas: q.incorrectas,
              noRespondidas: q.noRespondidas,
              porCorrectas: q.porCorrectas
            });
          }else if(response.state === 3){
            var message = "Error de autenticación."
            this.openSnack.openSnackBar(message);
            localStorage.clear();
            this._router.navigate(['/login']);
          }
        },
        error => {
          const errorMessage = <any>error;
          console.log(errorMessage);

        }
      );

    }
  }

}
