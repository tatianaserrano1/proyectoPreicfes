import { UserService } from './../../services/user.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import {SelectionModel, DataSource} from '@angular/cdk/collections';
import {MatTableDataSource, MatDialog, MatDialogRef, MatDialogConfig, MatPaginator, MatSort} from '@angular/material';
import { Question} from '../../classes/question';
import { QuestionService } from '../../services/question.service';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { OpenSnackBarService } from './../../services/openSnackBar.service';

export interface QuestionData {
  id: any;
  position: number;
  text: string;
  subject: string;
  subSubject: string;
  state: boolean;
}
@Component({
  selector: 'app-table-questions',
  templateUrl: './table-questions.component.html',
  styleUrls: ['./table-questions.component.css'],
  providers: [QuestionService, UserService, OpenSnackBarService]

})
export class TableQuestionsComponent implements OnInit{

  // Atributos
  public questionsA: any[] = [];
  public status: boolean;
  public token: string;
  public idUser: string;
  displayedColumns: string[] = [ 'position', 'text', 'subject', 'subSubject', 'deactivate'];
  dataSource: MatTableDataSource<QuestionData>;
  questionsArray: QuestionData[] = [];
  questionsDeactivate: string[] = [];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

// Métodos
  constructor(private _questionService: QuestionService,
              private _userService: UserService,
              public dialog: MatDialog,
              private _route: ActivatedRoute,
              private _router: Router,
              private openSnack: OpenSnackBarService) {
      this.dataSource  = new MatTableDataSource<QuestionData>([]);

  }

  ngOnInit(){
    this.token = this._userService.getTokenUser();
    this.idUser = this._userService.getIdentity()._id;
    this.getQuestions();
  }
  applyFilter(filterValue: string){
    filterValue = filterValue.trim(); //Elimina espacios en blanco
    filterValue = filterValue.toLowerCase(); // coloca en minuscula
    this.dataSource.filter = filterValue;

  }

  //  Checkbox
  selection = new SelectionModel<QuestionData>(true, []);
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
   const numSelected = this.selection.selected.length;
   const numRows = this.dataSource.data.length;
    if (numSelected >= 1) {
      this.status = true;
    }
   return numSelected === numRows;
 }
/** Selects all rows if they are not all selected; otherwise clear selection. */
 masterToggle() {
   this.isAllSelected() ?
       this.selection.clear() :
       this.dataSource.data.forEach(row => this.selection.select(row));
       this.status = false;
 }

 getQuestions() {
  this._questionService.getQuestions(this.token, this.idUser).subscribe(
    response => {
      if (response.state === true) {
      for (let i = 0; i < response.questions.length; i++) {
        this.questionsA.push(response.questions[i].preguntas);
      }
      var y = 0;
      for(var i = 0; i < this.questionsA.length; i++){
        for(let question of this.questionsA[i]) {
          this.questionsArray.push({
          id: question._id,
          position: y = y + 1,
          text: question.text,
          subject: question.subject,
          subSubject: question.subSubject,
          state: question.state
        });
       }
      }
      this.dataSource = new MatTableDataSource(this.questionsArray);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    } else if(response.state === 3){
      var message = "Error de autenticación."
      this.openSnack.openSnackBar(message);
      localStorage.clear();
      this._router.navigate(['/login']);
    }
  },
  error => {
    const errorMessage = <any>error;
    console.log(errorMessage);
  }
  );
}

updateQuestion(id){
   this._router.navigateByUrl('/addQuestion/' + id);
}
openDialog() {
  const dialogConfig = new MatDialogConfig();
  dialogConfig.disableClose = true;
  dialogConfig.autoFocus = true;
  dialogConfig.width = '250px';
  const dialogRef = this.dialog.open(DialogQuestionComponent, dialogConfig);

  dialogRef.afterClosed().subscribe(
    data => {
      if(data === true) {
      // this.deactivateQuestion();
      }
    }
  );
 }

 deactivateQuestion(id: string) {
  this.questionsDeactivate.push(id);
  console.log(this.questionsArray);
  for(let question of this.questionsArray){
    if(question.id === id && question.state === true){
      this._questionService.deactivateQuestion(this.questionsDeactivate, this.token, this.idUser).subscribe(
        response => {
          if (response.state === true) {
            this.questionsDeactivate = [];
            for(let question of this.questionsArray){
              if(id === question.id){
                question.state = false;
              }
            }
            this._router.navigate(['/admin', '1']);
          }else if(response.state === 3){
            var message = "Error de autenticación."
            this.openSnack.openSnackBar(message);
            localStorage.clear();
            this._router.navigate(['/login']);
          }
        },
        error => {
          const errorMessage = <any>error;
          console.log(errorMessage);
          console.log('Error en el servidor');
        }
      );
    }else if(question.id === id && question.state === false){
      this._questionService.activateQuestion(this.questionsDeactivate, this.token, this.idUser).subscribe(
        response => {
          if (response.state === true) {
            this.questionsDeactivate = [];
            for(let question of this.questionsArray){
              if(id === question.id){
                question.state = true;
              }
            }
            this._router.navigate(['/admin', '1']);
          }else if(response.state === 3){
            var message = "Error de autenticación."
            this.openSnack.openSnackBar(message);
            localStorage.clear();
            this._router.navigate(['/login']);
          }
        },
        error => {
          const errorMessage = <any>error;
          console.log(errorMessage);
          console.log('Error en el servidor');
        }
      );

    }
  }

 }

}

@Component({
  selector: 'app-dialogQuestion-component',
  templateUrl: './dialogQuestion.html',
})
export class DialogQuestionComponent {
  public state: boolean;

  constructor(
    public dialogRef: MatDialogRef<DialogQuestionComponent>) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  deactivate(){
    this.state = true;
    this.dialogRef.close(this.state);
  }

}
