import { Question } from './../../classes/question';
import { UserService } from './../../services/user.service';
import { TestService } from './../../services/test.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, Optional, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MatDialogConfig, MAT_DIALOG_DATA} from '@angular/material';
import { Injectable } from '@angular/core';
import { OpenSnackBarService } from './../../services/openSnackBar.service';
import { environment } from './../../../environments/environment';

@Component({
  selector: 'app-test-section',
  templateUrl: './test-section.component.html',
  styleUrls: ['./test-section.component.css'],
  providers: [TestService, UserService, OpenSnackBarService]
})
export class TestSectionComponent implements OnInit {

  // Atributos
  public url= environment.url;
  public subType: string; //Subtipo de la prueba
  public test; //prueba
  public asignatura: string;
  public identity; //usuario logueado
  public questionsTest: any[] = []; //Array de preguntas de la prueba
  public question; //Cada pregunta de la prueba
  public totalQ; //Total de preguntas
  public actual; // Contador para la pregunta actual
  public fin: boolean = false; // Para cambiar al boton finalizar
  public answerU; //Respuesta del usuario
  public mark: boolean = false; //Pregunta marcada
  public link; //Enlace de la pregunta
  public linkA; //Enlace de la respuesta a
  public linkB; //Enlace de la respuesta b
  public linkC; //Enlace de la respuesta c
  public linkD; //Enlace de la respuesta d
  public mapQuestions: Map<string, string>; //Mapa que guarda las respuestas de los usuarios
  public mapMarkQuestions: Map<string, boolean>; //Mapa que guarda si una pregunta está marcada o no
  public questionArray: QuestionData[];
  public try: number;
  public time; //json de tiempo para enviar en el servicio de enviar respuesta
  public token: string;
  public session: any;
  public area: number;
  public funcTime;
  public clear: boolean;

  // Metodos
  constructor( public dialog: MatDialog,
               private _route: ActivatedRoute,
               private _router: Router,
               private _testService: TestService,
               private _userService: UserService,
               private openSnack: OpenSnackBarService) {
    this.actual = 0;
    this.answerU = null;
    this.questionArray = [];
    this.clear = false;
  }

  ngOnInit() {
    this.subType = this._route.snapshot.paramMap.get('subType');
    this.session = this._route.snapshot.paramMap.get('session');
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getTokenUser();
    this.mapQuestions = new Map();
    this.mapMarkQuestions = new Map();
    this.getTest();
    this.blockButtom();
  }

  blockButtom(){
    if(this.actual === 1){
      document.getElementById("before").style.opacity = "0.5";
    }
  }

  //Inserta el video correspondiente al enlace de youtube obtenido
  insertVideo(option: string){
    if(this.question.subject !== 'lectura'){
      document.getElementById("section-video").setAttribute("style", "visibility: visible;");
      switch(option){
        case "q" :
          document.getElementById("vid").setAttribute("src", this.link);
          break;
        case "a":
          document.getElementById("vid").setAttribute("src", this.linkA);
          break;
        case "b":
          document.getElementById("vid").setAttribute("src", this.linkB);
          break;
        case "c":
          document.getElementById("vid").setAttribute("src", this.linkC);
          break;
        case "d":
          document.getElementById("vid").setAttribute("src", this.linkD);
      }
    }else{
      document.getElementById("section-video").setAttribute("style", "display:none;");
    }
  }

  //Marca la pregunta
  markQuestion(){
      this.mark = !this.mark;
      this.mapMarkQuestions.set(this.question._id, this.mark);
  }

  //Obtiene el simulacro que se va a realizar
  getTest() {
    this.test = this._testService.getTestLocal();
    if(this.test.type === 'prueba'){
      this.questionsTest = this.test.session1Area1;
      this.area = 1;
      if(this.session === '1c'){
        this.startTimer(0, 30, 0);
      }else{
        console.log('sin');
      }
      this.session = 1;
      // this.totalQ = this.questionsTest.length;
      // this.getQuestion(0);
      // this.startTimer(0, 30, 0);
    }else if(this.test.type === 'simulacro'){
      if(this.session === '1'){
        this.session = 1;
        for(let question of this.test.session1Area1){
          this.questionsTest.push(question);
        }
        for(let question of this.test.session1Area2){
          this.questionsTest.push(question);
        }
      }else if(this.session === '2'){
        this.session = 2;
        for(let question of this.test.session2Area1){
          this.questionsTest.push(question);
        }
        for(let question of this.test.session2Area2){
          this.questionsTest.push(question);
        }
      }
      // this.totalQ = this.questionsTest.length;
      // this.getQuestion(0);
      this.startTimer(0, 30, 0);
    }
    this.totalQ = this.questionsTest.length;
    this.getQuestion(0);
    for(let i = 0; i< this.identity.pruebasR.length; i++){
      if(this.identity.pruebasR[i]._id === this.test.subType){
        var tests = this.identity.pruebasR[i].pruebas;
        if(tests.length === 0){
          this.try = 0;
        }else{
          for(let j = 0; j< tests.length; j++){
            if(tests[j]._id === this.test._id){
              this.try = tests[j].intentos.length - 1;
            }
          }
        }
      }
    }
  }

  getQuestion(index: number){
    this.actual = index + 1;
    this.question = this.questionsTest[index];
    if(this.test.type === 'simulacro'){
      this.question = this.questionsTest[index];
      this.asignatura = this.question.subject;
      if(this.question.subject == this.test.sess1SubType[0] || this.question.subject == this.test.sess2SubType[0]){
        this.area = 1;
      }else if(this.question.subject == this.test.sess1SubType[1] || this.question.subject == this.test.sess2SubType[1]){
        this.area = 2;
      }
    }
    this.linkA = this.question.linkAnswerA;
    this.linkB = this.question.linkAnswerB;
    this.linkC = this.question.linkAnswerC;
    this.linkD = this.question.linkAnswerD;
    this.link = this.question.link;
    this.answerU = this.mapQuestions.get(this.question._id);
    this.mark = this.mapMarkQuestions.get(this.question._id);
    this.insertVideo('q');
    if(this.actual === this.totalQ){
      this.fin = true;
    }else{
      this.fin = false;
    }
    if(this.question.image !== undefined && this.question.image !== null){
      var imageQ = this.url + this.question.image;
      document.getElementById("imageQ").innerHTML = "<br/><img id='imgQ' class='zoom' width='300px' height='330px'/>";
      document.getElementById("imgQ").setAttribute("src", imageQ);
    }else{
      document.getElementById("imageQ").innerHTML = ""
    }
    if(this.question.imageAnswerA !== undefined && this.question.imageAnswerA !== null){
      var imageA = this.url  + this.question.imageAnswerA;
      document.getElementById("imageA").innerHTML = "<img id='imgA' class='zoom' width='300px' height='330px'/>";
      document.getElementById("imgA").setAttribute("src", imageA);
    }else{
      document.getElementById("imageA").innerHTML = ""
    }
    if(this.question.imageAnswerB !== undefined && this.question.imageAnswerB !== null){
      var imageB = this.url  + this.question.imageAnswerB;
      document.getElementById("imageB").innerHTML = "<img id='imgB' class='zoom' width='300px' height='330px'/>";
      document.getElementById("imgB").setAttribute("src", imageB);
    }else{
      document.getElementById("imageB").innerHTML = ""
    }
    if(this.question.imageAnswerC !== undefined &&this.question.imageAnswerC !== null){
      var imageC = this.url  + this.question.imageAnswerC;
      document.getElementById("imageC").innerHTML = "<img id='imgC' class='zoom' width='300px' height='330px'/>";
      document.getElementById("imgC").setAttribute("src", imageC);
    }else{
      document.getElementById("imageC").innerHTML = ""
    }
    if(this.question.imageAnswerD !== undefined && this.question.imageAnswerD !== null){
      var imageD = this.url  + this.question.imageAnswerD;
      document.getElementById("imageD").innerHTML = "<img id='imgD' class='zoom' width='300px' height='330px'/>";
      document.getElementById("imgD").setAttribute("src", imageD);
    }else{
      document.getElementById("imageD").innerHTML = ""
    }
  }

  nextQuestion(){
    document.getElementById("before").style.opacity = "1";
    this.sendAnswer(); //Envia la respuesta del usuario al servidor
    this.mapQuestions.set(this.question._id, this.answerU);
    this.actual = this.actual + 1; //Indica la pregunta actual
    this.getQuestion(this.actual - 1);
  }

  beforeQuestion(){
      if(this.actual !== 1){
        this.mapQuestions.set(this.question._id, this.answerU);
        this.actual = this.actual - 1; // Indica el numero de pregunta actual
        this.getQuestion(this.actual - 1);
      }
      this.blockButtom();
  }

  sendAnswer(){
    this.time = localStorage.getItem('time');
    this._testService.sendAnswer(this.identity._id, this.test._id, this.question._id, this.answerU, this.fin, this.try, this.time, this.token, this.session, this.area).subscribe(
      response => {
        if(response.state === true){
          var testUpdated = response.test;
          localStorage.setItem('test', JSON.stringify(testUpdated));
        }else if(response.state === 3){
          var message = "Error de autenticación."
          this.openSnack.openSnackBar(message);
          localStorage.clear();
          this._router.navigate(['/login']);
        }
      },
      error =>{
        const errorMessage = <any>error;
        console.log(errorMessage);
        console.log('Error en el servidor');
      }
    );
  }

  startTimer(hours: number, minutes:number, seconds:number){
    var that = this;
    var endTime = new Date();
    endTime.setHours(endTime.getHours() + hours);
    endTime.setMinutes(endTime.getMinutes() + minutes);
    endTime.setSeconds(endTime.getSeconds() + seconds);
    var endTime2 = endTime.getTime();
    this.funcTime = setInterval(function() {
      var seconds: any;
      var minutes: any;
      var startTime = new Date().getTime();
      var distance = endTime2 - startTime;
      var hours = Math.floor((distance % (1000*60*60*24))/(1000*60*60));
      minutes = Math.floor((distance % (1000*60*60)) / (1000*60));
      seconds = Math.floor((distance % (1000*60))/1000);
      if(seconds < 10 && seconds >= 0){
        seconds = "0" + seconds;
      }
      if(minutes < 10 && minutes >= 0){
        minutes = "0" + minutes;
      }
      document.getElementById("timer").innerHTML = hours + " : " + minutes + " : " + seconds;

      if(distance < 0){
        clearInterval(this.funcTime);
        if(that.test.type === 'simulacro' && that.session === 1){
          that._router.navigate(['/testStart', that.subType, '2']);
        }else if( that.test.type === 'simulacro' && that.session === 2){
          that._router.navigate(['/endTest', 'expired']);
        }else if( that.test.type === 'prueba'){
          that._router.navigate(['/endTest', 'expired']);
        }
        //that._router.navigate(['/endTest', 'expired']);
      }
      if(that.clear){
        console.log('entra');
        clearInterval(this.funcTime);
      }
      var time = {
        hours: hours,
        minutes: minutes,
        seconds: seconds
      }
      localStorage.setItem('time', JSON.stringify(time) );
    },1000);
  }

  openDialog() {
    this.mapQuestions.set(this.question._id, this.answerU);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '950px';
    dialogConfig.data = {questionsTest: this.questionsTest, totalQ: this.totalQ, questionsMark: this.mapMarkQuestions, questionsAnswered: this.mapQuestions, position: this.actual};
    const dialogRef = this.dialog.open(DialogSectionComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        this.getQuestion(data);
      }
    );
   }
   openConfirmation() {
    this.mapQuestions.set(this.question._id, this.answerU);
    // this.sendAnswer();
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '450px';
    dialogConfig.data = {session: this.session, type: this.test.type, subType: this.subType, answers: this.mapQuestions};
    const dialogRef = this.dialog.open(DialogConfirmationComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        if(data){
          this.sendAnswer();
          this.clear = true;
          clearInterval(this.funcTime);
        }
      }
    );
   }

}

export interface QuestionData {
  id: any;
  mark: boolean;
  answer: string;
}
@Component({
  selector: 'app-dialogSection-component',
  templateUrl: './dialogSection.html',
})
@Injectable()
export class DialogSectionComponent implements OnInit{
 public questionsTest: any[];
 public questionsMark: Map<string, boolean>;
 public questionsAnswer: Map<string, string>;
 public mark: boolean = false;
 public answer;
 public exits;
 public showArray: QuestionData[];
 public actual;
  constructor(public dialogRef: MatDialogRef<DialogSectionComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
      this.questionsTest = data.questionsTest;
      this.questionsMark = data.questionsMark;
      this.questionsAnswer = data.questionsAnswered;
      this.showArray = [];
      this.actual = data.position - 1;
    }

    ngOnInit(){
      // this.startQuestions();
    }
    ngAfterViewInit(){
      this.startQuestions();
    }
    startQuestions(){
      for(let question of this.questionsTest){
         this.mark = this.questionsMark.get(question._id);
         this.answer = this.questionsAnswer.get(question._id);
         if(this.answer !== undefined){
          //  this.exits = true;
          this.exits = "#fcd25c";
          //  document.getElementById("mark").setAttribute("style", "background-color: #FF8040");
         }else{
          this.exits = "#FFFFFF";
          //  this.exits = false;
         }
         this.showArray.push({
           id: question._id,
           mark: this.questionsMark.get(question._id),
          //  answer: this.exits
           answer: this.exits
         });
        console.log(this.showArray);

      }
    }
  goQuestion(index){
    this.dialogRef.close(index);
  }

  onNoClick(): void {
    this.dialogRef.close(this.actual);
  }
}


@Component({
  selector: 'app-dialogConfirmation-component',
  templateUrl: './dialogConfirmation.html',
  providers: [TestService]
})
@Injectable()
export class DialogConfirmationComponent {

  public session: number;
  public type: string;
  public subType: string;
  public answers;
  constructor(public dialogRef: MatDialogRef<DialogConfirmationComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private _router: Router,
              private _testService: TestService) {
      this.session = data.session;
      this.type = data.type;
      this.subType = data.subType;
      this.answers = data.answers;
    }

  onNoClick(): void {
    this.dialogRef.close(false);
  }
  end(){
    // this._testService.setAnswers(this.answers);
    this.dialogRef.close(true);
    if(this.type === 'simulacro' && this.session === 1){
      localStorage.setItem('answers1', JSON.stringify(Array.from(this.answers.entries())));
      this._router.navigate(['/testStart', this.subType, '2']);
    }else if( this.type === 'simulacro' && this.session === 2){
      localStorage.setItem('answers2', JSON.stringify(Array.from(this.answers.entries())));
      this._router.navigate(['/endTest', 'end']);
    }else if( this.type === 'prueba'){
      localStorage.setItem('answers1', JSON.stringify(Array.from(this.answers.entries())));
      this._router.navigate(['/endTest', 'end']);
    }
  }

}
