import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-show-suggestions',
  templateUrl: './show-suggestions.component.html',
  styleUrls: ['./show-suggestions.component.css'],
  providers: [UserService]
})
export class ShowSuggestionsComponent implements OnInit {

  public identity;
  public token: string;
  public numberVideosFalta: number;
  public videosSubidos;
  public sugerencias;
  public porSubir;
  public subidos;


  constructor(private _userService: UserService, private _route: ActivatedRoute,
    private _router: Router,) { }

  ngOnInit() {
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getTokenUser();
    this.getNumberVideos();
  }

  load(){
    this._userService.loadVideos(this.identity._id, this.token).subscribe(
      response =>{
        if(response.state === 1){
          //location.href = response.url;
          document.getElementById("anchorID").setAttribute("href", response.url);
          document.getElementById("anchorID").click();
        }
        console.log(response);
        //this._router.navigate(['/admin', '0']);
      },
      error => {
        const errorMessage = <any>error;
        console.log(errorMessage);
      }
    );
  }
  getNumberVideos(){

    this.identity.sugerencias? this.sugerencias = true: this.sugerencias=false;
    if(this.sugerencias){
      this.identity.sugerencias.porSubir? this.porSubir = true: this.porSubir=false;
      if(this.porSubir && this.identity.sugerencias.porSubir.length > 0){
        this.numberVideosFalta = this.identity.sugerencias.porSubir.length;
      }
    }
    if(this.sugerencias){
      this.identity.sugerencias.subidos? this.subidos = true: this.subidos=false;
      if(this.subidos && this.identity.sugerencias.subidos.length > 0){
        this.videosSubidos = this.identity.sugerencias.subidos;
      }
    }
  }


}
