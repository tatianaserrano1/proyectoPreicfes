import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {UserService} from '../../services/user.service';
import { OpenSnackBarService } from './../../services/openSnackBar.service';

@Component({
  selector: 'app-end-test',
  templateUrl: './end-test.component.html',
  styleUrls: ['./end-test.component.css'],
  providers: [UserService, OpenSnackBarService]
})
export class EndTestComponent implements OnInit {

  public identity;
  public token: string;
  public typeEnd: string;

  //Métodos
  constructor(private _route: ActivatedRoute,
              private _router: Router,
              private _userService: UserService,
              private openSnack: OpenSnackBarService) { }

  ngOnInit() {
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getTokenUser();
    this.typeEnd = this._route.snapshot.paramMap.get('type');
    console.log(this.typeEnd);
  }

  updateDataUser(idUser: string, token: string){
    this._userService.getDataUser(idUser, token).subscribe(
      response =>{
        if(response.state === 1){
          this.identity = response.user;
          this.identity.password = '';
          localStorage.setItem('identity', JSON.stringify(this.identity));
          this._router.navigate(['/showTables']);
        }else if(response.state === 3){
          var message = "Error de autenticación."
          this.openSnack.openSnackBar(message);
          localStorage.clear();
          this._router.navigate(['/login']);
        }
      },
        error => {
          const errorMessage = <any>error;
          console.log(errorMessage);
        }
    );
  }
}
