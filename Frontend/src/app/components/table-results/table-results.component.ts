import { Component, OnInit, ViewChild, Input} from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import { TestService } from '../../services/test.service';
import {UserService} from '../../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';

export interface resultsData {
  id: string;
  position: any;
  correction: boolean;
  session: number;
}
@Component({
  selector: 'app-table-results',
  templateUrl: './table-results.component.html',
  styleUrls: ['./table-results.component.css'],
  providers: [UserService, TestService]
})
export class TableResultsComponent implements OnInit {

  @Input() type: string;
  @Input() intento;
  public test: any;
  public identity: any;
  public token: string;
  public resultsArray: any[] = [];
  displayedColumns: string[] = ['position', 'correction'];
  dataSource: MatTableDataSource<resultsData>;

  constructor(private _userService: UserService,
              private _testService: TestService,
              private _route: ActivatedRoute,
              private _router: Router
              ) {
    this.dataSource  = new MatTableDataSource<resultsData>([]);
  }

  ngOnInit() {
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getTokenUser();
    this.getTest();
  }
  applyFilter(filterValue: string){
    filterValue = filterValue.trim(); //Elimina espacios en blanco
    filterValue = filterValue.toLowerCase(); // coloca en minuscula
    this.dataSource.filter = filterValue;
  }

  getTest(){
    if(this.intento){
      this.test = JSON.parse(localStorage.getItem('testResults'));
    }else{
      this.test = this._testService.getTestLocal();
    }
    this.fillTable();

  }
  fillTable(){
    if(this.type === 'prueba'){
      var questions = this.test.session1Area1;
      var y = 0;
      var correct;
      for(let ques of questions){
        if(ques.correctAnswer === ques.answerUser){
          correct = true;
        }else{
          correct = false;
        }
        this.resultsArray.push({
          id: ques._id,
          position: y = y+1,
          correction: correct,
          session: 1
        });
      }
    }else if(this.type === 's1a1'){
      var questions = this.test.session1Area1;
      var y = 0;
      var correct;
      for(let ques of questions){
        if(ques.correctAnswer === ques.answerUser){
          correct = true;
        }else{
          correct = false;
        }
        this.resultsArray.push({
          id: ques._id,
          position: y = y+1,
          correction: correct,
          session: 1
        });
      }
    }else if(this.type === 's1a2'){
      var questions = this.test.session1Area2;
      var y = 0;
      var correct;
      for(let ques of questions){
        if(ques.correctAnswer === ques.answerUser){
          correct = true;
        }else{
          correct = false;
        }
        this.resultsArray.push({
          id: ques._id,
          position: y = y+1,
          correction: correct,
          session: 1
        });
      }
    }else if(this.type === 's2a1'){
      var questions = this.test.session2Area1;
      var y = 0;
      var correct;
      for(let ques of questions){
        if(ques.correctAnswer === ques.answerUser){
          correct = true;
        }else{
          correct = false;
        }
        this.resultsArray.push({
          id: ques._id,
          position: y = y+1,
          correction: correct,
          session: 2
        });
      }
    }else if(this.type === 's2a2'){
      var questions = this.test.session2Area2;
      var y = 0;
      var correct;
      for(let ques of questions){
        if(ques.correctAnswer === ques.answerUser){
          correct = true;
        }else{
          correct = false;
        }
        this.resultsArray.push({
          id: ques._id,
          position: y = y+1,
          correction: correct,
          session: 2
        });
      }
    }
    this.dataSource = new MatTableDataSource(this.resultsArray);
  }
  routingQuestion(id: string, session: number){
    if(this.intento){
      this._router.navigate(['/showResults', id, session, this.intento]);
    }else{
      this._router.navigate(['/showResults', id, session]);
    }
  }

}
