import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableAveragesPruebasComponent } from './table-averages-pruebas.component';

describe('TableAveragesPruebasComponent', () => {
  let component: TableAveragesPruebasComponent;
  let fixture: ComponentFixture<TableAveragesPruebasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableAveragesPruebasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableAveragesPruebasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
