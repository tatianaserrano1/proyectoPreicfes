import { Component, OnInit, Input } from '@angular/core';
import { UserService } from './../../services/user.service';
import {TestService} from '../../services/test.service';
import {MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-table-averages-pruebas',
  templateUrl: './table-averages-pruebas.component.html',
  styleUrls: ['./table-averages-pruebas.component.css'],
  providers: [ UserService, TestService]
})
export class TableAveragesPruebasComponent implements OnInit {

  public idTest:string;
  public existTest: boolean;
  public testSelected;
  public type:string;
  public tests: any[] = [];
  public testsSimulacro: any[] = [];
  public testsPruebas: any[] = [];
  public identity;
  public token: string;
  public flag = 0;
  public averagesArray: any[] = [];
  public totalAverage;
  public totalAmount;
  public blnShow: boolean = true;

  constructor(private _testService: TestService, private _userService: UserService) {
    this.existTest = false;
   }

  ngOnInit() {
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getTokenUser();
    this.getTests();
  }


  getTests(){
    this._testService.getTests(this.token, this.identity._id).subscribe(
      response =>{
        if (response.state === true) {
          this.tests = response.tests;
          for(let test of this.tests){
            if(test.type === 'simulacro'){
              this.testsSimulacro.push(test);
            }else if(test.type === 'prueba'){
              this.testsPruebas.push(test);
            }
          }
        }
      },
     error => {
       const errorMessage = <any>error;
       console.log(errorMessage);
     }
    );
  }
  getAverages(id: string, type: string){
    this._userService.getAverage(id, this.identity._id, this.token, type).subscribe(
      response =>{
        console.log(response);
        if(response.state === 1){
          this.blnShow = false;
          var pruebas = response.stats;
          localStorage.setItem('showQues',JSON.stringify(pruebas.preguntas));
          this.existTest = true;
          var pruebas = response.stats;
          this.totalAverage = pruebas.total.score;
          this.totalAmount = pruebas.total.amount;
        }else if(response.state === 0){
          this.flag = 1;
          this.existTest = false;
        }
      },
     error => {
       const errorMessage = <any>error;
       console.log(errorMessage);
     }
    );
  }
  getSimulacro(type: string){
    if(type === 'examen'){
      this.flag = 5;
    }else if(type === 'prueba'){
      this.flag = 6
    }
  }

}
