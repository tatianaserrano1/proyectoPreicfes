import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, Validators, FormBuilder, FormGroup} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService} from '../../services/user.service';
import * as sha512 from 'js-sha512';

/** Error when invalid control is dirty, touched, or submitted. */
// export class MyErrorStateMatcher implements ErrorStateMatcher {
//   isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
//     const isSubmitted = form && form.submitted;
//     return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
//   }
// }
@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.css'],
  providers: [UserService]
})
export class RecoverPasswordComponent implements OnInit {
  public type;
  public email: string;
  public password: string;
  public confirmation: string;
  public safe: string; //Contraseña encriptada
  public exits: any;
  public state: any;
  public tokenUrl: string;
  public token: string;

  mailForm = this.fb.group({
    email: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])]
  });

  passForm = this.fb.group({
    password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
    confirmation: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
  }, {validator: this.checkPassword});

  constructor(private _userService: UserService, private _route:ActivatedRoute, private _router:Router,  private fb: FormBuilder) {
    this.type = this._route.snapshot.paramMap.get('type');
  }

  ngOnInit() {
    this.getToken();
  }

  checkPassword(group: FormGroup){
    let pass = group.controls.password.value;
    let confirm = group.controls.confirmation.value;
    // this.confirmation = this.regForm.get('confirmation');
    return pass === confirm ? null : {notsame: true}
  }

  getToken(){
    if(this.type === 'password'){
      this.tokenUrl = this._route.snapshot.queryParams.token;
      console.log(this.tokenUrl);
    }
  }
  sendData(){
    if(this.type === 'email'){
      this._userService.sendEmail(this.email).subscribe(
        response => {
          this.exits = response;
          if(this.exits.state === true){
            this.type = 'exits';
          }else{

          }
        },
        error =>{
          const errorMessage = <any>error;
          console.log(errorMessage);
        }
      );
    }
    if(this.type === 'password'){
      this.safe = sha512(this.password);
      console.log(this.tokenUrl);
      console.log(this.safe);
       this._userService.sendNewPassword(this.tokenUrl, this.safe).subscribe(
         response =>{
           this.state = response;
           if(this.state.state){
             this.type = 'change';
           }
         },
         error => {
          const errorMessage = <any>error;
          console.log(errorMessage);
         }
       );
    }
  }

}
