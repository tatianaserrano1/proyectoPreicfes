import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user.service';
import {User} from '../../classes/user';
import * as sha512 from 'js-sha512';
import { FormsModule, ReactiveFormsModule, FormGroup } from '@angular/forms';
import { FormBuilder, Validators, FormControl } from '../../../../node_modules/@angular/forms';
import { MatSnackBar, MatSnackBarConfig } from '../../../../node_modules/@angular/material';
import {Router, ActivatedRoute} from '@angular/router';
import {LoginObject} from '../../classes/loginObject';

import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider,
  SocialUser
} from 'angular-6-social-login';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [UserService]
})
export class RegisterComponent {

  private userSocial: SocialUser;
  public authorized: boolean = false;
  public user: User;
  public safe: string;
  public confirmation: any;
  public pass: any;
  public login: LoginObject;
  public identity;
  public maxDate = new Date();
  public exits : boolean = false;
  public token: string;

  generos = [
    {value: 'hombre', viewValue: 'Hombre'},
    {value: 'mujer', viewValue: 'Mujer'},
  ];

  regForm = this.fb.group({
    name: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
    surname: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
    email: ['', Validators.compose([
      Validators.required,
      Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
    ])],
    password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
    birthdate: ['', Validators.required],
    gender: ['', Validators.required],
    profilePicture: [''],
    confirmation: ['', Validators.compose([Validators.required, Validators.minLength(6)])] }, {validator: this.checkPassword }
  );

  constructor( private _userService: UserService,
               private fb: FormBuilder,
               private _route: ActivatedRoute,
               private _router: Router,
               public snackBar: MatSnackBar,
               private socialAuthService: AuthService) {
    this.user = new User(null, null, null, null, null, null, null, null);
   }

  // get f() { return this.regForm.controls; }

  onSubmit() {
    // Enviar contraseña con hash
    this.safe = sha512(this.user.password);
    this.user.password = this.safe;
    this._userService.register(this.user).subscribe(
      response => {
        if (response.state === 1) {
          this.login = new LoginObject(this.user.email, this.user.password, 2);
          this._userService.login(this.login).subscribe(
            response => {
              if (response.state === 1) {
              this.identity = response.doc_user;
              this.identity.password = '';
              this.token = response.token;
              localStorage.setItem('identity', JSON.stringify(this.identity));
              localStorage.setItem('token', JSON.stringify(this.token));
              this._router.navigate(['/index']);
              } else if (response.state === 0) {
              alert('Sus datos son incorrectos');
              } else {
              alert('Error en el servidor');
              }
            },
            error => {
              const errorMessage = <any>error;
              console.log(errorMessage);
            }
          );
        } else if (response.state === 0) {
          // this.exits = true;
          this._router.navigate(['/login']);
          var message = "Ya tienes una cuenta registrada con ese correo electrónico, Inicia sesión aqui."
          this.openSnackBar(message);
        } else {
          alert('Error en el servidor');
        }

      },
      error => {
        console.log(<any>error);
      }
    );
  }
  checkPassword(group: FormGroup){
    let pass = group.controls.password.value;
    let confirm = group.controls.confirmation.value;
    // this.confirmation = this.regForm.get('confirmation');
    return pass === confirm ? null : {notsame: true}
  }
  public socialSignIn(socialPlatform: string) {
    let socialPlatformProvider;
    if (socialPlatform === 'facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialPlatform === 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        console.log(socialPlatform + ' sign in data : ' , userData);
        // Now sign-in with userData
        // ...
        if (userData != null) {
          this.authorized = true;
          this.userSocial = userData;
          var user = {
            name: userData.name,
            email: userData.email,
            flag: 1
          }
          this._userService.login(user).subscribe(
            response =>{
              if (response.state === 1 && response.doc_user.state === true) {
                this.identity = response.doc_user;
                this.token = response.token;
                localStorage.setItem('token', JSON.stringify(this.token));
                this.identity.password = '';
                localStorage.setItem('identity', JSON.stringify(this.identity));
                if(this.identity.tipo === 0/* && this.type==='user' */){
                  this._router.navigate(['/index']);
                }else if(this.identity.tipo === 1 /*&& this.type=== 'admin'*/){
                  this._router.navigate(['/admin', '0']);
                }
                } else if (response.state === 0 || response.doc_user.state === false) {
                this.login.email = null;
                this.login.password = null;
                this.identity = null;
                var message = "Los datos ingresados no coinciden con nuestros registros. Por favor, revise e inténtelo de nuevo."
                this.openSnackBar(message);
                }else{
                alert('Error en el servidor');
                this.identity = null;
                }
            },
            error =>{
              const errorMessage = <any>error;
              console.log(errorMessage);
              this.identity = null;
            }
          );
       }
      }
    );
  }
  public signOut() {
    this.socialAuthService.signOut();
    this.authorized = false;
}

  openSnackBar(message: string){
    const snackConfig = new MatSnackBarConfig();
    snackConfig.verticalPosition = 'top';
    snackConfig.duration = 10000;
    this.snackBar.open(message, "Cerrar", snackConfig);
  }
}
