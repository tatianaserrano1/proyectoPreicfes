import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '../../../node_modules/@angular/material';

@Injectable()
export class OpenSnackBarService{

  constructor(public snackBar: MatSnackBar){
  }

  openSnackBar(message: string){
    const snackConfig = new MatSnackBarConfig();
    snackConfig.verticalPosition = 'top';
    snackConfig.duration = 8000;
    this.snackBar.open(message, "Cerrar", snackConfig);
  }
}
