import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Question } from '../classes/question';
import { Test } from '../classes/test';

@Injectable()
export class TestService {
  public url: string;
  public test: any;
  public try: any;
  public answers: Map<string, string> = null;

  constructor(public _http: HttpClient) {
    this.url = environment.url;
   }

   //Crea un test en la base de datos
   createTest(test: Test, token: string, idUser: string): Observable<any> {
     const params = {
       title: test.title,
       description: test.description,
       type: test.type,
       subType: test.subType,
       session1area1: test.session1Area1,
       session1area2: test.session1Area2,
       session2area1: test.session2Area1,
       session2area2: test.session2Area2,
       sess1SubType: test.sess1SubType,
       sess2SubType: test.sess2SubType,
       token: token,
       idUser: idUser
     }
     const headers = new HttpHeaders().set('Content-Type', 'application/json');
     return this._http.post(this.url + 'addTest', params, {headers: headers});
   }
  //  Obtiene todas las pruebas
   getTests(token: string, idUser: string): Observable<any> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('token', token || '');
    headers = headers.set('idUser', idUser || '');
     return this._http.get(this.url + 'tests', {headers: headers});
   }
  //  Obtiene las pruebas por ID NO SE NECESITÓ CAMBIAR NADA ES PORQUE NUNCA SE LLAMA ESTE SERVICIO
   getTestId(idTest: string, token: string, idUser: string): Observable<any> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('token', token || '');
    headers = headers.set('idUser', idUser || '');
    return this._http.get(this.url + 'getTest/' + idTest, {headers: headers});
   }
  //  Obtiene las pruebas por subtipo
   getTestSub(subType: string, token: string, idUser:string): Observable<any> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('token', token || '');
    headers = headers.set('idUser', idUser || '');
     return this._http.get(this.url + 'test/' + subType, {headers: headers});
   }

   updateTest( test: any, token: string, idUser: string): Observable<any> {
     const params = {
       _id: test._id,
       title: test.title,
       description: test.description,
       type: test.type,
       subType: test.subType,
       session1area1: test.session1Area1,
       session1area2: test.session1Area2,
       session2area1: test.session2Area1,
       session2area2: test.session2Area2,
       sess1SubType: test.sess1SubType,
       sess2SubType: test.sess2SubType,
       token: token,
       idUser: idUser
     }
     const headers = new HttpHeaders().set('Content-Type', 'application/json');
     return this._http.put(this.url + 'updateTest', params, {headers: headers});
   }

   deactivateTest(ids, token: string, idUser: string): Observable<any> {
     const params = {
       ids: ids,
       token: token,
       idUser: idUser
     }
     const headers = new HttpHeaders().set('Content-Type', 'application/json');
     return this._http.put(this.url + 'deactivateTest', params, {headers: headers});
   }
   activateTest(ids, token: string, idUser: string): Observable<any> {
     const params = {
       idUser: idUser,
       token: token,
       ids: ids
     }
     const headers = new HttpHeaders().set('Content-Type', 'application/json');
     return this._http.put(this.url + 'activateTest', params, {headers: headers});
   }

   sendAnswer(idUser: string, idTest:string, idQuestion:string, answer: string, last: boolean, intento: number, time, token:string, session: number, area:number): Observable<any>{
     const params = {
       idUser: idUser,
       idTest: idTest,
       idQuestion: idQuestion,
       answer: answer,
       last: last,
       try: intento,
       time: time,
       token: token,
       session: session,
       area: area
     }
     const headers = new HttpHeaders().set('Content-Type', 'application/json');
     return this._http.post(this.url + 'answer', params, {headers: headers});
   }

   //Inica un simulacro en el primer intento
   startTest(idUser: string, idTest: string, token: string): Observable<any>{
     const params = {
       idUser: idUser,
       idTest: idTest,
       token: token
     }
     const headers = new HttpHeaders().set('Content-Type', 'application/json');
     return this._http.post(this.url + 'start', params, {headers: headers});
   }

   //Inicia un simulacro después del segundo Intento
   startTry(idUser: string, idTest: string, token: string): Observable<any>{
    const params = {
      idUser: idUser,
      idTest: idTest,
      token: token
    }
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this._http.post(this.url + 'startTry', params, {headers: headers});
  }

   getResults(idTest: string, token: string, idUser: string): Observable<any>{
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('token', token || '');
    headers = headers.set('idUser', idUser || '');
    return this._http.get(this.url + 'results' + idTest, {headers: headers});
   }

   getTestLocal(){
    let test = JSON.parse(localStorage.getItem('test'));
    if(test != "undefined"){
      this.test = test;
    }else{
      this.test = null;
    }
    return this.test;
  }
  getTry(){
    let test = JSON.parse(localStorage.getItem('test'));
    let identity = JSON.parse(localStorage.getItem('identity'));
    for(let i = 0; i< identity.pruebasR.length; i++){
      if(identity.pruebasR[i]._id === this.test.subType){
        var tests = identity.pruebasR[i].pruebas;
        if(tests.length === 0){
           this.try = 0;
        }else{
          for(let j = 0; j< tests.length; j++){
            if(tests[j]._id === this.test._id){
              this.try = tests[j].intentos.length - 1;
            }
          }
        }
      }
    }
    return this.try;
  }
  getTestTry(idTest: string, subType: string, intento: number){
    let identity = JSON.parse(localStorage.getItem('identity'));
    var docIntento = null;
    for(let i = 0; i< identity.pruebasR.length; i++){
      if(identity.pruebasR[i]._id === subType){
        var tests = identity.pruebasR[i].pruebas;
        if(tests.length === 0){
        }else{
          for(let j = 0; j< tests.length; j++){
            if(tests[j]._id === idTest){
              this.try = tests[j].intentos.length - 1;
              var arrayTries = tests[j].intentos;
              docIntento = arrayTries[intento - 1];
              return docIntento;
          }
        }
      }
    }
  }

  }
}
