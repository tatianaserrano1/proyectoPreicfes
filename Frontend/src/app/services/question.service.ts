import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Question} from '../classes/question';

@Injectable()
export class QuestionService {
  public url: string;

  constructor(public _http: HttpClient) {
    this.url = environment.url;
   }

   createQuestion(question: Question, token: string, idUser:string): Observable<any> {
     const params = {
      link: question.link,
      text: question.text,
      image: question.image,
      subject: question.subject,
      subSubject: question.subSubject,
      answerA: question.answerA,
      answerB: question.answerB,
      answerC: question.answerC,
      answerD: question.answerD,
      linkAnswerA: question.linkAnswerA,
      linkAnswerB: question.linkAnswerB,
      linkAnswerC: question.linkAnswerC,
      linkAnswerD: question.linkAnswerD,
      imageAnswerA: question.imageAnswerA,
      imageAnswerB: question.imageAnswerB,
      imageAnswerC: question.imageAnswerC,
      imageAnswerD: question.imageAnswerD,
      correctAnswer: question.correctAnswer,
      token: token,
      idUser: idUser
     }
     const headers = new HttpHeaders().set('Content-Type', 'application/json');
     return this._http.post(this.url + 'add-question', params, {headers: headers});

   }
  //  Obtener todas las preguntas
   getQuestions(token: string|string[], idUser: string): Observable<any> {
     let headers = new HttpHeaders().set('Content-Type', 'application/json');

    headers = headers.set('token', token || '');
    headers = headers.set('idUser', idUser || '');
    headers = headers.set('iduser', idUser || '');
     return this._http.get(this.url + 'questions', {headers: headers});
   }
  //  Obtener preguntas por asignatura
   getQuestionsType(type, token: string, idUser: string): Observable<any> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('token', token || '');
    headers = headers.set('idUser', idUser || '');
    return this._http.get(this.url + 'questions/' + type, {headers: headers});
   }
   getQuestion(id, token:string, idUser:string): Observable<any>{
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('token', token || '');
    headers = headers.set('idUser', idUser || '');
    return this._http.get(this.url + 'question/' + id, {headers: headers});
   }

   updateQuestion(question: Question, token:string, idUser: string, idQuestion: string): Observable<any> {
     const params = {
       _id: String(idQuestion),
       link: question.link,
       text: question.text,
       image: question.image,
       subject: question.subject,
       subSubject: question.subSubject,
       answerA: question.answerA,
       answerB: question.answerB,
       answerC: question.answerC,
       answerD: question.answerD,
       linkAnswerA: question.linkAnswerA,
       linkAnswerB: question.linkAnswerB,
       linkAnswerC: question.linkAnswerC,
       linkAnswerD: question.linkAnswerD,
       imageAnswerA: question.imageAnswerA,
       imageAnswerB: question.imageAnswerB,
       imageAnswerC: question.imageAnswerC,
       imageAnswerD: question.imageAnswerD,
       correctAnswer: question.correctAnswer,
       token: token,
       idUser: idUser
     }
     const headers = new HttpHeaders().set('Content-type', 'application/json');
     return this._http.put(this.url + 'updateQuestion/', params, {headers: headers});
   }

   deactivateQuestion(ids, token: string, idUser: string): Observable<any> {
    const params = {
      ids: ids,
      token: token,
      idUser: idUser
    }
     const headers = new HttpHeaders().set('Content-type', 'application/json');
     return this._http.put(this.url + 'deactivateQuestions', params, {headers: headers});
   }
   activateQuestion(ids, token: string, idUser: string): Observable<any> {
    const params = {
      idUser: idUser,
      token: token,
      ids: ids
    }
     const headers = new HttpHeaders().set('Content-type', 'application/json');
     return this._http.put(this.url + 'activateQuestions', params, {headers: headers});
   }
}
