import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { User } from '../classes/user';
import { LoginObject } from '../classes/loginObject';

@Injectable()
export class UserService {
  //Atributos
  public url: string;
  public identity: any;
  public token: string;

  constructor(public _http: HttpClient) {
    this.url = environment.url;
   }

   register(user: User): Observable<any> {
     const params = user;
     const headers = new HttpHeaders().set('Content-Type', 'application/json');
     return this._http.post(this.url + 'register', params, {headers: headers}) ;
   }

   login(loginUser: any): Observable<any> {
     const params = loginUser;
     const headers = new HttpHeaders().set('Content-type', 'application/json');
     return this._http.post(this.url + 'login', params, {headers: headers});
   }

   getUsers(token: string, idUser: string): Observable<any> {
     let headers = new HttpHeaders().set('Content-Type', 'application/json');
     headers = headers.set('token', token || '');
     headers = headers.set('idUser', idUser || '');
     console.log(headers);
     return this._http.get(this.url + 'users', {headers: headers});
   }

   getUserById(idAdmin: string, idUser: string, token: string): Observable<any>{
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    headers = headers.set('token', token || '');
    headers = headers.set('idUser', idUser || '');
    headers = headers.set('idAdmin', idAdmin || '');

    return this._http.get(this.url + 'userById', {headers: headers});
   }

   updateUser( user: User): Observable<any> {
     const params = user;
     const headers = new HttpHeaders().set('Content-Type', 'application/json');
     return this._http.put(this.url + 'updateUser', params, {headers: headers});
   }

   deactivateUser(ids: Array<any>, token: string, idUser: string): Observable<any> {
     const params = {
       ids: ids,
       token: token,
       idUser: idUser
     }
     const headers = new HttpHeaders().set('Content-Type', 'application/json');
     return this._http.put(this.url + 'deactivateUsers',params, {headers: headers});
   }

   activateUser(ids: Array<any>, token: string, idUser: string): Observable<any> {
     const params = {
      idUser: idUser,
      token: token,
      ids: ids
     }
     const headers = new HttpHeaders().set('Content-Type', 'application/json');
     return this._http.put(this.url + 'activateUsers',params, {headers: headers});
   }

    getIdentity(){
      let identity = JSON.parse(localStorage.getItem('identity'));
      if(identity != "undefined"){
        this.identity = identity;
      }else{
        this.identity = null;
      }
      return this.identity;
    }
    setIdentity(identity: any){
      localStorage.setItem('identity', JSON.stringify(identity));
    }

    getTokenUser(){
      let token = JSON.parse(localStorage.getItem('token'));
      if(token != "undefined"){
        this.token = token;
      }else{
        this.token = null;
      }
      return this.token;
    }

    sendEmail(email: string){
      const params = {
        email: email
      };
      const headers = new HttpHeaders().set('Content-type', 'application/json');
      return this._http.post(this.url + 'recover', params, {headers: headers});
    }

    sendNewPassword(token: string, password: string){
      const params = {
        token: token,
        password: password
      }
      const headers = new HttpHeaders().set('Content-type', 'application/json');

      return this._http.post(this.url + 'recover/password', params, {headers: headers});
    }
    getDataUser(idUser: string, token: string): Observable<any>{
      let headers = new HttpHeaders().set('Content-Type', 'application/json');
      headers = headers.set('token', token || '');
      headers = headers.set('idUser', idUser || '');
      return this._http.get(this.url + 'data', {headers: headers} );
    }

    sendVideo(video: string, token: string, idUser: string): Observable<any>{
      const params = {
        token: token,
        idUser: idUser,
        video: video
      }
      const headers = new HttpHeaders().set('Content-type', 'application/json');

      return this._http.post(this.url + 'video', params, {headers: headers});
    }
    getAverage(idTest: string, idUser: string, token: string, type: string): Observable<any>{
      let headers = new HttpHeaders().set('Content-Type', 'application/json');
      headers = headers.set('idTest', idTest || '');
      headers = headers.set('token', token || '');
      headers = headers.set('idUser', idUser || '');
      headers = headers.set('type', type || '');
      return this._http.get(this.url + 'average', {headers: headers});
    }
    loadVideos(idUser: string, token: string): Observable<any>{
      let headers = new HttpHeaders().set('Content-Type', 'application/json');
      headers = headers.set('token', token || '');
      headers = headers.set('idUser', idUser || '');
      return this._http.get(this.url + 'loadVideos', {headers: headers});
    }
}
