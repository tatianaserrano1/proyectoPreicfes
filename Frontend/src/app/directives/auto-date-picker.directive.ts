import { Directive, ElementRef, OnInit } from "@angular/core";

@Directive({
    selector: '[autoDatePicker]'
})
export class AutoDatePickerDirective implements OnInit {
    
    private datePickerReference: any;

    constructor(private reference: ElementRef) {

    }

    ngOnInit() {
        this.datePickerReference = this.reference.nativeElement.parentElement.parentElement.getElementsByTagName('button');
        this.reference.nativeElement.addEventListener("click", () => {
            this.datePickerReference[0].click();
        });
    }


}