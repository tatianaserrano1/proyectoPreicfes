export class Question {
  constructor(
    public link: string,
    public text: string,
    public image: any,
    public subject: string,
    public subSubject: string,
    public answerA: string,
    public answerB: string,
    public answerC: string,
    public answerD: string,
    public linkAnswerA: string,
    public linkAnswerB: string,
    public linkAnswerC: string,
    public linkAnswerD: string,
    public imageAnswerA: string,
    public imageAnswerB: string,
    public imageAnswerC: string,
    public imageAnswerD: string,
    public correctAnswer: string
  ) {}
}
