export class Test {
  constructor (
    public title: string,
    public description: string,
    public type: string,
    public subType: string,
    public session1Area1: Array<number>,
    public session1Area2: Array<number>,
    public session2Area1: Array<number>,
    public session2Area2: Array<number>,
    public sess1SubType: Array<string>,
    public sess2SubType: Array<string>
    ) {}
}
