export class User {
  constructor (
    public name: string,
    public surname: string,
    public email: string,
    public password: string,
    public birthdate: Date,
    public gender: string,
    public picture: string,
    public token: string ) {}
}
