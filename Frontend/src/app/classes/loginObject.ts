export class LoginObject {
  constructor(
  public email: string,
  public password: string,
  public flag: number) {}
}
