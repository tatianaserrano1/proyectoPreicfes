import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule, FormBuilder, Validators} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {environment} from '../environments/environment';

import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
} from 'angular-6-social-login';

import { AppComponent } from './app.component';
import {MaterialModule} from './material/material.module';
import { HomeScreenComponent } from './components/home-screen/home-screen.component';
import { AppRoutingModule } from './/app-routing.module';
import { RecoverPasswordComponent } from './components/recover-password/recover-password.component';
import { LoginComponent } from './components/login/login.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { RegisterComponent } from './components/register/register.component';
import { IndexUserComponent } from './components/index-user/index-user.component';
import { NavbarUserComponent, HelpDialogComponent, HelpDialogAdminComponent } from './components/navbar-user/navbar-user.component';
import { SettingUserComponent } from './components/setting-user/setting-user.component';
import { AdminComponent } from './components/admin/admin.component';
import { AddQuestionComponent } from './components/add-question/add-question.component';
import { AddTestComponent, DialogInformationComponent } from './components/add-test/add-test.component';
import { ErrorComponent } from './components/error/error.component';
import { DialogComponent} from './components/table-users/table-users.component';
import { DialogQuestionComponent} from './components/table-questions/table-questions.component';
import { DialogTestComponent} from './components/table-tests/table-tests.component';
import { DialogSectionComponent} from './components/test-section/test-section.component';
import { DialogConfirmationComponent} from './components/test-section/test-section.component';
import { TableUsersComponent } from './components/table-users/table-users.component';
import { TableQuestionsComponent } from './components/table-questions/table-questions.component';
import { TableTestsComponent } from './components/table-tests/table-tests.component';
import { UserService } from './services/user.service';
import { TestSectionComponent } from './components/test-section/test-section.component';
import { EndTestComponent } from './components/end-test/end-test.component';
import { ResultsComponent } from './components/results/results.component';
import { AutoDatePickerDirective } from './directives/auto-date-picker.directive';
import { ResourcesComponent } from './components/resources/resources.component';
import { TestStartComponent } from './components/test-start/test-start.component';
import { TableExamenIndexComponent } from './components/table-examen-index/table-examen-index.component';
import { TableTestIndexComponent } from './components/table-test-index/table-test-index.component';
import { VocabularyComponent } from './components/vocabulary/vocabulary.component';
import { SuggestionsComponent, DialogSpinnerComponent } from './components/suggestions/suggestions.component';
import { TableResultsComponent } from './components/table-results/table-results.component';
import { ShowTablesResultsComponent } from './components/show-tables-results/show-tables-results.component';
import { ResultsTriesComponent } from './components/results-tries/results-tries.component';
import { TableTriesComponent } from './components/table-tries/table-tries.component';
import { TipsComponent } from './components/tips/tips.component';
import { ShowSuggestionsComponent } from './components/show-suggestions/show-suggestions.component';
import { AveragesComponent } from './components/averages/averages.component';
import { TableAveragesComponent } from './components/table-averages/table-averages.component';
import { TableAveragesPruebasComponent } from './components/table-averages-pruebas/table-averages-pruebas.component';
import { ShowQuestionsComponent } from './components/show-questions/show-questions.component';


// Configs
export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
      [
        {
          id: FacebookLoginProvider.PROVIDER_ID,
          provider: new FacebookLoginProvider('967835536729020')
        },
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider('895792055053-rj191gkg71ms9lscb2bn94u4eebf2vts.apps.googleusercontent.com')
        }
      ]
  );
  return config;
}
@NgModule({
  declarations: [
    AppComponent,
    HomeScreenComponent,
    RecoverPasswordComponent,
    LoginComponent,
    NavbarComponent,
    FooterComponent,
    RegisterComponent,
    IndexUserComponent,
    NavbarUserComponent,
    SettingUserComponent,
    AdminComponent,
    AddQuestionComponent,
    AddTestComponent,
    ErrorComponent,
    DialogComponent,
    DialogQuestionComponent,
    DialogTestComponent,
    DialogSectionComponent,
    DialogConfirmationComponent,
    DialogInformationComponent,
    HelpDialogComponent,
    HelpDialogAdminComponent,
    TableUsersComponent,
    TableQuestionsComponent,
    TableTestsComponent,
    TestSectionComponent,
    EndTestComponent,
    ResultsComponent,
    AutoDatePickerDirective,
    ResourcesComponent,
    TestStartComponent,
    TableExamenIndexComponent,
    TableTestIndexComponent,
    VocabularyComponent,
    SuggestionsComponent,
    DialogSpinnerComponent,
    TableResultsComponent,
    ShowTablesResultsComponent,
    ResultsTriesComponent,
    TableTriesComponent,
    TipsComponent,
    ShowSuggestionsComponent,
    AveragesComponent,
    TableAveragesComponent,
    TableAveragesPruebasComponent,
    ShowQuestionsComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SocialLoginModule,
    AngularFireModule.initializeApp(environment.firebase, 'angular-auth-firebase'),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  providers: [
    {
        provide: AuthServiceConfig,
        useFactory: getAuthServiceConfigs
      },
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
