var AdminDAO = require('../model/admin').AdminDAO; //carga de archvo para el acceso a la base de datos
const nodemailer = require('nodemailer');

const serverEmail="preicfesparasordos.com";

const Youtube = require("youtube-api");

var readJson = require("r-json");

const CREDENTIALS = readJson('./credentials.json');

var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
           user: 'proyectopreicfessordos@gmail.com',
           pass: '3166240253'
       }
   });

   
   if (serverEmail=="preicfesparasordos.com")serverYoutube=CREDENTIALS.web.redirect_uris[1];
   else serverYoutube=CREDENTIALS.web.redirect_uris[0];

   var  oauth = Youtube.authenticate({
       type: "oauth"
     , client_id: CREDENTIALS.web.client_id
     , client_secret: CREDENTIALS.web.client_secret
     , redirect_url: serverYoutube
   });
 




function AdminHandler(io,db, fs,ObjectID,async) { //cargamos exactamente las mismas variables que vienen de index.js es decir recibe las variables
    "use strict";

    var admin = new AdminDAO(db,ObjectID,async,fs); // Data Acces Object para la base de datos



    /*let oauth = Youtube.authenticate({
        type: "oauth"
      , client_id: CREDENTIALS.web.client_id
      , client_secret: CREDENTIALS.web.client_secret
      , redirect_url: CREDENTIALS.web.redirect_uris[0]
    });
    
    opn(oauth.generateAuthUrl({
        access_type: "offline"
      , scope: ["https://www.googleapis.com/auth/youtube.upload"]
    }));*/
















    //gets

    this.getUsers = function(req, res, next) { 
        "use strict";

        
         admin.validateToken(req.headers.iduser,req.headers.token, function(stateToken){        
            "use strict";


    if (stateToken) {
        
        admin.getUsers( function(state, usuarios){        
            "use strict";
    
            res.send({state:state, users:usuarios});

        }

        );

    } else res.send({state:3, users:[] });

    }

    );

    }

    this.getQuestions = function(req, res, next) { 
        "use strict";

        admin.validateToken(req.headers.iduser,req.headers.token, function(stateToken){        
            "use strict";


    if (stateToken) {

        admin.getQuestions( function(state, preguntas){        
        "use strict";

        //console.log(preguntas)

        res.send({state:state, questions:preguntas});

        }

        );

    } else  res.send({state:3, questions:[]}); 

    }

    );


    }

    
    this.getTests = function(req, res, next) { 
        "use strict";

        admin.validateToken(req.headers.iduser,req.headers.token, function(stateToken){        
            "use strict";


    if (stateToken) {

        admin.getTests( function(state, pruebas){        
        "use strict";

        //console.log(pruebas)

        res.send({state:state, tests:pruebas});

        }

        );
    } else res.send({state:3, tests:[] });
    }

    );
    }
    

    this.getQuestionsSubject = function(req, res, next) { 
        "use strict";
        
        var subject= req.params.subject;

        admin.validateToken(req.headers.iduser,req.headers.token, function(stateToken){        
            "use strict";


    if (stateToken) {

        admin.getQuestionsSubject( subject, function(state, preguntas){        
        "use strict";
        
        console.log(preguntas)

        res.send({state:state, questions:preguntas});

        }

        );

    } else res.send({state:3, questions:[] });

    }

    );
    }

    this.getTestSubType = function(req, res, next) { 
        "use strict";
        
        var subType= req.params.subType;


        admin.validateToken(req.headers.iduser,req.headers.token, function(stateToken){        
            "use strict";


    if (stateToken) {

        admin.getTestSubType( subType, function(state, pruebas){        
        "use strict";
        
        console.log(pruebas)

        res.send({state:state, tests:pruebas});

        }

        );
    } else res.send({ state:3, tests:[] });

    }

    );
    }    

    this.getLoadVideos = function(req, res, next) { 
        "use strict";
        
        //console.log(req.headers);

        admin.validateToken(req.headers.iduser,req.headers.token, function(stateToken){        
            "use strict";


    if (stateToken) {

        

        
       /* oauth.generateAuthUrl({
            access_type: "offline"
          , scope: ["https://www.googleapis.com/auth/youtube.upload"]
        })*/

            var jsonUrlYoutubeSend={
                url:oauth.generateAuthUrl({
                access_type: "offline"
            , scope: ["https://www.googleapis.com/auth/youtube.upload"]
            })
            };

            jsonUrlYoutubeSend["state"]=1;

            console.log(jsonUrlYoutubeSend)
        res.send(
            jsonUrlYoutubeSend
        );


        

    } else res.send({state:3, url:""  });

    }

    );
    }

    this.getAverage = function(req, res, next) { 
        "use strict";
        
        console.log(req.headers);

        admin.validateToken(req.headers.iduser,req.headers.token, function(stateToken){        
            "use strict";


    if (stateToken) {

        admin.getAverage( req.headers.idtest, function(state, estadisticas){        
        "use strict";

        console.log(estadisticas)

        res.send({state:state, stats:estadisticas});

        }

        );

    } else res.send({state:3, user:{}  });

    }

    );
    }

    this.getUserById = function(req, res, next) { 
        "use strict";
        
        console.log(req.headers);

        admin.validateToken(req.headers.idadmin,req.headers.token, function(stateToken){        
            "use strict";


    if (stateToken) {

        admin.getUserById( req.headers.iduser, function(state, docUser){        
        "use strict";

        res.send({state:state, user:docUser});

        }

        );

    } else res.send({state:3, user:{}  });

    }

    );
    }

    this.getDataInicio = function(req, res, next) { 
        "use strict";
        
        console.log(req.headers);

        admin.validateToken(req.headers.iduser,req.headers.token, function(stateToken){        
            "use strict";


    if (stateToken) {

        admin.getDataInicio( req.headers.iduser, function(state, docUser){        
        "use strict";

        res.send({state:state, user:docUser});

        }

        );

    } else res.send({state:3, user:{}  });

    }

    );
    }

    this.getTest = function(req, res, next) { 
        "use strict";
        
        var idprueba= req.params.idprueba;

        admin.validateToken(req.headers.iduser,req.headers.token, function(stateToken){        
            "use strict";


    if (stateToken) {

        admin.getTest( idprueba, function(state, prueba){        
        "use strict";

        res.send({state:state, test:prueba});

        }

        );

    } else res.send({state:3, test:[]  });

    }

    );
    }

    this.getTestByUser = function(req, res, next) { 
        "use strict";


        
        var idusuario= req.headers.iduser;
        var subType= req.params.subType;

        admin.validateToken(req.headers.iduser,req.headers.token, function(stateToken){        
            "use strict";


    if (stateToken) {
        

        admin.getTestByUser( idusuario,subType, function(state, prueba){        
        "use strict";

        res.send({state:state, test:prueba});

        }

        );
    } else res.send({state:3, test:{}});

    }

    );
    }


    this.callbackYoutubeApiGet = function(reqExp, res, next) { 
        "use strict";



        //console.log(reqExp);

        oauth.getToken(reqExp.query.code, (err, tokens) => {

            if (err) {
                console.log(err);
                return res.send("Error de token de youtube. Vuelva a intentarlo");
            }
    
            console.log("Got the tokens.");
    
            oauth.setCredentials(tokens);
    
           console.log("The video is being uploaded. Check out the logs in the terminal.");


           admin.callbackYoutubeApiGet(  function(state, videos){        
            "use strict";
    
            if (state) {

                async.forEachOf(videos, function (value, key, callbackAs) {

                    console.log("video es " + value.videoUploaded);

                    console.log(value);

                    Youtube.videos.insert({
                        resource: {
                            // Video title and description
                            snippet: {
                                title: value.titulo
                            , description: value.descripcion
                            }
                            // I don't want to spam my subscribers
                        , status: {
                                privacyStatus: "private"
                            }
                        }
                        // This is for the callback function
                    , part: "snippet,status"
            
                        // Create the readable stream to upload the video
                    , media: {
                            body: fs.createReadStream("./public"+value.videoUploaded)
                        }
                    }, (err, data) => {

                        if (err){ 
                            
                            console.log("el error es");
                            console.log(err);
                            return callbackAs(true);

                        }

                        var urlFinalYoutube="https://www.youtube.com/watch?v="+data.id;

                        admin.updateAdminYoutube(urlFinalYoutube,value, function(stateUpdate){        
                            "use strict";

                            if (stateUpdate) {
                                    
                                console.log("Done.");   

                                return callbackAs();
                                
                            }else return callbackAs(true);
      


                        });
                
                               
                    });
        
        
                }, function (state) {
        
                    if (state) return res.send("Error. Se ha alcanzado el límite diario de subir videos a YouTube");
        
                       
                           return res.status(200).send('Videos subidos correctamente!');                                            
        
        
                        });  
                



                
            } else  res.send("Error en obtener las sugerencias");




    
            });
    

    

        });


    }


    this.getTry = function(req, res, next) { 
        "use strict";



        var idprueba= req.params.idprueba;

        admin.validateToken(req.headers.iduser,req.headers.token, function(stateToken){        
            "use strict";


    if (stateToken) {
        

        admin.getTry( idprueba, function(state, prueba){        
        "use strict";

        res.send({state:state, test:prueba});

        }

        );
    } else res.send({state:3, test:{}});

    }

    );
    }    

    this.postRecoverPassword = function(req, res, next) { 
        "use strict";
        
        var token= req.body.token,
        password= req.body.password;

        
        admin.postRecoverPassword( token,password, function(state){        
        "use strict";

        res.send({state:state});

        }

        );
    }

    this.getQuestion = function(req, res, next) { 
        "use strict";
        
        var idpregunta= req.params.idpregunta;

        admin.validateToken(req.headers.iduser,req.headers.token, function(stateToken){        
            "use strict";


    if (stateToken) {

        admin.getQuestion( idpregunta, function(state, pregunta){        
        "use strict";

        res.send({state:state, question:pregunta});

        }

        );

    } else res.send({state:3, question:[]  });


    }

    );
    }



    //posts



    
    this.postRegister = function(req, res, next) { 
        "use strict";
        
        console.log(req.body);        
        admin.postRegister(req.body, function(state, id_usuario){        
        "use strict";

        res.send({state:state, id_usuario:id_usuario});

        }

        );
    } 


          

    this.postLogin = function(req, res, next){
        "use strict";

        console.log(req.body);
        admin.postLogin(req.body, function(state,doc_user){
        "use strict";        
        
        res.send({state:state, doc_user:doc_user,token:doc_user.token});


        }
        );
    }

    this. postAddQuestion = function(req, res, next) { 
        "use strict";

        console.log(req.body);

        
        admin.validateToken(req.body.idUser,req.body.token, function(stateToken){        
            "use strict";


    if (stateToken) {

        var idpregunta=ObjectID();
        
        if (req.body.image!=null) {

            req.body.image =  req.body.image.substring(req.body.image.indexOf("base64,")+7);
            fs.writeFile('./public/imagenes/preguntas/' + 'imagen'+idpregunta+'.jpg', req.body.image, 'base64', function(err) {
            console.log(err);
            });
            req.body.image='/imagenes/preguntas/' + 'imagen'+idpregunta+'.jpg';    
            
        }

        if (req.body.imageAnswerA!=null) {

            req.body.imageAnswerA =  req.body.imageAnswerA.substring(req.body.imageAnswerA.indexOf("base64,")+7);
            fs.writeFile('./public/imagenes/preguntas/' + 'imageAnswerA'+idpregunta+'.jpg', req.body.imageAnswerA, 'base64', function(err) {
            console.log(err);
            });
            req.body.imageAnswerA='/imagenes/preguntas/' + 'imageAnswerA'+idpregunta+'.jpg';    
            
        }

        if (req.body.imageAnswerB!=null) {

            req.body.imageAnswerB =  req.body.imageAnswerB.substring(req.body.imageAnswerB.indexOf("base64,")+7);
            fs.writeFile('./public/imagenes/preguntas/' + 'imageAnswerB'+idpregunta+'.jpg', req.body.imageAnswerB, 'base64', function(err) {
            console.log(err);
            });
            req.body.imageAnswerB='/imagenes/preguntas/' + 'imageAnswerB'+idpregunta+'.jpg';    
            
        }

        if (req.body.imageAnswerC!=null) {

            req.body.imageAnswerC =  req.body.imageAnswerC.substring(req.body.imageAnswerC.indexOf("base64,")+7);
            fs.writeFile('./public/imagenes/preguntas/' + 'imageAnswerC'+idpregunta+'.jpg', req.body.imageAnswerC, 'base64', function(err) {
            console.log(err);
            });
            req.body.imageAnswerC='/imagenes/preguntas/' + 'imageAnswerC'+idpregunta+'.jpg';    
            
        }

        if (req.body.imageAnswerD!=null) {

            req.body.imageAnswerD =  req.body.imageAnswerD.substring(req.body.imageAnswerD.indexOf("base64,")+7);
            fs.writeFile('./public/imagenes/preguntas/' + 'imageAnswerD'+idpregunta+'.jpg', req.body.imageAnswerD, 'base64', function(err) {
            console.log(err);
            });
            req.body.imageAnswerD='/imagenes/preguntas/' + 'imageAnswerD'+idpregunta+'.jpg';    
            
        }


        req.body._id=idpregunta;
        req.body.state=true;

        delete req.body.idUser;
        delete req.body.token;

        admin.postAddQuestion(req.body, function(error){        
        "use strict";

        if(error){
            res.send({state:false});
        }
        res.send({state:true});
        }

        );

        } else  return res.send({state:3});
    }

    );

    
    } 

    this.postAddTest = function(req, res, next) { 
        "use strict";

        console.log(req.body);

        admin.validateToken(req.body.idUser,req.body.token, function(stateToken){        
            "use strict";


    if (stateToken) {


        var title= req.body.title,
            description=req.body.description,
            type=req.body.type,
            subType=req.body.subType,
            session1Area1= req.body.session1area1,
            session1Area2= req.body.session1area2,
            session2Area1= req.body.session2area1,
            session2Area2= req.body.session2area2,
            sess1SubType=req.body.sess1SubType,
            sess2SubType=req.body.sess2SubType;


        admin.postAddTest(title,description,type,subType,session1Area1,session1Area2,session2Area1,session2Area2,sess1SubType,sess2SubType, function(state){        
        "use strict";

        
        res.send({state:state});

        
        }

        );
    } else  res.send({state:3});

        }

        );
    } 

    this. postIniciarPrueba = function(req, res, next) { 
        "use strict";

        console.log(req.body);

        admin.validateToken(req.body.idUser,req.body.token, function(stateToken){        
            "use strict";


    if (stateToken) {

        admin.postIniciarPrueba(req.body.idTest, req.body.idUser, function(state,testsUpdated){        
        "use strict";

            
                res.send({state:state,testsUpdated:testsUpdated});

                
                }

            );

    } else return  res.send({state:3 });

                            
    }

    );


    }  

    
    this. postEnviarSugerencia = function(req, res, next) { 
        "use strict";

        //console.log(req.body);

        req.body.video = req.body.video.replace(/^data:(.*?);base64,/, ""); // <--- make it any type
        req.body.video = req.body.video.replace(/ /g, '+'); // <--- this is important

        var nombreID=ObjectID();

        fs.writeFile('./public/videos/sugerencias'+nombreID+'.mp4', req.body.video, 'base64', function(err) {
        console.log(err);
        });
        var videoUploaded='/videos/sugerencias'+nombreID+'.mp4';  

        admin.validateToken(req.body.idUser,req.body.token, function(stateToken){        
            "use strict";


    if (stateToken) {

        

        admin.postEnviarSugerencia(req.body.idUser,videoUploaded, function(state){        
        "use strict";

            
                res.send({state:state});

                
                }

            );

    } else return  res.send({state:3 });

                            
    }

    );


    }      


    
    this. postIniciarPruebaIntento = function(req, res, next) { 
        "use strict";

        console.log(req.body);

        admin.validateToken(req.body.idUser,req.body.token, function(stateToken){        
            "use strict";


    if (stateToken) {

        admin.postIniciarPruebaIntento(req.body.idTest, req.body.idUser, function(state,testsUpdated){        
        "use strict";

            
                res.send({state:state,testsUpdated:testsUpdated});

                
                }

            );

    } else return  res.send({state:3 });

                            
    }

    );


    }      

    this.postAnswer = function(req, res, next) { 
        "use strict";

        console.log(req.body);

        admin.validateToken(req.body.idUser,req.body.token, function(stateToken){        
            "use strict";


    if (stateToken) {

        var idusuario= req.body.idUser,
            idprueba=req.body.idTest,
            idpregunta=req.body.idQuestion,
            resp=req.body.answer,
            ultima= req.body.last,
            session= req.body.session,
            area =req.body.area,
            intento=req.body.try,
            tiempo=req.body.time;
           

        admin.postAnswer(idusuario,idprueba,idpregunta,resp,ultima,intento,session,area,tiempo, function(state,prueba){        
        "use strict";

                console.log({state:state,test:prueba});
                res.send({state:state,test:prueba});
                

                
                }

            );

    } else return  res.send({state:3,test:{}});

                            
    }

    );


    } 

    this.postRecover = function(req, res, next) { 
        "use strict";

        console.log(req.body);

        var email= req.body.email;

        admin.postRecover(email, function(state,nombre,tokenPass){        
        "use strict";


        if (state) {
                    
            // setup email data with unicode symbols
            let mailOptions = {
                from: '"Proyecto PreIcfes Sordos  " <info@proyectopreicfessordos.com>', // sender address
                to: email, // list of receivers
                subject: 'Restablecer la contraseña 🔓 ', // Subject line                
                html: 'Estimado/a <b>'+nombre+'</b> <br> <br> Recientemente solicitaste el restablecimiento de tu contraseña, haz click en el siguiente enlace para continuar: <br><br><a href="http://'+serverEmail+'/recover/password?token='+tokenPass+'">Restablecer contraseña</a><br><br> Si no realizaste esta modificación o si crees que alguien ha accedido a tu cuenta sin autorización, ingresa y actualiza tu contraseña<br><br> Atentamente,<br><br> <b>Soporte técnico de Proyecto Preicfes Sordos</b>' // html body
            };

            // send mail with defined transport object
            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    console.log("Error enviando el correo")
                    console.log(error)
                }
                console.log('Message sent: %s', info.messageId);
                // Preview only available when sending through an Ethereal account
                console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

            });
            
        }

            
                return res.send({state:state});
                

                

                
                }

            );
    }     



    // put


    
    this.putUpdateQuestion = function(req, res, next) { 
        "use strict";

        console.log(req.body);

        admin.validateToken(req.body.idUser,req.body.token, function(stateToken){        
            "use strict";


    if (stateToken) {


        var idpregunta=req.body._id;
        
        if (req.body.image!=null) {

            if (req.body.image!="misma") {

                req.body.image =  req.body.image.substring(req.body.image.indexOf("base64,")+7);
                fs.writeFile('./public/imagenes/preguntas/' + 'imagen'+idpregunta+'.jpg', req.body.image, 'base64', function(err) {
                console.log(err);
                });                                
            }
            req.body.image='/imagenes/preguntas/' + 'imagen'+idpregunta+'.jpg';             
        }else {

            fs.stat('./public/imagenes/preguntas/' + 'imagen'+idpregunta+'.jpg', function(err, stat) {
                if(err == null) {
                    fs.unlink('./public/imagenes/preguntas/' + 'imagen'+idpregunta+'.jpg', (err) => {
                        if (err) throw err;
                        
                    });
                } 
            });


        }

        if (req.body.imageAnswerA!=null) {


            if (req.body.imageAnswerA!="misma") {

                req.body.imageAnswerA =  req.body.imageAnswerA.substring(req.body.imageAnswerA.indexOf("base64,")+7);
                fs.writeFile('./public/imagenes/preguntas/' + 'imageAnswerA'+idpregunta+'.jpg', req.body.imageAnswerA, 'base64', function(err) {
                console.log(err);
                });
                
            }
            req.body.imageAnswerA='/imagenes/preguntas/' + 'imageAnswerA'+idpregunta+'.jpg';    
            
        }else {

            fs.stat('./public/imagenes/preguntas/' + 'imageAnswerA'+idpregunta+'.jpg', function(err, stat) {
                if(err == null) {
                    fs.unlink('./public/imagenes/preguntas/' + 'imageAnswerA'+idpregunta+'.jpg', (err) => {
                        if (err) throw err;
                        
                    });
                } 
            });


        }

        if (req.body.imageAnswerB!=null) {


            if (req.body.imageAnswerB!="misma") {

                
                req.body.imageAnswerB =  req.body.imageAnswerB.substring(req.body.imageAnswerB.indexOf("base64,")+7);
                fs.writeFile('./public/imagenes/preguntas/' + 'imageAnswerB'+idpregunta+'.jpg', req.body.imageAnswerB, 'base64', function(err) {
                console.log(err);
                });
                
            }

            req.body.imageAnswerB='/imagenes/preguntas/' + 'imageAnswerB'+idpregunta+'.jpg';    
            
        }else {

            fs.stat('./public/imagenes/preguntas/' + 'imageAnswerB'+idpregunta+'.jpg', function(err, stat) {
                if(err == null) {
                    fs.unlink('./public/imagenes/preguntas/' + 'imageAnswerB'+idpregunta+'.jpg', (err) => {
                        if (err) throw err;
                        
                    });
                } 
            });


        }

        if (req.body.imageAnswerC!=null) {

            if (req.body.imageAnswerC!="misma") {

                req.body.imageAnswerC =  req.body.imageAnswerC.substring(req.body.imageAnswerC.indexOf("base64,")+7);
                fs.writeFile('./public/imagenes/preguntas/' + 'imageAnswerC'+idpregunta+'.jpg', req.body.imageAnswerC, 'base64', function(err) {
                console.log(err);
                });
                
            }

            req.body.imageAnswerC='/imagenes/preguntas/' + 'imageAnswerC'+idpregunta+'.jpg';    
            
        }else {

            fs.stat('./public/imagenes/preguntas/' + 'imageAnswerC'+idpregunta+'.jpg', function(err, stat) {
                if(err == null) {
                    fs.unlink('./public/imagenes/preguntas/' + 'imageAnswerC'+idpregunta+'.jpg', (err) => {
                        if (err) throw err;
                        
                    });
                } 
            });


        }

        if (req.body.imageAnswerD!=null) {

            if (req.body.imageAnswerD!="misma") {

                req.body.imageAnswerD =  req.body.imageAnswerD.substring(req.body.imageAnswerD.indexOf("base64,")+7);
                fs.writeFile('./public/imagenes/preguntas/' + 'imageAnswerD'+idpregunta+'.jpg', req.body.imageAnswerD, 'base64', function(err) {
                console.log(err);
                });
                
            }
            req.body.imageAnswerD='/imagenes/preguntas/' + 'imageAnswerD'+idpregunta+'.jpg';    
            
        }else {

            fs.stat('./public/imagenes/preguntas/' + 'imageAnswerD'+idpregunta+'.jpg', function(err, stat) {
                if(err == null) {
                    fs.unlink('./public/imagenes/preguntas/' + 'imageAnswerD'+idpregunta+'.jpg', (err) => {
                        if (err) throw err;
                        
                    });
                } 
            });


        }




        delete req.body.idUser;
        delete req.body.token;
      
        admin.putUpdateQuestion(req.body, function(state){        
        "use strict";

       

        res.send({state:state});


        }

        );
    } else return    res.send({state:3});
        
    }

    );

    }

    this.putUpdateUser = function(req, res, next) { 
        "use strict";

        console.log(req.body);


        admin.validateToken(req.body._id,req.body.token, function(stateToken){        
            "use strict";


            if (stateToken) {

                if (req.body.picture!=null) {

                    req.body.picture =  req.body.picture.substring(req.body.picture.indexOf("base64,")+7);
                    fs.writeFile('./public/imagenes/usuarios/' + 'imagen'+req.body._id+'.jpg', req.body.picture, 'base64', function(err) {
                    console.log(err);
                    });                       
                }
                req.body.picture='/imagenes/usuarios/' + 'imagen'+req.body._id+'.jpg';    
              
                admin.putUpdateUser(req.body, function(state){        
                "use strict";
        
               
        
                res.send({state:state});
        
        
                });
                
            } else return  res.send({state:3});
    
        
    
    
            });



            
   

       

    }    

    
    this.putUpdateTest = function(req, res, next) { 
        "use strict";

        console.log(req.body);

    admin.validateToken(req.body.idUser,req.body.token, function(stateToken){        
    "use strict";

        

    if (stateToken) {

        var title= req.body.title,
            description=req.body.description,
            type=req.body.type,
            subType=req.body.subType,
            session1Area1= req.body.session1area1,
            session1Area2= req.body.session1area2,
            session2Area1= req.body.session2area1,
            session2Area2= req.body.session2area2,
            sess1SubType=req.body.sess1SubType,
            idprueba=req.body._id,
            sess2SubType=req.body.sess2SubType;


        admin.putUpdateTest(title,description,type,subType,session1Area1,session1Area2,session2Area1,session2Area2,sess1SubType,sess2SubType,idprueba, function(state){        
        "use strict";

        
        res.send({state:state});




        }

        );

    } else res.send({state:3});
        
        }

        );
    } 

    this.putDeactivateUsers = function(req, res, next) { 
        "use strict";

        console.log(req.body);

            admin.validateToken(req.body.idUser,req.body.token, function(stateToken){        
            "use strict";


            if (stateToken) {
      
        admin.putDeactivateUsers(req.body.ids, function(state){        
        "use strict";

       

        res.send({state:state});


        }

        );

    } else return res.send({state:3});


        
    }

    );

    } 

    this.putActivateUsers = function(req, res, next) { 
        "use strict";

        console.log(req.body);

            admin.validateToken(req.body.idUser,req.body.token, function(stateToken){        
            "use strict";


            if (stateToken) {
      
        admin.putActivateUsers(req.body.ids, function(state){        
        "use strict";

       

        res.send({state:state});


        }

        );

    } else return res.send({state:3});


        
    }

    );

    } 

    this.putDeactivateTests = function(req, res, next) { 
        "use strict";

        console.log(req.body);

        admin.validateToken(req.body.idUser,req.body.token, function(stateToken){        
            "use strict";


            if (stateToken) {

        
      
        admin.putDeactivateTests(req.body.ids, function(state){        
        "use strict";

       

        res.send({state:state});


        }

        );

    }

    }

    );


    } 

    this.putActivateTests = function(req, res, next) { 
        "use strict";

        console.log(req.body);

        admin.validateToken(req.body.idUser,req.body.token, function(stateToken){        
            "use strict";


            if (stateToken) {

        
      
        admin.putActivateTests(req.body.ids, function(state){        
        "use strict";

       

        res.send({state:state});


        }

        );

    }

    }

    );


    }

    this.putActivateQuestions = function(req, res, next) { 
        "use strict";

        console.log(req.body);


        admin.validateToken(req.body.idUser,req.body.token, function(stateToken){        
            "use strict";


    if (stateToken) {
        
      
        admin.putActivateQuestions(req.body.ids, function(state){        
        "use strict";

       

        res.send({state:state});
    }

    );

    } else res.send({state:3});

        }

        );

    }   

    this.putDeactivateQuestions = function(req, res, next) { 
        "use strict";

        console.log(req.body);


        admin.validateToken(req.body.idUser,req.body.token, function(stateToken){        
            "use strict";


    if (stateToken) {
        
      
        admin.putDeactivateQuestions(req.body.ids, function(state){        
        "use strict";

       

        res.send({state:state});
    }

    );

    } else res.send({state:3});

        }

        );

    }     

    //delete 


    //Support Functions


    function isJson(item) { //get no se puede enviar json entonces lo convierte
        item = typeof item !== "string" //socket si se puede enviar json
            ? JSON.stringify(item) //json es un clave valor, direcciones, objeto
            : item; //{nombre:"elkin", paises:["colombia","ecuador"]};
    
        try {
            item = JSON.parse(item);
        } catch (e) {
            return false;
        }
    
        if (typeof item === "object" && item !== null) {
            return item;
        }
    
        return false;
    }



}

module.exports = AdminHandler; //al crear la instancia es lo que exportara