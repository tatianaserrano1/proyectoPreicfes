var multipart = require('connect-multiparty') //Modulo para enviar datos desde el cliente tipo post.
	,AdminHandler = require('./adminHandler') 
	,multipartMiddleware = multipart() //instancia de el objeto multipart (constructor)
	,fs = require('fs') //file server -> libreria para controlar archivos (guardar,recibir y leer archivos, etc)
	,async = require('async')
	,ObjectID = require('mongodb').ObjectID;

	
module.exports = exports = function(app,db, io) { //cargamos exactamente las mismas variables que vienen de app.js es decir recibe las variables


			var admin = new AdminHandler(io,db,fs,ObjectID,async); //tablas colecciones, registros doc json 


			app.use(function (req, res, next) {
				//AJAX - FORM DINAMICO
					// Website you wish to allow to connec
					res.setHeader('Access-Control-Allow-Origin', '*');
				
					// Request methods you wish to allow
					res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
				
					// Request headers you wish to allow
					res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Authorization,token,idUser');
				
					// Set to true if you need the website to include cookies in the requests sent
					// to the API (e.g. in case you use sessions)
					res.setHeader('Access-Control-Allow-Credentials', true);
				
					// Pass to next layer of middleware
					next();
				});



			//post 			

				

				app.post('/register', admin.postRegister);				

				app.post('/login', admin.postLogin);

				app.post('/add-question', admin.postAddQuestion); 
				
				app.post('/addTest', admin.postAddTest);

				app.post('/answer', admin.postAnswer);

				app.post('/recover', admin.postRecover);

				app.post('/recover/password', admin.postRecoverPassword);

				app.post('/start', admin.postIniciarPrueba);

				app.post('/startTry', admin.postIniciarPruebaIntento);

				app.post('/video', admin.postEnviarSugerencia);				

				


			//gets

				app.get('/questions', admin.getQuestions);

				app.get('/tests', admin.getTests); 

				app.get('/questions/:subject', admin.getQuestionsSubject); 

				//app.get('/test/:subType', admin.getTestSubType); //ya sesión
				 
				app.get('/users', admin.getUsers); 

				app.get('/getTest/:idprueba', admin.getTest); 

				app.get('/question/:idpregunta', admin.getQuestion); 

				app.get('/test/:subType', admin.getTestByUser); 

				app.get('/testTry/:idprueba', admin.getTry);

				app.get('/oauth2callback', admin.callbackYoutubeApiGet);

				app.get('/data', admin.getDataInicio);

				app.get('/loadVideos', admin.getLoadVideos);

				app.get('/userById', admin.getUserById);

				app.get('/average', admin.getAverage);

				

				

				

				

			app.put('/updateQuestion', admin.putUpdateQuestion); 

			app.put('/updateTest', admin.putUpdateTest); 

			app.put('/updateUser', admin.putUpdateUser); 

			app.put('/deactivateUsers', admin.putDeactivateUsers); 

			app.put('/activateUsers', admin.putActivateUsers); 

			app.put('/deactivateTest', admin.putDeactivateTests); 

			app.put('/activateTest', admin.putActivateTests); 

			app.put('/deactivateQuestions', admin.putDeactivateQuestions); 

			app.put('/activateQuestions', admin.putActivateQuestions); 

			//delete
			

				

								


		io.sockets.on('connection', function (socket) {  //conexión de websockets para atrapar o "handle" las peticiones socket.io




		});

}