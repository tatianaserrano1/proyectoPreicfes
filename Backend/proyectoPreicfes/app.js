

//Para más información  de cada librería busquen por su nombre en google
var express = require('express') // Express Routing - Manejador de peticiones http-s
//este sirve para conectar el servidor con el front end, en express pregunta con ese index
   , favicon = require('serve-favicon') //icono en el navegador de la pag
  , app = require('express')() // creando el servidor con socket.io mismo express inicializacion, carga libreria express
  , server = require('http').createServer(app) //crea el servidor agregando la app
  , io = require('socket.io').listen(server) // socket es una libreria de comunicacion bidireccional entre frontend y back- tcp ip, envia y trae mensaje
  ,MongoClient = require('mongodb').MongoClient //carga base de datos mongo
  , routes = require('./routes') // Modulo de rutas, carga una carpeta en el servidor que va a tener todo el enrutamiento
  ,servidorbd= "localhost" //Servidor de la BD
    ,puertobd= "27017", //predet
    morgan  = require('morgan'),//
    cors= require('cors'),
    bodyParser=require('body-parser'); //cuando se hace post desde html morgan y body parser la transforma en formato mas facil de leer 
    

    // Configuracion del express

MongoClient.connect('mongodb://'+servidorbd+':'+puertobd+'/preicfes', function(err, db) { //Se crea la unica conexion en el servidor de la bd
    "use strict"; // tecnicas buenas de programacion // se conecta la base de datos //function anonima, objetos, dice errores en la conexion
    if(err) throw err;

    // Configuracion del express

    var db= db.db('preicfes');
    
    app.set('views', __dirname + '/views'); //muestra que en views o public vistas de tatiana
    app.set('view engine', 'ejs');
    app.use(morgan('dev'));
    app.use(cors());
    app.use(bodyParser.json({limit: '50mb'}));
    app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));        
    app.use(express.static(__dirname + '/public')); //donde esta lo publico que accede sin solicitud
    app.disable('x-powered-by');


    // Pasamos al modulo la conexion app del servidor, db de la base de datos, io de socket.io
    routes(app, db,io); 


    server.listen(1641); //socket funcionando en puerto 0641
    console.log('Todo fue un exito!!! 1641');
});

