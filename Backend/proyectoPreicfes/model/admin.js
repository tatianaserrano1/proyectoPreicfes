
var crypto = require('crypto');

function AdminDAO(db,ObjectID,async,fs) {

    /* If this constructor is called without the "new" operator, "this" points
     * to the global object. Log a warning and call it correctly. */
    if (false === (this instanceof AdminDAO)) {
        console.log('Warning: PostsDAO constructor called without "new" operator');
        return new AdminDAO(db,ObjectID,async,fs);
    }

    //Colecciones a usar en este modelo
    var usuarios = db.collection("usuarios");
    var preguntas = db.collection("preguntas");
    var pruebas = db.collection("pruebas");
    var estadisticas= db.collection("estadisticas");

    //Estados de registro, 0: correo ya existe, 1:Registro existoso, 2: error de servidor
    //Estados de login, 0: email o contraseña incorrecta, 1:login exitoso, 2: error de servidor

    

    this.getUsers = function( callback){
        "use strict";

        usuarios.find({tipo:0},{projection:{password:0,tipo:0}}).toArray(function(err,result){
            "use strict";
            
            if (err) return callback(false,[]);

            //console.log(result);

            //

            return callback(true,result);

        });


    }

    this.getQuestions = function( callback){
        "use strict";

        preguntas.aggregate([{ $match : { } },{ $group : {  _id :"$subject" , preguntas:{$push: "$$ROOT" } } } ]).toArray(function(err,result){
            "use strict";
            
            if (err) return callback(false,[]);

            //

            return callback(true,result);



        });


    }

    this.getQuestionsSubject = function(subject, callback){
        "use strict";

        preguntas.find({subject:subject,state:true}).toArray(function(err,result){
            "use strict";
            
            if (err) return callback(false,[]);

            console.log(result);

            //

            return callback(true,result);



        });


    }


    this.getTestSubType = function(subType, callback){
        "use strict";

        pruebas.find({subType:subType}).toArray(function(err,result){
            "use strict";
            
            if (err) return callback(false,[]);

            

            //

            return callback(true,result);



        });        


}

    this.getTests = function(callback){
        "use strict";

        pruebas.find({}).toArray(function(err,result){
            "use strict";
            
            if (err) return callback(false,[]);

            

            //

            return callback(true,result);



        });


    }

    this.getTest = function(idprueba, callback){
        "use strict";

        pruebas.findOne({_id:ObjectID(idprueba)}, function(err, docPrueba){
            "use strict";
            
            if (err) return callback(false,{});

            console.log(docPrueba);

            

            return callback(true,docPrueba);



        });


    }


    this.getTry = function(idprueba, callback){
        "use strict";


                pruebas.findOne({_id:ObjectID( idprueba)},{projection:{"questions.correctAnswer":0,"questions.state":0,state:0}}, function(err, docPrueba){
                    "use strict";
                    
                    if (err) return callback(false,{});
         

                    return callback(true,docPrueba);
            
                });

    
    }    

    this.getTestByUser = function(idusuario,subType, callback){
        "use strict";

       // usuarios.findOneAndUpdate({_id:ObjectID(idusuario), "pruebasNR._id":subType  } ,  { $pop:{"pruebasNR.$.pruebas":-1 }}, function(error, result){            
           // "use strict";

        usuarios.findOne( {_id:ObjectID(idusuario) } , function(error, docUsuario){            
                "use strict";
            
            if (error) return callback(false,{});
           
            //console.log(docUsuario);

            if (docUsuario==null) return callback(false,{})

            var idprueba;
            switch( subType ) {
                case "naturales":
                    idprueba=docUsuario.pruebasNR[0].pruebas[0];                    
                    break;
                case "largo":
                    idprueba=docUsuario.pruebasNR[1].pruebas[0];   
                    break;
                case "sociales":
                    idprueba=docUsuario.pruebasNR[2].pruebas[0];   
                    break;    
                case "lectura":
                    idprueba=docUsuario.pruebasNR[3].pruebas[0];  
                    break;
                case "corto":
                    idprueba=docUsuario.pruebasNR[4].pruebas[0];  
                    break;
                case "matematicas":
                    idprueba=docUsuario.pruebasNR[5].pruebas[0];  
                    break;        
                case "medio":
                    idprueba=docUsuario.pruebasNR[6].pruebas[0];  
                    break;                                               
                default:
                    console.log("ERROR HAY UN NUEVO tipo");
            }


            console.log(idprueba);

            if (idprueba==undefined) return callback(2,{});            
            else
            {

                pruebas.findOne({_id:idprueba},{projection:{"questions.correctAnswer":0,"questions.state":0,state:0}}, function(err, docPrueba){
                    "use strict";
                    
                    if (err) return callback(false,{});
                    

                   /* usuarios.updateOne({_id:ObjectID(idusuario), "pruebasR._id":subType  },  { $push:{"pruebasR.$.pruebas":{ _id:idprueba, intentos:[docPrueba]    }}}, function(error, result){
                        "use strict";
    
                        if(error){
                            return callback(false,{});
                        }
                     
                    });*/
    
                    
                    return callback(true,docPrueba);
    
    
    
                });

            }
                
        




        });


    }


    this.postRecoverPassword = function(token,password, callback){
        "use strict";

               
        usuarios.findOneAndUpdate(  {tokenPass:token },  { $set:{password:password,tokenPass:ObjectID()}  }, function(err, result){
            "use strict";
            
            if (err) return callback(false);

            var userDoc=result.value;

            console.log(userDoc);

            if (userDoc) return callback(true);
            else  return callback(false);

        });


    }

    this.getQuestion = function(idpregunta, callback){
        "use strict";

        preguntas.findOne({_id:ObjectID(idpregunta)}, function(err, docPregunta){
            "use strict";
            
            if (err) return callback(false,{});

            console.log(docPregunta);

            

            return callback(true,docPregunta);



        });


    }


    this.postRegister = function(user, callback){
        "use strict";

        
        usuarios.findOne({email:user.email}, function(error, docUsu){
            "use strict";
            if(error){
                return callback(2, "");
            }
            
            if (docUsu) {

                return callback(0,"");

                
            } else {


                var _id = new ObjectID();

                pruebas.aggregate([{ $match : { state : true } },{ $group : {  _id :"$subType" , pruebas:{$push: "$_id" } } } ]).toArray(function(err,docsPruebas){
                    "use strict";
                    
                    if (err) return callback(false,[]);

                 
                       
                    var docsPruebasTemp=[{_id:"naturales",pruebas:[]},{_id:"largo",pruebas:[]},
                    {_id:"sociales",pruebas:[]}, {_id:"lectura",pruebas:[]}, {_id:"corto",pruebas:[]},
                    {_id:"matematicas",pruebas:[] }, {_id:"medio",pruebas:[] }  ]; //si agregan un tipo, agregarlo también en  '/test/:subType/:idusuario', admin.getTestByUser, en postAnswer y en login

                        
                    var docspruebasR=[{_id:"naturales",pruebas:[]},{_id:"largo",pruebas:[]},
                    {_id:"sociales",pruebas:[]}, {_id:"lectura",pruebas:[]}, {_id:"corto",pruebas:[]},
                    {_id:"matematicas",pruebas:[]} , {_id:"medio",pruebas:[] }   ];

                        for (let i = 0; i < docsPruebas.length; i++) {
                            
                            switch(docsPruebas[i]._id ) {
                                case "naturales":
                                    docsPruebasTemp[0].pruebas=docsPruebas[i].pruebas;
                                    break;
                                case "largo":
                                    docsPruebasTemp[1].pruebas=docsPruebas[i].pruebas;
                                    break;
                                case "sociales":
                                    docsPruebasTemp[2].pruebas=docsPruebas[i].pruebas;
                                    break;    
                                case "lectura":
                                    docsPruebasTemp[3].pruebas=docsPruebas[i].pruebas;
                                    break;
                                case "corto":
                                    docsPruebasTemp[4].pruebas=docsPruebas[i].pruebas;
                                    break;
                                case "matematicas":
                                    docsPruebasTemp[5].pruebas=docsPruebas[i].pruebas;
                                    break;    
                                case "medio":
                                    docsPruebasTemp[6].pruebas=docsPruebas[i].pruebas;
                                    break;                                                                  
                                default:
                                    console.log("ERROR HAY UN NUEVO tipo");
                            }

                            
                        }
                       
                   
                    


                        usuarios.insertOne({ _id:_id, tipoLogin:2,name: user.name, surname: user.surname, email: user.email, password: user.password, birthdate: new Date(user.birthdate), gender: user.gender,picture:null, tipo:0,state:true, pruebasNR:docsPruebasTemp,pruebasR:docspruebasR}, function(error, result){
                            "use strict";
                            if(error){
                                return callback(2, "");
                            }
                            return callback(1, _id);
                        });

        
                });   

 

                
            }






        });
        

        
    }

    
    this.getDataInicio = function(idusuario, callback){
        "use strict";


        usuarios.findOne({ _id:ObjectID(idusuario) }, function(err, document) {
            "use strict";

                if (err) return callback(0,{});
                if(document)return callback(1,document);
                else return callback(0,{});

            });

        }


        this.getUserById = function(idusuario, callback){
            "use strict";
    
    
            usuarios.findOne({ _id:ObjectID(idusuario) }, function(err, docUsu) {
                "use strict";

                if (err) return callback(0,{})


                return callback(1,docUsu)
            
                
            });

        }


    this.postEnviarSugerencia = function(idusuario,videoUploaded, callback){
        "use strict";


        usuarios.findOne({ _id:ObjectID(idusuario) }, function(err, document) {
            "use strict";

                if (err) return callback(0);
                if(document){


                    usuarios.updateOne( { tipo:1 } , {$push:{ "sugerencias.porSubir":{fecha:new Date(),videoUploaded:videoUploaded,idusuario:idusuario,descripcion:"id de usuario: "+idusuario,titulo:document.name+"-"+new Date().toLocaleDateString("en",{ weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' }) } }  }, function (err, respu) {
                        "use strict";
                         if (err) return callback(0); 

                            return callback(1);


                        });



                } else return callback(0);


        });

    }

    this.postLogin = function(userLogin, callback){
        "use strict";

        //facebook o google --> 1
        // correo y pass --> 2

        usuarios.findOne({email:userLogin.email,tipoLogin:userLogin.flag}, function(err, document) {
            "use strict";

                if (err) return callback(2,{});
                if(document){
                           
                if( document.password==userLogin.password || userLogin.flag==1  ){
                    var token = String(new ObjectID());

                    usuarios.updateOne( {email:userLogin.email,tipoLogin:userLogin.flag} , {$set:{token:token}  }, function (err, respu) {
                    "use strict";
                     if (err) return callback(2,{}); 

                        document["token"]=token;
                        return callback(1,document);

                    });

                    

                }else return callback(0,{});
                }else {


                        usuarios.findOne({email:userLogin.email}, function(err, document){
                        "use strict";                    
                        if (err) return callback(0,{});

                                    if(document && userLogin.flag==1 ) return callback(1,document );

                                    if(document && userLogin.flag==2 ) return callback(3,document );

                                    if ( userLogin.flag !=1 ) return callback(0,{});                                 

                                    var token = String(new ObjectID());
  
                                    var _id = new ObjectID();
                    
                                    pruebas.aggregate([{ $match : { state : true } },{ $group : {  _id :"$subType" , pruebas:{$push: "$_id" } } } ]).toArray(function(err,docsPruebas){
                                        "use strict";
                                        
                                        if (err) return callback(2,{});
                    
                                       
                                           
                                        var docsPruebasTemp=[{_id:"naturales",pruebas:[]},{_id:"largo",pruebas:[]},
                                        {_id:"sociales",pruebas:[]}, {_id:"lectura",pruebas:[]}, {_id:"corto",pruebas:[]},
                                        {_id:"matematicas",pruebas:[] }, {_id:"medio",pruebas:[] }  ]; //si agregan un tipo, agregarlo también en  '/test/:subType/:idusuario', admin.getTestByUser, en postAnswer y en login
                    
                                            
                                        var docspruebasR=[{_id:"naturales",pruebas:[]},{_id:"largo",pruebas:[]},
                                        {_id:"sociales",pruebas:[]}, {_id:"lectura",pruebas:[]}, {_id:"corto",pruebas:[]},
                                        {_id:"matematicas",pruebas:[]} , {_id:"medio",pruebas:[] }   ];
                    
                                            for (let i = 0; i < docsPruebas.length; i++) {
                                                
                                                switch(docsPruebas[i]._id ) {
                                                    case "naturales":
                                                        docsPruebasTemp[0].pruebas=docsPruebas[i].pruebas;
                                                        break;
                                                    case "largo":
                                                        docsPruebasTemp[1].pruebas=docsPruebas[i].pruebas;
                                                        break;
                                                    case "sociales":
                                                        docsPruebasTemp[2].pruebas=docsPruebas[i].pruebas;
                                                        break;    
                                                    case "lectura":
                                                        docsPruebasTemp[3].pruebas=docsPruebas[i].pruebas;
                                                        break;
                                                    case "corto":
                                                        docsPruebasTemp[4].pruebas=docsPruebas[i].pruebas;
                                                        break;
                                                    case "matematicas":
                                                        docsPruebasTemp[5].pruebas=docsPruebas[i].pruebas;
                                                        break;    
                                                    case "medio":
                                                        docsPruebasTemp[6].pruebas=docsPruebas[i].pruebas;
                                                        break;                                                                  
                                                    default:
                                                        console.log("ERROR HAY UN NUEVO tipo");
                                                }
                    
                                                
                                            }
                                           
                                        
                                        
                    
                    
                                            usuarios.insertOne({ _id:_id, token:token ,name: userLogin.name, email: userLogin.email,tipoLogin:userLogin.flag, tipo:0, picture:null,state:true, pruebasNR:docsPruebasTemp,pruebasR:docspruebasR}, function(error, result){
                                                "use strict";
                                                if(error){
                                                    return callback(2,{});
                                                }
                                                return callback(1, { _id:_id, token:token ,name: userLogin.name, email: userLogin.email,tipoLogin:userLogin.flag, tipo:0,picture:null, state:true, pruebasNR:docsPruebasTemp,pruebasR:docspruebasR}  );
                                            });
                    
                            
                                    });                                      


                                   

                        });


                    }

            });  



        /*usuarios.findOneAndUpdate({email:userLogin.email}, {$set:{token:generate_token(parseInt((Math.random() * (50)).toFixed(0)))+(new ObjectID())}},{returnOriginal:false}, function(error, docUsu){
            
            "use strict";
            if(error){
                return callback(2, {});

            }

            docUsu=docUsu.value;
            console.log(docUsu)

            


            if (docUsu) {


                if (docUsu.password==userLogin.password) {

                    return callback(1,docUsu);
                    
                } else {

                    return callback(0,{});
                    
                }



                

                
            } 
            else
            {
                return callback(0,{});
                
            }



        });*/
        

    }


    this.postRecover = function(email, callback){
        "use strict";

        var tokenPass= ObjectID()+ObjectID()+ObjectID(),
            tokenPass=tokenPass.shuffle();
        usuarios.findOneAndUpdate(  {email:email },  { $set:{tokenPass:tokenPass}  },{projection:{name:1}}, function(err, result){
            "use strict";
            
            if (err) return callback(false);

            var userDoc=result.value;

            console.log(userDoc);

            if (userDoc) return callback(true,userDoc.name,tokenPass);
            else  return callback(false,"","");

        });


    }


    this.postAnswer = function(idusuario,idprueba,idpregunta,resp,ultima,intento,session,area, tiempo, callback){
        "use strict";

        pruebas.findOne({_id:ObjectID(idprueba)}, function(err, docPrueba){
            "use strict";
            
            if (err) return callback(false,{});


                
            usuarios.findOne({_id:ObjectID(idusuario)}, function(err, docUsuario){
                "use strict";
                
                if (err) return callback(false,{});


                var indexSubType;
                var multiple;

                switch( docPrueba.subType ) {
                    case "naturales":                        
                        indexSubType=0;
                        break;
                    case "largo":                        
                        indexSubType=1;           
                        break;
                    case "sociales":                        
                        indexSubType=2;                           
                        break;    
                    case "lectura":                        
                        indexSubType=3;         
                        break;
                    case "corto":                        
                        indexSubType=4;                              
                        break;
                    case "matematicas":                        
                        indexSubType=5;
                        break;
                    case "medio":                        
                        indexSubType=6;                                                  
                        break;                                
                    default:
                        console.log("ERROR HAY UN NUEVO tipo");
                }

                var puntaje=0;

                for (let i = 0; i < docUsuario.pruebasR[indexSubType].pruebas.length; i++) {

                    const pruebaEnUsu = docUsuario.pruebasR[indexSubType].pruebas[i];

                    if (pruebaEnUsu._id==idprueba) {

                        var indexPrueba=i;

                        var pruebaIntento= pruebaEnUsu.intentos[intento];

                        

                            var stringSessionArea ="session"+session+"Area"+area;

                            var sessionAreaPruebas=pruebaIntento[stringSessionArea];

                            for (let j = 0; j < sessionAreaPruebas.length; j++) {

                                const pregunta = sessionAreaPruebas[j];

                                if ( String(pregunta._id)==idpregunta  ) {

                                    var indexPregunta=j;

                                    

                                    if (pregunta.correctAnswer==resp) puntaje=100/sessionAreaPruebas.length

                                    break;
                                    
                                }

                                
                                
                            }

                        break;
                    }
                    
                }


                var updateMongo={};

                var updateMongoInc={};
                
                var pruebaUpdated=docUsuario["pruebasR"][indexSubType]["pruebas"][indexPrueba]["intentos"][intento];

                pruebaUpdated[stringSessionArea][indexPregunta]["answerUser"]=resp;
                pruebaUpdated[stringSessionArea][indexPregunta]["time"]=tiempo;

                if (pruebaUpdated[stringSessionArea+"Score"] ==undefined) pruebaUpdated[stringSessionArea+"Score"]=0;

                if (pruebaUpdated["testScore"] ==undefined) pruebaUpdated["testScore"]=0;

                pruebaUpdated[stringSessionArea+"Score"]=pruebaUpdated[stringSessionArea+"Score"]+puntaje;
                pruebaUpdated["testScore"]=pruebaUpdated["testScore"]+puntaje;


                updateMongo["pruebasR."+indexSubType+".pruebas."+indexPrueba+".intentos."+intento+"."+stringSessionArea+"."+indexPregunta+".answerUser"]=resp;
                updateMongo["pruebasR."+indexSubType+".pruebas."+indexPrueba+".intentos."+intento+"."+stringSessionArea+"."+indexPregunta+".time"]=tiempo;

                updateMongoInc["pruebasR."+indexSubType+".pruebas."+indexPrueba+".intentos."+intento+"."+stringSessionArea+"Score"]=puntaje;
                updateMongoInc["pruebasR."+indexSubType+".pruebas."+indexPrueba+".intentos."+intento+".testScore"]=puntaje;

                var updateMongoEstaInc={},
                    updateMongoEstaInc2={idpregunta:ObjectID(idpregunta)};  

                if (resp==undefined) 
                {
                    updateMongoEstaInc["preguntas.$.noRespondidas"]=1;
                    updateMongoEstaInc2["noRespondidas"]=1;

                    if (pruebaUpdated["noRespondidas"] ==undefined) pruebaUpdated["noRespondidas"]=0;
                    pruebaUpdated["noRespondidas"]=pruebaUpdated["noRespondidas"]+1;

                    updateMongoInc["pruebasR."+indexSubType+".pruebas."+indexPrueba+".intentos."+intento+".noRespondidas"]=1;                
                    

                }
                if(puntaje==0 && resp!=undefined){
                    updateMongoEstaInc["preguntas.$.incorrectas"]=1;
                    updateMongoEstaInc2["incorrectas"]=1;
                                        
                    if (pruebaUpdated["incorrectas"] ==undefined) pruebaUpdated["incorrectas"]=0;
                    pruebaUpdated["incorrectas"]=pruebaUpdated["incorrectas"]+1;

                    updateMongoInc["pruebasR."+indexSubType+".pruebas."+indexPrueba+".intentos."+intento+".incorrectas"]=1;
                    

                } 
                if (puntaje>0){
                    updateMongoEstaInc["preguntas.$.correctas"]=1;
                    updateMongoEstaInc2["correctas"]=1;
                    

                    if (pruebaUpdated["correctas"] ==undefined) pruebaUpdated["correctas"]=0;
                    pruebaUpdated["correctas"]=pruebaUpdated["correctas"]+1;

                    updateMongoInc["pruebasR."+indexSubType+".pruebas."+indexPrueba+".intentos."+intento+".correctas"]=1;

                } 



                switch (stringSessionArea) {
                    case "session1Area1":

                    if (docPrueba.session1Area1.length-1==indexPregunta) {

                        var estUpdate={};
                        if (indexSubType==0 || indexSubType==3 ||  indexSubType==2 || indexSubType==5 ){}
                        else {
                        
                                    estUpdate[docPrueba.sess1SubType[0]]=pruebaUpdated[stringSessionArea+"Score"];

                                    estadisticas.updateOne({ idprueba:ObjectID(idprueba)  } ,  { $set:{idprueba: ObjectID(idprueba) },$push:estUpdate   },{upsert:true}, function(error, result){            
                                        "use strict";
                                
                                        if (error) return callback(false,{});
                                                                
                                    });

                            }

                        
                    }
                        
                        break;

                    case "session2Area1":

                    
                    if (docPrueba.session2Area1.length-1==indexPregunta) {

                        var estUpdate={};
                        if (indexSubType==0 || indexSubType==3 ||  indexSubType==2 || indexSubType==5 ) estUpdate[docPrueba.subType]=pruebaUpdated[stringSessionArea+"Score"];
                        else estUpdate[docPrueba.sess2SubType[0]]=pruebaUpdated[stringSessionArea+"Score"];

                        estadisticas.updateOne({ idprueba:ObjectID(idprueba)  } ,  { $set:{idprueba: ObjectID(idprueba) },$push:estUpdate   },{upsert:true}, function(error, result){            
                            "use strict";
                    
                            if (error) return callback(false,{});                            
    
                        });

                        
                    }
                    
                    break;
                }
                
                if (ultima) {

                    var noRespondidasTotal=0;

                    for (let i = 0; i < docUsuario.pruebasR[indexSubType].pruebas.length; i++) {

                        const pruebaEnUsu = docUsuario.pruebasR[indexSubType].pruebas[i];
    
                        if (pruebaEnUsu._id==idprueba) {
    
                            var indexPrueba=i;
    
                            var pruebaIntento= pruebaEnUsu.intentos[intento];


                                   if (indexSubType==1 || indexSubType==4 ||  indexSubType==6 ){

                                            
                                        var stringSessionArea ="session"+session+"Area1";

                                        var sessionAreaPruebas=pruebaIntento[stringSessionArea];

                                        for (let j = 0; j < sessionAreaPruebas.length; j++) {

                                            const pregunta = sessionAreaPruebas[j];

                                            if (typeof pregunta.answerUser=="undefined") noRespondidasTotal=noRespondidasTotal+1;

                                            
                                            
                                        }

                                    }


                                    
    
                            
    
                                var stringSessionArea ="session"+session+"Area"+area;
    
                                var sessionAreaPruebas=pruebaIntento[stringSessionArea];
    
                                for (let j = 0; j < sessionAreaPruebas.length; j++) {
    
                                    const pregunta = sessionAreaPruebas[j];
    
                                   if (typeof pregunta.answerUser=="undefined") noRespondidasTotal=noRespondidasTotal+1;
    
                                    
                                    
                                }
    
                            break;
                        }
                        
                    }

                    if (noRespondidasTotal>0) {

                        console.log(noRespondidasTotal)
                            
                        if (pruebaUpdated["noRespondidas"] ==undefined) pruebaUpdated["noRespondidas"]=0;
                        pruebaUpdated["noRespondidas"]=pruebaUpdated["noRespondidas"]+noRespondidasTotal;
                    
                        updateMongoInc["pruebasR."+indexSubType+".pruebas."+indexPrueba+".intentos."+intento+".noRespondidas"]=noRespondidasTotal;                
                        updateMongoEstaInc["preguntas.$.noRespondidas"]=noRespondidasTotal;
                        updateMongoEstaInc2["noRespondidas"]=noRespondidasTotal;
                    
                        
                    }

                    
                    updateMongo["pruebasR."+indexSubType+".pruebas."+indexPrueba+".intentos."+intento+"."+"session"+session+"End"]=1;
                    
                    pruebaUpdated["session"+session+"End"]=1;

                    var estUpdate={};

                    if (indexSubType==0 || indexSubType==3 ||   indexSubType==2 || indexSubType==5 ) estUpdate[docPrueba.subType]=pruebaUpdated[stringSessionArea+"Score"];
                    else {

                        switch (stringSessionArea) {

                            case "session1Area2":

                            estUpdate[docPrueba.sess1SubType[1]]=pruebaUpdated[stringSessionArea+"Score"];
                            
                            break;

                            case "session2Area2":

                            estUpdate[docPrueba.sess2SubType[1]]=pruebaUpdated[stringSessionArea+"Score"];
                            
                            break;                    
                        }

                        
                    }

                    

                    if (session==2 || (session==1 && (indexSubType==0 || indexSubType==3 ||  indexSubType==2 || indexSubType==5 ) )  ) {

                        estUpdate["total"]=pruebaUpdated["testScore"];
                        
                    }

                    

                    estadisticas.updateOne({ idprueba:ObjectID(idprueba)  } ,  { $set:{idprueba: ObjectID(idprueba) },$push:estUpdate   },{upsert:true}, function(error, result){            
                        "use strict";
                
                        if (error) return callback(false,{});

                    });

                    
                }



                estadisticas.updateOne({ idprueba:ObjectID(idprueba)  } ,  { $set:{idprueba: ObjectID(idprueba) }  },{upsert:true}, function(error, result){            
                    "use strict";
            
                    if (error) return callback(false,{});

                        
                    estadisticas.findOneAndUpdate({  idprueba: ObjectID(idprueba), "preguntas.idpregunta":ObjectID(idpregunta)     },  { $inc: updateMongoEstaInc  }, function(error, resultEst){
                            "use strict";
        
                            if(error) return callback(false,{});
                            
                         
                            if (!resultEst.value) {

                                estadisticas.updateOne({ idprueba:ObjectID(idprueba)  } ,  {  $push:{ preguntas: updateMongoEstaInc2   }} ,  function(error, result){            
                                    "use strict";
                            
                                    if (error) return callback(false,{}  );

                                
                                });

                                
                            } 

                            
                                       



                        });
                }); 

                
                usuarios.updateOne({_id:ObjectID(idusuario) } ,  { $set: updateMongo,$inc:updateMongoInc  }, function(error, result){            
                    "use strict";
            
                    if (error) return callback(false,{});

                    
                    

                    return callback(true,pruebaUpdated);


                });

            });
        });


    }

    this.postAddQuestion = function(question, callback){
        "use strict";

       
        preguntas.insertOne(question, function(error, result){
            "use strict";
            if(error){
                return callback(true);
            }
            return callback(false);
        });
    } 


    this.postIniciarPrueba = function(idprueba,idusuario , callback){
        "use strict";

       // usuarios.findOneAndUpdate({_id:ObjectID(idusuario), "pruebasNR._id":subType  } ,  { $pop:{"pruebasNR.$.pruebas":-1 }}, function(error, result){            
           // "use strict";

                //pruebas.findOne({_id: ObjectID(idprueba) },{projection:{'session1Area1.correctAnswer':0,'session1Area2.correctAnswer':0,'session2Area1.correctAnswer':0,'session2Area2.correctAnswer':0}   }, function(err, docPrueba){
                  //  "use strict";

                  
                pruebas.findOne({_id: ObjectID(idprueba) }, function(err, docPrueba){
                   "use strict";
                    
                    if (err) return callback(false,{});

                     usuarios.updateOne({_id:ObjectID(idusuario), "pruebasNR._id":docPrueba.subType  } ,  { $pull:{"pruebasNR.$.pruebas":ObjectID(idprueba) }}, function(error, result){            
                        "use strict";

                    if (result.modifiedCount==1) {

                        usuarios.findOneAndUpdate({_id:ObjectID(idusuario), "pruebasR._id":docPrueba.subType  },  { $push:{"pruebasR.$.pruebas":{ _id:idprueba, intentos:[docPrueba] ,state:true    }}  },{returnOriginal:false,projection:{ _id:0,pruebasR:1,pruebasNR:1} }, function(error, result){
                            "use strict";
        
                            if(error){
                                return callback(false,{});
                            }
                         
    
                            var docUsuarioAc=result.value;
    
    
                        
        
                        
                            return callback(true,docUsuarioAc);
        
    
                        });
                        
                    }
                    else{

                        usuarios.findOne({_id: ObjectID(idusuario) }, function(err, docUsuario){
                            "use strict";
                             
                             if (err) return callback(false,{});

                             return callback(true,docUsuario);


                        });


                    }



                    });
    
                });

            
                
        






    }  
    
    this.postIniciarPruebaIntento = function(idprueba,idusuario , callback){
        "use strict";

       // usuarios.findOneAndUpdate({_id:ObjectID(idusuario), "pruebasNR._id":subType  } ,  { $pop:{"pruebasNR.$.pruebas":-1 }}, function(error, result){            
           // "use strict";

                //pruebas.findOne({_id: ObjectID(idprueba) },{projection:{'session1Area1.correctAnswer':0,'session1Area2.correctAnswer':0,'session2Area1.correctAnswer':0,'session2Area2.correctAnswer':0}   }, function(err, docPrueba){
                  //  "use strict";

                  
                pruebas.findOne({_id: ObjectID(idprueba) }, function(err, docPrueba){
                   "use strict";
                    
                    if (err) return callback(false,{});


                    var indexType;

                    switch( docPrueba.subType ) {
                        case "naturales":
                            indexType=0;
                            break;
                        case "largo":
                            indexType=1;
                            break;
                        case "sociales":
                            indexType=2;   
                            break;    
                        case "lectura":
                            indexType=3; 
                            break;
                        case "corto":
                            indexType=4;
                            break;
                        case "matematicas":
                            indexType=5;
                            break;        
                        case "medio":
                            indexType=6;
                            break;                                               
                        default:
                            console.log("ERROR HAY UN NUEVO tipo");
                    }

                    var filterMongo={};
                    var updateMongo={};
                    
                    filterMongo["_id"]=ObjectID(idusuario);
                    filterMongo["pruebasR."+indexType+".pruebas._id"]=idprueba;

                    updateMongo["pruebasR."+indexType+".pruebas.$.intentos"]=docPrueba;


                    usuarios.findOneAndUpdate( filterMongo,  { $push: updateMongo  },{returnOriginal:false,projection:{ _id:0,pruebasR:1,pruebasNR:1} }, function(error, result){
                        "use strict";
    
                        if(error){
                            return callback(false,{});
                        }
                     
                    
    
                        var docUsuarioAc=result.value;


                    
    
                    
                        return callback(true,docUsuarioAc);

                   

                    });
    
                });

    }     

    this.postAddTest = function(title,description,type,subType,session1Area1,session1Area2,session2Area1,session2Area2,sess1SubType,sess2SubType, callback){
        "use strict";

        



        async.forEachOf(session1Area1, function (value, key, callbackAs) {

            console.log("value es " + value);
                    
            preguntas.findOne( {_id:ObjectID(value)}, function(err, docPregunta) {
                "use strict";       

                if (err) return callbackAs(true);  
      
                session1Area1[key]=docPregunta;

                        return callbackAs();

            });    

        }, function (err) {

            if (err) return callback(false);


            if (session1Area2==null) {


                pruebas.insertOne({title:title,description:description,type:type,session1Area1Score:0,testScore:0,session1End:0,subType:subType,session1Area1:session1Area1,state:true}, function(error, result){
                    "use strict";
                    if(error){
                        return callback(false);
                    }

                    

                    usuarios.updateMany({"pruebasNR._id":subType,tipo:0},  { $push:{"pruebasNR.$.pruebas":ObjectID(result.insertedId) } }, function(error, result){
                        "use strict";
                        if(error){
                            return callback(false);
                        }

                    });


                    return callback(true);
                });

                
            }
            else{




                async.forEachOf(session1Area2, function (value, key, callbackAs2) {

                    console.log("value es " + value);
                            
                    preguntas.findOne( {_id:ObjectID(value)}, function(err, docPregunta) {
                        "use strict";       
        
                        if (err) return callbackAs2(true);  
              
                        session1Area2[key]=docPregunta;
        
                                return callbackAs2();
        
                    });    
        
                }, function (err) {
        
                    if (err) return callback(false);
         
        
                    async.forEachOf(session2Area1, function (value, key, callbackAs3) {

                        console.log("value es " + value);
                                
                        preguntas.findOne( {_id:ObjectID(value)}, function(err, docPregunta) {
                            "use strict";       
            
                            if (err) return callbackAs3(true);  
                  
                            session2Area1[key]=docPregunta;
            
                                    return callbackAs3();
            
                        });    
            
                    }, function (err) {
            
                        if (err) return callback(false);
             
            
    
                        async.forEachOf(session2Area2, function (value, key, callbackAs4) {

                            console.log("value es " + value);
                                    
                            preguntas.findOne( {_id:ObjectID(value)}, function(err, docPregunta) {
                                "use strict";       
                
                                if (err) return callbackAs4(true);  
                      
                                session2Area2[key]=docPregunta;
                
                                        return callbackAs4();
                
                            });    
                
                        }, function (err) {
                
                            if (err) return callback(false);
                 
                            pruebas.insertOne({title:title,description:description,type:type,subType:subType,session1Area1Score:0,testScore:0,session1Area2Score:0,session2Area1Score:0,session2Area2Score:0,session1End:0,session2End:0,session1Area1:session1Area1,session1Area2:session1Area2,session2Area1:session2Area1,session2Area2:session2Area2,sess1SubType:sess1SubType,sess2SubType:sess2SubType,state:true}, function(error, result){
                                "use strict";
                                if(error){
                                    return callback(false);
                                }
            
                                
            
                                usuarios.updateMany({"pruebasNR._id":subType,tipo:0},  { $push:{"pruebasNR.$.pruebas":ObjectID(result.insertedId) } }, function(error, result){
                                    "use strict";
                                    if(error){
                                        return callback(false);
                                    }
            
                                });
            
            
                                return callback(true);
                            });
        
        
        
        
                            
                         
        
                                });   
    
    
                        
                     
    
                            });    




                 

                        });                 









            }


                    
            



                });  

       

        
    } 




    
    
    //put

    this.putUpdateQuestion = function(question, callback){
        "use strict";

       
        preguntas.updateOne({_id:ObjectID(question._id)},  { $set:{link:question.link,text:question.text,image:question.image,subject:question.subject,subSubject:question.subSubject,answerA:question.answerA,answerB:question.answerB,answerC:question.answerC,answerD:question.answerD,correctAnswer:question.correctAnswer, linkAnswerA:question.linkAnswerA,  linkAnswerB:question.linkAnswerB , linkAnswerC:question.linkAnswerC, linkAnswerD:question.linkAnswerD, imageAnswerA:question.imageAnswerA, imageAnswerB:question.imageAnswerB, imageAnswerC:question.imageAnswerC, imageAnswerD:question.imageAnswerD  }}, function(error, result){
            "use strict";
            if(error){
                return callback(false);
            }

            pruebas.updateMany({   "session1Area1._id":ObjectID(question._id)  },  { $set:{"session1Area1.$.link":question.link,"session1Area1.$.text":question.text,"session1Area1.$.image":question.image,"session1Area1.$.subject":question.subject,"session1Area1.$.subSubject":question.subSubject,"session1Area1.$.answerA":question.answerA,"session1Area1.$.answerB":question.answerB,"session1Area1.$.answerC":question.answerC,"session1Area1.$.answerD":question.answerD,"session1Area1.$.correctAnswer":question.correctAnswer, "session1Area1.$.linkAnswerA":question.linkAnswerA,  "session1Area1.$.linkAnswerB":question.linkAnswerB , "session1Area1.$.linkAnswerC":question.linkAnswerC, "session1Area1.$.linkAnswerD":question.linkAnswerD, "session1Area1.$.imageAnswerA":question.imageAnswerA, "session1Area1.$.imageAnswerB":question.imageAnswerB, "session1Area1.$.imageAnswerC":question.imageAnswerC, "session1Area1.$.imageAnswerD":question.imageAnswerD  }}, function(error, result){
                "use strict";
                if(error){
                    return callback(false);
                }

                pruebas.updateMany({   "session1Area2._id":ObjectID(question._id)  },  { $set:{"session1Area2.$.link":question.link,"session1Area2.$.text":question.text,"session1Area2.$.image":question.image,"session1Area2.$.subject":question.subject,"session1Area2.$.subSubject":question.subSubject,"session1Area2.$.answerA":question.answerA,"session1Area2.$.answerB":question.answerB,"session1Area2.$.answerC":question.answerC,"session1Area2.$.answerD":question.answerD,"session1Area2.$.correctAnswer":question.correctAnswer, "session1Area2.$.linkAnswerA":question.linkAnswerA,  "session1Area2.$.linkAnswerB":question.linkAnswerB , "session1Area2.$.linkAnswerC":question.linkAnswerC, "session1Area2.$.linkAnswerD":question.linkAnswerD, "session1Area2.$.imageAnswerA":question.imageAnswerA, "session1Area2.$.imageAnswerB":question.imageAnswerB, "session1Area2.$.imageAnswerC":question.imageAnswerC, "session1Area2.$.imageAnswerD":question.imageAnswerD  }}, function(error, result){
                    "use strict";
                    if(error){
                        return callback(false);
                    }

                    pruebas.updateMany({   "session2Area1._id":ObjectID(question._id)  },  { $set:{"session2Area1.$.link":question.link,"session2Area1.$.text":question.text,"session2Area1.$.image":question.image,"session2Area1.$.subject":question.subject,"session2Area1.$.subSubject":question.subSubject,"session2Area1.$.answerA":question.answerA,"session2Area1.$.answerB":question.answerB,"session2Area1.$.answerC":question.answerC,"session2Area1.$.answerD":question.answerD,"session2Area1.$.correctAnswer":question.correctAnswer, "session2Area1.$.linkAnswerA":question.linkAnswerA,  "session2Area1.$.linkAnswerB":question.linkAnswerB , "session2Area1.$.linkAnswerC":question.linkAnswerC, "session2Area1.$.linkAnswerD":question.linkAnswerD, "session2Area1.$.imageAnswerA":question.imageAnswerA, "session2Area1.$.imageAnswerB":question.imageAnswerB, "session2Area1.$.imageAnswerC":question.imageAnswerC, "session2Area1.$.imageAnswerD":question.imageAnswerD  }}, function(error, result){
                        "use strict";
                        if(error){
                            return callback(false);
                        }

                        pruebas.updateMany({   "session2Area2._id":ObjectID(question._id)  },  { $set:{"session2Area2.$.link":question.link,"session2Area2.$.text":question.text,"session2Area2.$.image":question.image,"session2Area2.$.subject":question.subject,"session2Area2.$.subSubject":question.subSubject,"session2Area2.$.answerA":question.answerA,"session2Area2.$.answerB":question.answerB,"session2Area2.$.answerC":question.answerC,"session2Area2.$.answerD":question.answerD,"session2Area2.$.correctAnswer":question.correctAnswer, "session2Area2.$.linkAnswerA":question.linkAnswerA,  "session2Area2.$.linkAnswerB":question.linkAnswerB , "session2Area2.$.linkAnswerC":question.linkAnswerC, "session2Area2.$.linkAnswerD":question.linkAnswerD, "session2Area2.$.imageAnswerA":question.imageAnswerA, "session2Area2.$.imageAnswerB":question.imageAnswerB, "session2Area2.$.imageAnswerC":question.imageAnswerC, "session2Area2.$.imageAnswerD":question.imageAnswerD  }}, function(error, result){
                            "use strict";
                            if(error){
                                return callback(false);
                            }







                                         return callback(true);
                            });
                        });
                 });
             });
        });
    }   
      
    this.putUpdateUser = function(user, callback){
        "use strict";

        if (user.password==null) {

            var updateJson={ $set:{name:user.name,surname:user.surname,email:user.email,birthdate:user.birthdate,gender:user.gender,picture:user.picture}};
            
        } else {

            var updateJson={ $set:{name:user.name,surname:user.surname,email:user.email,birthdate:user.birthdate,gender:user.gender,picture:user.picture,password:user.password}};
            
        }

       
        usuarios.updateOne({_id:ObjectID(user._id)}, updateJson , function(error, result){
            "use strict";
            if(error){
                return callback(false);
            }
            return callback(true);
        });


    } 

    this.putUpdateTest = function(title,description,type,subType,session1Area1,session1Area2,session2Area1,session2Area2,sess1SubType,sess2SubType,idprueba, callback){
        "use strict";

     



        async.forEachOf(session1Area1, function (value, key, callbackAs) {

            console.log("value es " + value);
                    
            preguntas.findOne( {_id:ObjectID(value)}, function(err, docPregunta) {
                "use strict";       

                if (err) return callbackAs(true);  
      
                session1Area1[key]=docPregunta;

                        return callbackAs();

            });    

        }, function (err) {

            if (err) return callback(false);


            if (session1Area2==null) {


                pruebas.updateOne({_id:ObjectID(idprueba)},{ $set:{title:title,description:description,type:type,session1End:0,subType:subType,session1Area1:session1Area1,state:true}}, function(error, result){
                    "use strict";
                    if(error){
                        return callback(false);
                    }
                

                    return callback(true);
                });

                
            }
            else{




                async.forEachOf(session1Area2, function (value, key, callbackAs2) {

                    console.log("value es " + value);
                            
                    preguntas.findOne( {_id:ObjectID(value)}, function(err, docPregunta) {
                        "use strict";       
        
                        if (err) return callbackAs2(true);  
              
                        session1Area2[key]=docPregunta;
        
                                return callbackAs2();
        
                    });    
        
                }, function (err) {
        
                    if (err) return callback(false);
         
        
                    async.forEachOf(session2Area1, function (value, key, callbackAs3) {

                        console.log("value es " + value);
                                
                        preguntas.findOne( {_id:ObjectID(value)}, function(err, docPregunta) {
                            "use strict";       
            
                            if (err) return callbackAs3(true);  
                  
                            session2Area1[key]=docPregunta;
            
                                    return callbackAs3();
            
                        });    
            
                    }, function (err) {
            
                        if (err) return callback(false);
             
            
    
                        async.forEachOf(session2Area2, function (value, key, callbackAs4) {

                            console.log("value es " + value);
                                    
                            preguntas.findOne( {_id:ObjectID(value)}, function(err, docPregunta) {
                                "use strict";       
                
                                if (err) return callbackAs4(true);  
                      
                                session2Area2[key]=docPregunta;
                
                                        return callbackAs4();
                
                            });    
                
                        }, function (err) {
                
                            if (err) return callback(false);
                 
                            pruebas.updateOne(  {_id:ObjectID(idprueba) },{ $set:{title:title,description:description,type:type,subType:subType,session1End:0,session2End:0,session1Area1:session1Area1,session1Area2:session1Area2,session2Area1:session2Area1,session2Area2:session2Area2,sess1SubType:sess1SubType,sess2SubType:sess2SubType,state:true}}, function(error, result){
                                "use strict";
                                if(error){
                                    return callback(false);
                                }
            
                            
            
                                return callback(true);
                            });
        
        
        
        
                            
                         
        
                                });   
    
    
                        
                     
    
                            });    




                 

                        });                 









            }


                    
            



                });  


       

        
    } 

    this.putDeactivateUsers = function(ids, callback){
        "use strict";



        async.forEachOf(ids, function (value, key, callbackAs) {

            console.log("id es " + value);
                    
            usuarios.updateOne({_id:ObjectID(value)},  { $set:{state:false}}, function(error, result){
                "use strict";
                if(error){
                    return callbackAs(true);
                }
                return callbackAs();
            });


        }, function (err) {

            if (err) return callback(false);

               
                    return callback(true);                                         


                });  

       

        
    } 

    this.updateAdminYoutube = function(youtubeUrl,docSugerencia, callback){
        "use strict";
       
            usuarios.updateOne({ tipo:1 },  { $pull:{  "sugerencias.porSubir":{ videoUploaded:docSugerencia.videoUploaded  }   } ,$push:{"sugerencias.subidos":{ fecha:docSugerencia.fecha,videoUploaded:youtubeUrl, idusuario:docSugerencia.idusuario, descripcion:docSugerencia.descripcion, titulo:docSugerencia.titulo   }    }  }, function(error, result){
                "use strict";
                if(error){
                    return callback(false);
                }

                fs.stat('./public'+docSugerencia.videoUploaded, function(err, stat) {
                    if(err == null) {
                        fs.unlink('./public'+docSugerencia.videoUploaded,(err) => {
                            if (err) throw err;
                            
                        });
                    }
                    else{
                        console.log("error en borrar video");
                        console.log(err);
                    } 
                });



                return callback(true);


            });
    } 

    this.putActivateUsers = function(ids, callback){
        "use strict";



        async.forEachOf(ids, function (value, key, callbackAs) {

            console.log("id es " + value);
                    
            usuarios.updateOne({_id:ObjectID(value)},  { $set:{state:true}}, function(error, result){
                "use strict";
                if(error){
                    return callbackAs(true);
                }
                return callbackAs();
            });


        }, function (err) {

            if (err) return callback(false);

               
                    return callback(true);                                         


                });  

       

        
    } 


    this.putDeactivateTests = function(ids, callback){
        "use strict";



        async.forEachOf(ids, function (value, key, callbackAs) {

            console.log("id es " + value);
                    
            pruebas.findOneAndUpdate({_id:ObjectID(value)},  { $set:{state:false}}, function(error, resultTest){
                "use strict";
                if(error){
                    return callbackAs(true);
                }

                
                usuarios.updateMany({"pruebasNR._id":resultTest.value.subType,tipo:0},  { $pull:{"pruebasNR.$.pruebas":ObjectID(value) } }, function(error, result){
                    "use strict";
                    if(error){
                        return callbackAs(true);
                    }

                    let indexSubType;

                    switch( resultTest.value.subType ) {
                        case "naturales":                             
                            indexSubType=0;                
                            break;
                        case "largo":
                            indexSubType=1;     
                            break;
                        case "sociales":
                            indexSubType=2;       
                            break;    
                        case "lectura":
                            indexSubType=3;  
                            break;
                        case "corto":
                            indexSubType=4;  
                            break;
                        case "matematicas":
                            indexSubType=5; 
                            break;        
                        case "medio":
                            indexSubType=6; 
                            break;                                               
                        default:
                            console.log("ERROR HAY UN NUEVO tipo");
                    }

                    let updateManyMongo={},
                        setManyMongo={};

                        updateManyMongo["pruebasR."+indexSubType+".pruebas._id"]=value;
                        setManyMongo["pruebasR."+indexSubType+".pruebas.$.state"]=false;

                    usuarios.updateMany( updateManyMongo ,  { $set:setManyMongo }, function(error, result){
                        "use strict";
                        if(error){
                            return callbackAs(true);
                        }
    
    
                    });


                    return callbackAs();

                });


               
            });


        }, function (err) {

            if (err) return callback(false);

               
                    return callback(true);                                         


                });  

       

        
    }

    this.putActivateTests = function(ids, callback){
        "use strict";



        async.forEachOf(ids, function (value, key, callbackAs) {

            console.log("id es " + value);
                    
            pruebas.findOneAndUpdate({_id:ObjectID(value)},  { $set:{state:true}}, function(error, resultTest){
                "use strict";
                if(error){
                    return callbackAs(true);
                }
                
                let indexSubType;

                switch( resultTest.value.subType ) {
                    case "naturales":                             
                        indexSubType=0;                
                        break;
                    case "largo":
                        indexSubType=1;     
                        break;
                    case "sociales":
                        indexSubType=2;       
                        break;    
                    case "lectura":
                        indexSubType=3;  
                        break;
                    case "corto":
                        indexSubType=4;  
                        break;
                    case "matematicas":
                        indexSubType=5; 
                        break;        
                    case "medio":
                        indexSubType=6; 
                        break;                                               
                    default:
                        console.log("ERROR HAY UN NUEVO tipo");
                }

                let updateManyUser={},
                    setManyUser={};

                    updateManyUser["tipo"]=0;
                    updateManyUser["pruebasR."+indexSubType+".pruebas._id"]= { $ne: value };

                    setManyUser["pruebasNR."+indexSubType+".pruebas"]=ObjectID(value);



                
                usuarios.updateMany(  updateManyUser ,  { $push: setManyUser }, function(error, result){
                    "use strict";
                    if(error){
                        return callbackAs(true);
                    }


                    let updateManyMongo={},
                        setManyMongo={};

                        updateManyMongo["pruebasR."+indexSubType+".pruebas._id"]=value;
                        setManyMongo["pruebasR."+indexSubType+".pruebas.$.state"]=true;

                    usuarios.updateMany( updateManyMongo ,  { $set:setManyMongo }, function(error, result){
                        "use strict";
                        if(error){
                            return callbackAs(true);
                        }
    
    
                    });

                    return callbackAs();

                });


               
            });


        }, function (err) {

            if (err) return callback(false);

               
                    return callback(true);                                         


                });  

       

        
    }   
    
    this.putActivateQuestions = function(ids, callback){
        "use strict";



        async.forEachOf(ids, function (value, key, callbackAs) {

            console.log("id es " + value);
                    
            preguntas.updateOne({_id:ObjectID(value)},  { $set:{state:true}}, function(error, result){
                "use strict";
                if(error){
                    return callbackAs(true);
                }
                return callbackAs();
            });


        }, function (err) {

            if (err) return callback(false);

               
                    return callback(true);                                         


                });  

       

        
    }

    this.putDeactivateQuestions = function(ids, callback){
        "use strict";



        async.forEachOf(ids, function (value, key, callbackAs) {

            console.log("id es " + value);
                    
            preguntas.updateOne({_id:ObjectID(value)},  { $set:{state:false}}, function(error, result){
                "use strict";
                if(error){
                    return callbackAs(true);
                }
                return callbackAs();
            });


        }, function (err) {

            if (err) return callback(false);

               
                    return callback(true);                                         


                });  

       

        
    }

    this.getAverage = function( idtest,callback){
        "use strict";

        estadisticas.findOne({  idprueba:ObjectID(idtest)  }, function(err, estadis){
            "use strict";
            
            if (err) return callback(0,{});

            if (estadis) {

                var estadisFinal={};

                if (estadis.naturales!=undefined) estadisFinal.naturales={score: estadis.naturales.reduce((a, b) => a + b, 0) /estadis.naturales.length, amount:estadis.naturales.length     };
                if (estadis.sociales!=undefined) estadisFinal.sociales={score: estadis.sociales.reduce((a, b) => a + b, 0) /estadis.sociales.length, amount:estadis.sociales.length     };    
                if (estadis.lectura!=undefined) estadisFinal.lectura={score: estadis.lectura.reduce((a, b) => a + b, 0) /estadis.lectura.length, amount:estadis.lectura.length     };    
                if (estadis.matematicas!=undefined) estadisFinal.matematicas={score: estadis.matematicas.reduce((a, b) => a + b, 0) /estadis.matematicas.length, amount:estadis.matematicas.length     };    
                if (estadis.total!=undefined) estadisFinal.total={score: estadis.total.reduce((a, b) => a + b, 0) /estadis.total.length, amount:estadis.total.length     };    

                for (let i = 0; i < estadis.preguntas.length; i++) {
                    const preguntaDoc = estadis.preguntas[i];

                    var sumaT=0;

                    if (preguntaDoc.correctas==undefined)preguntaDoc.correctas=0;
                    else sumaT=sumaT+preguntaDoc.correctas;

                    if (preguntaDoc.incorrectas==undefined)preguntaDoc.incorrectas=0;
                    else sumaT=sumaT+preguntaDoc.incorrectas;

                    if(sumaT==0)sumaT=1;

                    preguntaDoc.porCorrectas=(preguntaDoc.correctas*100)/sumaT;
     
                }

                estadisFinal.preguntas=estadis.preguntas;


                return callback(1, estadisFinal );
                
            } else  return callback(0,{} );





        });


    }

    this.callbackYoutubeApiGet = function( callback){
        "use strict";

        usuarios.findOne({  tipo:1  },{projection:{"sugerencias.porSubir":1} }, function(err, docUsu){
            "use strict";
            
            if (err) return callback(false);

            if (docUsu) {

                return callback(true,docUsu.sugerencias.porSubir);
                
            } else  return callback(false);





        });


    }

    this.validateToken = function(idusuario,token, callback){
        "use strict";

        usuarios.findOne({_id:ObjectID(idusuario),token:token}, function(err, docUsu){
            "use strict";
            
            if (err) return callback(false);

            if (docUsu) {

                return callback(true);
                
            } else  return callback(false);





        });


    }
    
    String.prototype.shuffle = function () {
        var a = this.split(""),
            n = a.length;
    
        for(var i = n - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var tmp = a[i];
            a[i] = a[j];
            a[j] = tmp;
        }
        return a.join("");
    }

    function generate_token(length){
        //edit the token allowed characters
        var a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".split("");
        var b = [];  
        for (var i=0; i<length; i++) {
            var j = (Math.random() * (a.length-1)).toFixed(0);
            b[i] = a[j];
        }
        return b.join("");
    }


}

module.exports.AdminDAO = AdminDAO;


